//
//  OK_PromoMoreInfoElement.swift
//  Okinava
//
//  Created by _ on 22.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_PromoMoreInfoElement: UIView {
    
    let selectingImageView = UIImageView(image: #imageLiteral(resourceName: "right_arrow_small"))
    
    let textLabel = UILabel()
    let imageView = UIImageView()
    var isDrawOnce = false
    var padding = 16
    var actionURL: URL? = nil
    
    init(image: UIImage, title: String, url: URL){
        super.init(frame: CGRect.zero)
        self.textLabel.text = title
        self.imageView.image = image
        
        
        self.actionURL = url
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        self.backgroundColor = .clear
        
        self.addSubview(imageView)
        self.addSubview(textLabel)
        
        
        self.textLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(imageView.snp.right).offset(13)
            make.right.equalToSuperview().offset(-self.padding)
        }
        
        self.imageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(20)
            make.left.equalToSuperview().offset(self.padding)
            make.centerY.equalToSuperview()
        }
        
        
        
        self.addSubview(self.selectingImageView)
        self.selectingImageView.snp.makeConstraints { (make) in
            make.height.equalTo(10)
            make.width.equalTo(5)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-self.padding)
        }
        
        self.imageView.contentMode = .scaleAspectFit
        self.textLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.textLabel.textColor = DEFAULT_COLOR_BLACK
        self.textLabel.adjustsFontSizeToFitWidth = true
        self.isDrawOnce = true
        let tappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped))
        self.addGestureRecognizer(tappedGesture)
        
        
    }
    
    @objc func viewTapped(){
        UIApplication.shared.open(self.actionURL!, options: [:], completionHandler: nil)
    }
    
    
}
