//
//  DeliveryTerminalTableViewCell.swift
//  Okinava
//
//  Created by _ on 20.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
let DeliveryTerminalTableViewCellIdentifer = "DeliveryTerminalTableViewCellIdentifer"

class DeliveryTerminalTableViewCell: UITableViewCell {
    var terminal: smartRestorans?
    let terminalNameLabel = UILabel()
    let terminalWorkTimeLabel = UILabel()
    let terminalPhoto = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func prepareView(){
        self.subviews.forEach({ $0.removeFromSuperview()})
        let wrapper = UIView()
        self.backgroundColor = .clear
        wrapper.backgroundColor = .white
        let imageForName = UIImageView(image: #imageLiteral(resourceName: "icon_house_gray"))
        let imageForTime = UIImageView(image: #imageLiteral(resourceName: "icon_clock_gray"))
        self.addSubview(wrapper)
        wrapper.addSubview(terminalPhoto)
        wrapper.addSubview(imageForName)
        wrapper.addSubview(terminalNameLabel)
        wrapper.addSubview(imageForTime)
        wrapper.addSubview(terminalWorkTimeLabel)
        
        wrapper.layer.shadowOffset = CGSize(width: 0, height: 8)
        wrapper.layer.shadowOpacity = 0.12
        wrapper.layer.shadowRadius = 15
        wrapper.layer.cornerRadius = 8
        
        self.terminalPhoto.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.terminalPhoto.layer.cornerRadius = 8
        
//        wrapper.clipsToBounds = true
        
        wrapper.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
//            make.height.equalTo(50)
        }
        
        self.terminalPhoto.sd_setImage(with: URL(string: self.terminal!.image), completed: nil)
        self.terminalPhoto.contentMode = .scaleAspectFill
        self.terminalPhoto.clipsToBounds = true
        self.terminalPhoto.snp.makeConstraints { (make) in
            make.height.equalTo(160)
            make.top.left.right.equalToSuperview()
            
        }
        imageForName.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(self.terminalPhoto.snp.bottom).offset(15)
            make.bottom.equalToSuperview().offset(-15)
            make.height.width.equalTo(16)
        }
        self.terminalNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageForName.snp.right).offset(7)
            make.centerY.equalTo(imageForName)
            make.width.equalToSuperview().multipliedBy(0.45)

        }
        self.terminalWorkTimeLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
                       make.centerY.equalTo(imageForName)
        }
        imageForTime.snp.makeConstraints { (make) in
            make.right.equalTo(self.terminalWorkTimeLabel.snp.left).offset(-5)
            make.centerY.equalTo(imageForName)
            make.height.width.equalTo(16)
        }
        
        self.terminalWorkTimeLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.terminalWorkTimeLabel.textAlignment = .right
        self.terminalWorkTimeLabel.textColor = DEFAULT_COLOR_BLACK
        self.terminalNameLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.terminalNameLabel.adjustsFontSizeToFitWidth = true
        
    }
    func setData(){
        let week = Date().get(component: .weekday)
        
        var from = ""
        var to = ""
        for openHour in  self.terminal!.openingHours {
            if openHour.dayOfWeek == week {
                from = openHour.from
                to = openHour.to
                break
            }
        }
        self.terminalWorkTimeLabel.text = from + " - " +  to
        self.terminalNameLabel.text = self.terminal?.name
    }
    

}
