//
//  OK_ContactView.swift
//  Okinava
//
//  Created by _ on 20.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_ContactView: UIView {
    var haveSelectingView  = false
    let selectingImageView = UIImageView(image: #imageLiteral(resourceName: "selected_icon"))
    
    let textLabel = UILabel()
    let imageView = UIImageView()
    var isDrawOnce = false
    var padding = 16
    
    
    init(image: UIImage, title: String){
        super.init(frame: CGRect.zero)
        self.textLabel.text = title
        self.imageView.image = image
        self.isOpaque = false
        self.selectingImageView.alpha = 0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        self.backgroundColor = .clear
        
        self.addSubview(imageView)
        self.addSubview(textLabel)
        
        
        self.textLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(imageView.snp.right).offset(10)
            make.right.equalToSuperview().offset(-self.padding)
        }
        
        self.imageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(24)
            make.left.equalToSuperview().offset(self.padding)
            make.centerY.equalToSuperview()
        }
        
        
        if (self.haveSelectingView){
            self.addSubview(self.selectingImageView)
            self.selectingImageView.snp.makeConstraints { (make) in
                make.height.equalTo(8)
                make.width.equalTo(12)
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-self.padding)
            }
        }
        self.imageView.contentMode = .scaleAspectFit
//        self.textLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.textLabel.textColor = DEFAULT_COLOR_BLACK
        self.textLabel.adjustsFontSizeToFitWidth = true
        self.isDrawOnce = true
    }
    
    func isSelecting(visible: Bool) {
        if (self.haveSelectingView) {
            self.selectingImageView.alpha = visible  ? 1 : 0
        }
        if visible {
            self.textLabel.font = FONT_ROBOTO_BOLD?.withSize(13)
        } else {
            self.textLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        }
    }
    
    
}
