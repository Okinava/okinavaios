//
//  Groups.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite

struct Groups: SQLCommand {    
    var id: String
    var name: String
    var order: Int64
    var parentGroup: String
    var image: String
    var isIncludedInMenu: Bool
    
    init(id: String = "", name: String = "", order: Int64 = 0, parentGroup: String = "", image: String = "", isIncludedInMenu: Bool = false) {
        self.id = id
        self.name = name
        self.order = order
        self.parentGroup = parentGroup
        self.image = image
        self.isIncludedInMenu = isIncludedInMenu
    }
    
    static let id = Expression<String>("id")
    static let name = Expression<String>("name")
    static let order = Expression<Int64>("order")
    static let parentGroup = Expression<String>("parentGroup")
    static let image = Expression<String>("image")
    static let isIncludedInMenu = Expression<Bool>("isIncludedInMenu")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Groups.id <- id,
            Groups.name <- name,
            Groups.order <- order,
            Groups.parentGroup <- parentGroup,
            Groups.image <- image,
            Groups.isIncludedInMenu <- isIncludedInMenu
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Groups.id == id).update(
            Groups.name <- name,
            Groups.order <- order,
            Groups.parentGroup <- parentGroup,
            Groups.image <- image,
            Groups.isIncludedInMenu <- isIncludedInMenu
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Groups.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Groups.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Groups(id: row[id], name: row[name], order: row[order], parentGroup: row[parentGroup], image: row[image], isIncludedInMenu: row[isIncludedInMenu]) as! T
    }
    
    func tableName() -> String{ return "Groups"}
    static func tableName() -> String{ return "Groups"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(name)
            t.column(order)
            t.column(parentGroup)
            t.column(image)
            t.column(isIncludedInMenu)
        })
        return table
    }
}

