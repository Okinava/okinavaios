//
//  TutorialCollectionViewCell.swift
//  Okinava
//
//  Created by _ on 29.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
let TutorialCollectionViewCellIdentifer = "TutorialCollectionViewCellIdentifer"
class TutorialCollectionViewCell: UICollectionViewCell {
    
    private let mainImage: UIImageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    public var isPreparied = false
    public func prepareView(){
        self.addSubview(self.mainImage)
        self.addSubview(self.titleLabel)
        self.addSubview(self.descriptionLabel)
        
        self.titleLabel.font = FONT_ROBOTO_REGULAR?.withSize(18)
        self.titleLabel.textColor = DEFAULT_COLOR_BLACK
        self.descriptionLabel.font = FONT_ROBOTO_REGULAR?.withSize(13)
        self.descriptionLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.descriptionLabel.numberOfLines = 4
        self.titleLabel.numberOfLines = 2
        self.titleLabel.textAlignment = .center
        self.descriptionLabel.textAlignment = .center
        
        self.titleLabel.adjustsFontSizeToFitWidth = true
        self.descriptionLabel.adjustsFontSizeToFitWidth = true
        
        self.mainImage.snp.makeConstraints { (make) in
            make.height.equalToSuperview().multipliedBy(0.6)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.mainImage.snp.bottom).offset(48)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.8)
        }
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            
            make.width.equalToSuperview().multipliedBy(0.8)
        }
        
        self.isPreparied = true
        self.mainImage.clipsToBounds = true
        self.mainImage.contentMode = .scaleAspectFit
        self.clipsToBounds = true
        
        
        
        
    }
    public func setData(title: String, description: String, image: UIImage){
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        self.mainImage.image = image
    }
    
    
}
