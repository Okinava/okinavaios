//
//  OK_EditAddressBlockView.swift
//  Okinava
//
//  Created by _ on 27.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_EditAddressBlockView: UIView {

    var titleLabel = UILabel()
    var valueTextField = OK_TextField()
    
    
    override func draw(_ rect: CGRect) {
        self.isOpaque = false
        self.backgroundColor = .white
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.titleLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.titleLabel.textColor = DEFAULT_COLOR_GRAY
        
        self.valueTextField.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.valueTextField.textColor = .black
        
        self.valueTextField.layer.borderWidth = 1
        self.valueTextField.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.valueTextField)
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16+12)
            make.right.equalToSuperview().offset((16+12) * -1)
            make.top.equalToSuperview()
            
        }
        self.valueTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(2)
            make.height.equalTo(48)
            make.bottom.equalToSuperview()
        }
        
    }
    

}

