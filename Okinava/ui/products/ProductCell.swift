//
//  ProductCell.swift
//  Okinava
//
//  Created by Timerlan on 02.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import RxSwift

let ProductCellID = "ProductCell"

class ProductCell: UICollectionViewCell, UIScrollViewDelegate {
    private var product: Products!
    private var modiferProducts: [Products] = []
    private var currentModifier: String = ""
    private var mName = "";
    private var currentModifierPrice: Double = 0.0
    private var currentProductCount = 1
    private let max = 260 + UIApplication.shared.statusBarFrame.height
    private let min = 160 + UIApplication.shared.statusBarFrame.height
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        view.addSubview(headerImageView)
        view.addSubview(bonusesView)
        return view
    }()
    private lazy var headerImageView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "main_screen_background"))
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    private lazy var bonusesView: UIView = {
        let view = UIView()
        view.addSubview(whiteView)
        view.addSubview(cardView)
        return view
    }()
    private lazy var whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        view.addSubview(starView)
        view.addSubview(line)
        view.addSubview(productTitleLabel)
        view.addSubview(productPriceLabel)
        view.addSubview(productWeightLabel)
        return view
    }()
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()
    private lazy var starView: UIImageView = {
        let view = UIImageView(image: UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star"), targetSize: CGSize(width: 16, height: 16)))
        view.contentMode = .center
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.backgroundColor = .white
        view.isOpaque = false
        return view
    }()
    private lazy var starBtn: UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor().withAlphaComponent(0)
        view.addTarget(self, action: #selector(addToFavButtonPressed), for: .touchUpInside)
        return view
    }()
    private lazy var productTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.5
        return view
    }()
    private lazy var productPriceLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_BOLD?.withSize(21)
        view.textAlignment = .right
        return view
    }()
    private lazy var productWeightLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_GRAY
        view.font = FONT_ROBOTO_REGULAR?.withSize(16)
        return view
    }()
    private lazy var mainScrollView: UIScrollView = {
        let view = UIScrollView()
        view.parallaxHeader.view = headerView
        view.parallaxHeader.height = max
        view.parallaxHeader.minimumHeight = min
        view.parallaxHeader.mode = .topFill
        view.alwaysBounceVertical = true
        view.addSubview(contentMainView)
        return view
    }()
    private lazy var bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOffset = CGSize(width: 0, height: -4)
        view.layer.shadowRadius = 7;
        view.layer.shadowOpacity = 0.04;
        view.addSubview(productCountMore)
        view.addSubview(productCountLabel)
        view.addSubview(productCountLess)
        view.addSubview(productTotalPriceLabel)
        view.addSubview(addToCartButton)
        return view
    }()
    private lazy var productCountMore: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .highlighted)
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.addTarget(self, action: #selector(productMoreTapped), for: .touchUpInside)
        return view
    }()
    private lazy var productCountLess: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "ic_minus"), for: .normal   )
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.addTarget(self, action: #selector(productLessTapped), for: .touchUpInside)
        return view
    }()
    private lazy var productCountLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.font = FONT_ROBOTO_MEDIUM?.withSize(21)
        view.text = "1"
        return view
    }()
    private lazy var productTotalPriceLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor().hexStringToUIColor(0x222222)
        view.font = FONT_ROBOTO_MEDIUM?.withSize(21)
        return view
    }()
    private lazy var addToCartButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("В корзину", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal )
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 4
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
        view.addTarget(self, action: #selector(addToCartButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var contentMainView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(headerDesriptionLabel)
        view.addSubview(productDescriptionLabel)
        view.addSubview(headerNutValueLabel)
        view.addSubview(nutValueStackView)
        view.addSubview(headerTypeLabel)
        view.addSubview(modifiersStackView)
        return view
    }()
    private lazy var headerDesriptionLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = "Описание"
        return view
    }()
    private lazy var productDescriptionLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(14   )
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.numberOfLines = 0
        return view
    }()
    private lazy var headerNutValueLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = "Пищевая ценность на 100г"
        return view
    }()
    private lazy var carbohydrateView = OK_NutValueView()
    private lazy var fatView = OK_NutValueView()
    private lazy var energyView = OK_NutValueView()
    private lazy var fiberView = OK_NutValueView()
    private lazy var nutValueStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .fill
        view.isOpaque = false
        view.addArrangedSubview(carbohydrateView)
        view.addArrangedSubview(fatView)
        view.addArrangedSubview(fiberView)
        view.addArrangedSubview(energyView)
        return view
    }()
    private lazy var modifiersStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .fill
        view.isOpaque = false
        return view
    }()
    private lazy var headerTypeLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = ""
        return view
    }()
    
    func setData(product: Products, index: Int) {
        fiberView.keyLabel.text = "Белки"
        fatView.keyLabel.text = "Жиры"
        carbohydrateView.keyLabel.text = "Углеводы"
        energyView.keyLabel.text = "кКал"
        addSubview(mainScrollView)
        addSubview(bottomView)
        addSubview(starBtn)
        mainScrollView.delegate = self
        update()
        mainScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 600)
        
        self.product = product
        fiberView.valueLabel.text = String(format: "%.02f", product.fiberAmount)
        fatView.valueLabel.text = String(format: "%.02f", product.fatAmount)
        carbohydrateView.valueLabel.text = String(format: "%.02f", product.carbohydrateAmount)
        energyView.valueLabel.text = String(format: "%.02f", product.energyAmount)
        productTitleLabel.text = product.name
        productPriceLabel.text = product.price.cleanValue + " " + RUBLE_LETTER.uppercased()
        if product.weight >= 1 {
            productWeightLabel.text = String(format: "%.02f", product.weight) + " кг."
        }else{
            productWeightLabel.text = "\(Int(product.weight * 1000)) гр."
        }
        productDescriptionLabel.text = product.product_description
        if ((self.product.image.count) > 0){
            headerImageView.sd_setImage(with: URL(string: self.product!.image ))
        }

        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        modiferProducts = DataManager.shared.getModificatorsNO(product: product)
        if modiferProducts.count != 0 {
            self.headerTypeLabel.text = "Добавить"
            self.headerTypeLabel.isHidden = false;
            modifiersStackView.isHidden = false;
        }else{
            self.headerTypeLabel.isHidden = true;
            modifiersStackView.isHidden = true;
        }
        modifiersStackView.arrangedSubviews.forEach { (u) in
            modifiersStackView.removeArrangedSubview(u)
            u.isHidden = true
        }
        modiferProducts.forEach({ (p) in
            var name = p.name + " ( + " + p.price.cleanValue + RUBLE_LETTER + " )"
            if name.first == "-" { name.remove(at: name.startIndex) }
            let modView = OK_ModifierView(title: name, product: p)
            modView.haveSelectingView = true
            self.modifiersStackView.addArrangedSubview(modView)
            modView.snp.makeConstraints { (make) in make.height.equalTo(35) }
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.modifierTapped))
            modView.addGestureRecognizer(gesture)
        })
        
        if (DataManager.shared.isFavourite(product: product)) {
            starView.image = UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star_fill"), targetSize: CGSize(width: 16, height: 16))
        } else {
            starView.image = UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star_bordered"), targetSize: CGSize(width: 16, height: 16))
        }
        if index > 2 { mainScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 10, height: 10), animated: false) }
    }
    
    private func update() {
        headerImageView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
        bonusesView.snp.remakeConstraints { (make) in
            make.left.right.width.equalToSuperview()
            make.top.equalTo(-mainScrollView.parallaxHeader.scrollView.contentOffset.y - 80)
        }
        whiteView.snp.remakeConstraints{ make in
            make.left.right.width.equalToSuperview()
            make.top.equalTo(30)
            make.height.equalTo(70)
        }
        cardView.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
            make.height.equalTo(70)
        }
        starView.snp.remakeConstraints { (make) in
            make.height.centerY.left.equalToSuperview()
            make.width.equalTo(34)
        }
        starBtn.snp.remakeConstraints { (make) in
            make.top.equalTo(bonusesView.snp.top)
            make.left.equalTo(10)
            make.width.equalTo(60)
            make.height.equalTo(70)
        }
        line.snp.remakeConstraints{ make in
            make.height.equalToSuperview()
            make.width.equalTo(1)
            make.left.equalTo(starView.snp.right)
        }
        productTitleLabel.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(9)
            make.left.equalTo(line.snp.right).offset(10)
            make.right.equalTo(productPriceLabel.snp.left).offset(5)
        }
        productWeightLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(productTitleLabel.snp.bottom).offset(3)
            make.left.equalTo(productTitleLabel.snp.left)
            make.bottom.equalToSuperview().offset(-9)
        }
        productPriceLabel.snp.remakeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(productTitleLabel.snp.top)
            make.width.equalTo(80)
        }
        mainScrollView.snp.remakeConstraints{ make in
            make.top.left.equalTo(0)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.bottom.equalTo(0).offset(-66 - UIApplication.shared.statusBarFrame.height)
        }
        bottomView.snp.remakeConstraints { (make) in
            make.height.equalTo(64+UIApplication.shared.statusBarFrame.height)
            make.width.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
            make.centerX.equalToSuperview()
        }
        productCountMore.snp.remakeConstraints { (make) in
            make.width.height.equalTo(32)
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(16)
        }
        productCountLess.snp.remakeConstraints { (make) in
            make.width.height.equalTo(32)
            make.left.equalTo(productCountLabel.snp.right).offset(5)
            make.top.equalToSuperview().offset(16)
        }
        productCountLabel.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(18)
            make.left.equalTo(productCountMore.snp.right).offset(5)
            make.width.greaterThanOrEqualTo(30)
        }
        addToCartButton.snp.remakeConstraints { (make) in
            make.width.equalTo(96)
            make.top.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        productTotalPriceLabel.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.centerX.equalToSuperview()
        }
        contentMainView.snp.remakeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(600)
        }
        headerDesriptionLabel.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(15)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        }
        productDescriptionLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(headerDesriptionLabel.snp.bottom).offset(10)
            make.width.equalTo(cardView.snp.width)
            make.left.equalTo(16)
        }
        headerNutValueLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(productDescriptionLabel.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        }
        nutValueStackView.snp.remakeConstraints { (make) in
            make.top.equalTo(headerNutValueLabel.snp.bottom).offset(10)
            make.width.equalTo(cardView.snp.width)
            make.left.equalTo(16)
        }
        headerTypeLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(nutValueStackView.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        }
        modifiersStackView.snp.remakeConstraints { (make) in
            make.top.equalTo(headerTypeLabel.snp.bottom).offset(10)
            make.left.equalTo(0)
            make.width.equalTo(cardView.snp.width)
        }
        fiberView.snp.remakeConstraints { (make) in make.height.equalTo(22)}
        fatView.snp.remakeConstraints { (make) in make.height.equalTo(22)}
        carbohydrateView.snp.remakeConstraints { (make) in make.height.equalTo(22)}
        energyView.snp.remakeConstraints { (make) in make.height.equalTo(22)}
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -min { bonusesView.snp.updateConstraints{ make in make.top.equalTo(-scrollView.contentOffset.y - 80) }
        }else{ bonusesView.snp.updateConstraints{ make in make.top.equalTo(min - 80) } }
        self.layoutIfNeeded()
    }
    
    
    @objc func modifierTapped(_ sender: UITapGestureRecognizer){
        for subview in self.modifiersStackView.arrangedSubviews {
            (subview as? OK_ModifierView)?.isSelecting(visible: false)
        }
        (sender.view as? OK_ModifierView)?.isSelecting(visible: true)
        self.mName = (sender.view as? OK_ModifierView)!.textLabel.text!
        self.currentModifier  = (sender.view as? OK_ModifierView)!.product!.id
        self.currentModifierPrice = (sender.view as? OK_ModifierView)!.product!.price
        self.changeProductCount(increment: 0)
    }
    
    @objc func addToFavButtonPressed() {
        if (DataManager.shared.isFavourite(product: self.product)) {
            _ = DataManager.shared.removeFavourite(product: product).subscribe{ self.reloadFav() }
            self.starView.image = UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star_bordered"), targetSize: CGSize(width: 16, height: 16))
            AlertUtil.alert({}, mes: "\(self.product.name) \(self.mName) удален из Избранного", title: "Печально!")
        } else {
            _ = DataManager.shared.addFavourite(product: product).subscribe{ self.reloadFav() }
            self.starView.image = UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star_fill"), targetSize: CGSize(width: 16, height: 16))
            AlertUtil.alert({}, mes: "\(self.product.name) \(self.mName) добавлен в Избранное", title: "Ура!")
        }
        InteractionFeedbackManager.shared.makeFeedback()
    }
    
    private func reloadFav(){
        NotificationCenter.default.post(name: NotificationUtil.FAVOURITES_UPD, object: nil)
    }
    
    private func changeProductCount(increment: Int){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        var i = increment
        if increment < 0 && self.currentProductCount == 1 {
            i = 0
            InteractionFeedbackManager.shared.makeNotification(level: .warning)
        }
        self.currentProductCount = self.currentProductCount + i
        
        self.productTotalPriceLabel.text = ((self.product.price + self.currentModifierPrice) * Double(self.currentProductCount)).cleanValue + " " + RUBLE_LETTER
        self.productCountLabel.text =  String(self.currentProductCount)
    }
    
    @objc func addToCartButtonTapped(){
        _ = DataManager.shared.addOrderAndCheck(product: product, modificator: currentModifier, count: Int64(currentProductCount))
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { res in
                if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                else{
                    self.currentProductCount = 1
                    self.changeProductCount(increment: 0)
                    AlertUtil.alert({}, mes: "\(self.product.name) \(self.mName) добавлен в корзину", title: "Ура!")
                }
            })
    }
    @objc func productLessTapped(){
        self.changeProductCount(increment: -1)
    }
    
    @objc func productMoreTapped(){
        self.changeProductCount(increment: 1)
    }
}
