//
//  EditProfileViewController.swift
//  Okinava
//
//  Created by _ on 09.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import UIKit
import SnapKit
import RxSwift

class EditProfileViewController: SwipeRightToPopViewController {
    private lazy var saveButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Сохранить", for: .normal)
        view.setTitleColor(.white, for: .normal  )
        view.backgroundColor = DEFAULT_COLOR_RED
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.layer.cornerRadius = 6
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addTarget(self, action: #selector(self.saveButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var exitButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Выйти", for: .normal)
        view.setTitleColor(.white, for: .normal  )
        view.backgroundColor = DEFAULT_COLOR_RED
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.layer.cornerRadius = 6
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addTarget(self, action: #selector(self.exit), for: .touchUpInside)
        return view
    }()
    private let topNavigationView = OK_TopNavigationBlock()
    private let nameBlock = OK_EditBlockView()
    private let birtdayBlock = OK_EditBlockView()
    private let sexBlock = OK_EditBlockView()
    let scrollView = UIScrollView()
    let contentView = UIView()
    var activeField: UITextField?
    var datePicker = UIDatePicker()
    let sexList = ["Мужской", "Женский"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        self.loadData()
        self.hideKeyboardWhenTappedAround()
        self.registerForKeyboardNotifications()
    }
  
    private func prepareView(){
        self.view.backgroundColor = DEFAULT_BACKGROUND
        self.view.addSubview(self.scrollView)
        nameBlock.backgroundColor = .white
        birtdayBlock.backgroundColor = .white
        sexBlock.backgroundColor = .white
        scrollView.alwaysBounceVertical = true
        self.scrollView.addSubview(self.contentView)
        self.view.addSubview(self.topNavigationView)
        self.contentView.backgroundColor = .white
        self.scrollView.addSubview(self.saveButton)
        self.scrollView.addSubview(self.exitButton)
        self.topNavigationView.prepareView()
        self.topNavigationView.backgroundColor = DEFAULT_BACKGROUND
        self.topNavigationView.navigationTitle = "Редактирование"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 600)
        
        topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
        }
        saveButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.contentView.snp.bottom).offset(16)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
            make.height.equalTo(45)
        }
        exitButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.saveButton.snp.bottom).offset(16)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
            make.height.equalTo(45)
        }
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.bottom.equalToSuperview().offset(0)
            make.left.right.equalToSuperview()
        }
        contentView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-140)
            make.top.equalToSuperview().offset(10)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
        }
        self.contentView.addSubview(self.nameBlock)
        self.contentView.addSubview(self.birtdayBlock)
        self.contentView.addSubview(self.sexBlock)
        self.nameBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            make.height.equalTo(80)
        }
        self.birtdayBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(nameBlock.snp.bottom)
            make.height.equalTo(80)
        }
        self.sexBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(birtdayBlock.snp.bottom)
            make.height.equalTo(80)
            make.bottom.equalToSuperview().offset(-20)
        }
        self.contentView.backgroundColor = .white
        self.contentView.layer.cornerRadius = 8
        self.contentView.layer.shadowRadius = 16
        self.contentView.layer.shadowOpacity = 0.08
        self.contentView.layer.shadowOffset = CGSize(width: 0, height: 8)
        self.nameBlock.set(title: "Имя")
        self.birtdayBlock.set(title: "День рождения")
        self.sexBlock.set(title: "Пол")
        sexBlock.valueTextField.delegate = self
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: (self.view.frame.height - 200), width: view.frame.width, height: 200))
        datePicker.backgroundColor = .white
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = DEFAULT_COLOR_RED
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Выбрать", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.birthDaySelectDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, spaceButton,  doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.birtdayBlock.valueTextField.inputView = datePicker
        self.birtdayBlock.valueTextField.inputAccessoryView = toolBar
        self.datePicker.addTarget(self, action: #selector(self.birthdayDateChanged), for: UIControl.Event.valueChanged)
        self.datePicker.maximumDate = Date()
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
    }
    @objc private func birthDaySelectDone(){
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        self.birtdayBlock.set(value: self.datePicker.date.toUserFormatString(format: "YYYY-MM-dd"))
        self.birtdayBlock.valueTextField.endEditing(true)
    }
    
    private func loadData(){
        nameBlock.set(value: DataManager.shared.customerHelper.name)
        birtdayBlock.set(value: DataManager.shared.customerHelper.date)
        if (DataManager.shared.customerHelper.date == "Дата рождения не указана") {
            birtdayBlock.set(value: DataManager.shared.customerHelper.date)
        }else{
            birtdayBlock.valueTextField.textColor = DEFAULT_COLOR_GRAY
            birtdayBlock.valueTextField.isUserInteractionEnabled = false
            birtdayBlock.valueTextField.isEnabled = false
            let bday = DataManager.shared.customerHelper.date
            let splited = bday.split(separator: ".")
            if splited.count == 3 {
                birtdayBlock.set(value: splited[2] + "-" + splited[1] + "-" + splited[0])
            }
        }
        switch (DataManager.shared.customerHelper.sex) {
        case 1: sexBlock.set(value: "Мужской"); break
        case 2: sexBlock.set(value: "Женский"); break
        default: sexBlock.set(value: "Пол не указан"); break
        }
    }
    
    @objc func openDatePicker()  {
        self.dismissKeyboard()
        self.datePicker.maximumDate = Date()
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        self.datePicker.backgroundColor = UIColor.white
    }
    @objc func birthdayDateChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
    }
    
    @objc func exit(){
        DataManager.shared.customerHelper.setDefault(phone: "")
        navigationController?.viewControllers = [ProfileViewController()]
    }
    
    @objc func saveButtonTapped(){
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.setCustomer(name: nameBlock.getValue() ?? "Нет имени", date: birtdayBlock.getValue() != "Дата рождения не указана" ? birtdayBlock.getValue() : nil, sex: sexBlock.getValue() == "Мужской" ? 1 : sexBlock.getValue() == "Женский" ? 2 : 0)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { s in
                self.hideModalSpinner(indicator: spiner)
                self.end()
            },onError: { t in
                self.hideModalSpinner(indicator: spiner)
                if t.localizedDescription == "Не удалось получить ответ"{
                    AlertUtil.alert({}, mes: "Не удалось обновить пользователя. Поверьте интернет соединение!")
                    return
                }
                self.end()
            })
    }
    private func end(){
        DataManager.shared.customerHelper.name = nameBlock.getValue() ?? "Нет имени"
        DataManager.shared.customerHelper.date = birtdayBlock.getValue()!
        DataManager.shared.customerHelper.sex = sexBlock.getValue() == "Мужской" ? 1 : sexBlock.getValue() == "Женский" ? 2 : 0
        navigationController?.viewControllers = [ProfileViewController()]
    }
    
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize!.height) }
    }
    
    @objc  func keyboardWillBeHidden(notification: NSNotification){
       self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        dismissKeyboard()
        AlertUtil.myAlert("Выберите Ваш пол", messages: sexList, voids: [{
                self.sexBlock.set(value: "Мужской")
            },{
                self.sexBlock.set(value: "Женский")
            }])
    }
}
