//
//  SplashView.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit

class SplashView: BaseView {
    private lazy var imgView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "load_screen")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    override func setContent() {
        contentView.addSubview(imgView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        imgView.snp.remakeConstraints{ make in make.edges.equalToSuperview() }
    }
}
