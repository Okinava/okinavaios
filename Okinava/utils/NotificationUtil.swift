//
//  NotificationUtils.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//
import Foundation

class NotificationUtil{
    public static let FAVOURITES_UPD = NSNotification.Name(rawValue: "FAVOURITES_ADD");
    
    public static let ORDERS_ADD = NSNotification.Name(rawValue: "ORDERS_ADD");
    public static let ORDERS_REMOVE = NSNotification.Name(rawValue: "ORDERS_REMOVE");
    public static let ORDERS_UPDATE = NSNotification.Name(rawValue: "ORDERS_UPDATE");
    public static let ORDERS_CLEAR = NSNotification.Name(rawValue: "ORDERS_CLEAR");
    public static let CUSTOMER = NSNotification.Name(rawValue: "CUSTOMER");
    public static let ADDRESS = NSNotification.Name(rawValue: "ADDRESS");
}
