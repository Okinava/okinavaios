//
//  OK_HowToGetElement.swift
//  Okinava
//
//  Created by _ on 22.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class OK_HowToGetElement: UIView {
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    let iconImageView = UIImageView()
    let borderView = UIView()
    
    
    override func draw(_ rect: CGRect) {
        
        self.addSubview(self.borderView)
        self.borderView.addSubview(self.iconImageView)
        self.addSubview(titleLabel)
        self.addSubview(descriptionLabel)
        
        self.borderView.snp.makeConstraints { (make) in
            make.width.height.equalTo(40)
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            
        }
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.borderView.snp.right).offset(10)
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-10)
            make.left.equalTo(self.titleLabel.snp.left)
        }
        
        self.iconImageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(15)
            make.centerX.centerY.equalToSuperview()
        }
        
        self.borderView.layer.cornerRadius  = 4
        self.borderView.layer.borderWidth = 1
        self.borderView.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        
        self.titleLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.descriptionLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.descriptionLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.iconImageView.contentMode = .scaleAspectFit
        self.layer.cornerRadius = 8
//        self.descriptionLabel.numberOfLines  = 0
    }
    
    public func set(title: String)
    {
        self.titleLabel.text = title
    }
    public func set(icon: UIImage){
        self.iconImageView.image = icon
    }
    public func set(description: String){
        self.descriptionLabel.text = description
    }
}
