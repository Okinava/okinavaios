//
//  customersDeli veryHistory.swift
//  Okinava
//
//  Created by Эмиль Сабитов on 19.05.2018.
//  Copyright © 2018 Эмиль Сабитов. All rights reserved.
//


import Foundation
import AlamofireObjectMapper
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class OrderItem:  Object, Mappable  {
    
    @objc dynamic var id: String = ""
    @objc dynamic var code: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var amount: Float = 0.0
    @objc dynamic var sum: Float = 0.0
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        name <- map["name"]
        amount <- map["amount"]
        sum <- map["sum"]
        
    }
    
}
