//
//  ViewPagerTabView.swift
//  Okinava
//
//  Created by Timerlan on 27.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import UIKit

public final class ViewPagerTabView: ZFRippleButton {
    private var mTitle = ""
    
    override init(frame: CGRect) { super.init(frame: frame) }
    
    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    internal func setup(_ title: String) {
        mTitle = title
        removeHighlight()
        backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        rippleBackgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        rippleColor = UIColor.white
        ripplePercent = 1
    }
    
    internal func addHighlight() {
        self.setAttributedTitle(NSAttributedString(string: mTitle, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20.0), NSAttributedString.Key.foregroundColor: UIColor.black]), for: .normal)
    }
    
    internal func removeHighlight() {
        self.setAttributedTitle(NSAttributedString(string: mTitle, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20.0), NSAttributedString.Key.foregroundColor: UIColor.gray]), for: .normal)
    }
}

