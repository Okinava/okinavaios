//
//  BackButton.swift
//  Okinava
//
//  Created by _ on 11.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_BackButton: ZFRippleButton {
      var isWithShadow : Bool = true
    override func draw(_ rect: CGRect) {
        self.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal)
        self.backgroundColor = .white
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.cornerRadius = 4
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        
        if isWithShadow {
        self.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 8
        }
        self.adjustsImageWhenHighlighted = true
        
    }

}
