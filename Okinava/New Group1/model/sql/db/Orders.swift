//
//  Orders.swift
//  Okinava
//
//  Created by Timerlan on 28.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import SQLite

struct Orders: SQLCommand {
    var productId: String
    var modificatorId: String
    var count: Int64
    
    init(
        productId: String,
        modificatorId: String,
        count: Int64
        ) {
        self.productId = productId
        self.modificatorId = modificatorId
        self.count = count
    }
    
    static let productId = Expression<String>("productId")
    static let modificatorId = Expression<String>("modificatorId")
    static let count = Expression<Int64>("count")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Orders.productId <- productId,
            Orders.modificatorId <- modificatorId,
            Orders.count <- count
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Orders.productId == productId && Orders.modificatorId == modificatorId).update(
            Orders.count <- count
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Orders.productId == productId && Orders.modificatorId == modificatorId).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Orders.productId == productId && Orders.modificatorId == modificatorId)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Orders(productId: row[productId], modificatorId: row[modificatorId], count: row[count]) as! T
    }
    
    func tableName() -> String{ return "Orders"}
    static func tableName() -> String{ return "Orders"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Orders.tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(productId)
            t.column(modificatorId)
            t.column(count)
        })
        return table
    }
}

