//
//  GroupView.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import ParallaxHeader
import SCLAlertView

class GroupView: BaseView, UIScrollViewDelegate {
    private let max = 300 + UIApplication.shared.statusBarFrame.height
    private let min = 120 + UIApplication.shared.statusBarFrame.height
    private let h: CGFloat = 300 - 170
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var alpha: CGFloat = 1
        var alphaButton = 1
        if scrollView.contentOffset.y > -max && scrollView.contentOffset.y < -min {
            alpha = (scrollView.contentOffset.y + h) / -h
        }else if scrollView.contentOffset.y > -min {
            alpha = 0
            alphaButton = 0
        }
        if scrollView.contentOffset.y < -min {
            bonusesView.snp.updateConstraints{ make in
                make.top.equalTo(-scrollView.contentOffset.y - 120)
            }
        }else{
            bonusesView.snp.updateConstraints{ make in
                make.top.equalTo(min - 120)
            }
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.headerImageView.alpha = alpha
            self.logoImageView.alpha = alpha
            self.cardView.alpha = alpha
            self.descriptionLabel.alpha = alpha
            self.topMenuLabel.alpha = 1 - alpha
            self.chatButton.alpha = CGFloat(alphaButton)
            self.searchButton.alpha = CGFloat(alphaButton ^ 1)
            self.promoCodeButton.alpha = alpha
        })
        self.layoutIfNeeded()
    }
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.parallaxHeader.view = headerView
        view.parallaxHeader.height = max
        view.parallaxHeader.minimumHeight = min
        view.parallaxHeader.mode = .topFill
//        view.rowHeight = UITableView.automaticDimension
//        view.estimatedRowHeight = 50
        view.separatorStyle = .none
        view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        return view
    }()
    
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        view.addSubview(headerImageView)
        view.addSubview(logoImageView)
        view.addSubview(descriptionLabel)
        view.addSubview(bonusesView)
        view.addSubview(topMenuLabel)
        view.addSubview(searchButton)
        view.addSubview(basketButton)
        view.addSubview(chatButton)
        view.addSubview(tabView)
        view.addSubview(promoCodeButton)
        return view
    }()
    
    private lazy var headerImageView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "main_screen_background"))
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var topMenuLabel: UILabel = {
        let view = UILabel()
        view.alpha = 0
        view.text = "Меню"
        view.font =  FONT_ROBOTO_BOLD?.withSize(20)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let view = UILabel()
        view.text = "Проверенное качество\nза разумные деньги"
        view.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.textColor = .white
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }()
    
    private lazy var logoImageView = UIImageView(image: #imageLiteral(resourceName: "okinava_text_logo") )
    lazy var searchButton = OK_SearchButton()
    lazy var basketButton = OK_BasketButton()
    private lazy var chatButton = OK_ChatButton()
    
    private lazy var bonusesView: UIView = {
        let view = UIView()
        view.addSubview(whiteView)
        view.addSubview(cardView)
        return view
    }()
    
    private lazy var whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        return view
    }()
    
    private lazy var cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        view.addSubview(starView)
        view.addSubview(bonusTextLabel)
        view.addSubview(bonusCountLabel)
        return view
    }()
    
    private lazy var starView: UIImageView = {
        let view = UIImageView(image: UIImage().resizeImage(image: #imageLiteral(resourceName: "icon_star"), targetSize: CGSize(width: 16, height: 16)))
        view.contentMode = .center
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.backgroundColor = DEFAULT_COLOR_YELLOW
        view.isOpaque = false
        return view
    }()
    
    private lazy var promoCodeButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Промокод", for: .normal)
        view.tintColor  = DEFAULT_COLOR_RED
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.layer.backgroundColor = UIColor.white.cgColor
        view.addTarget(self, action: #selector(self.notYetButtonTapped), for: .touchUpInside)
        return view
    }()
    @objc private func notYetButtonTapped(){
        let alert = SCLAlertView(appearance: GroupView.appearance)
        let txt = alert.addTextField("Введите промокод")
        txt.text = DataManager.shared.customerHelper.promo
        alert.addButton("Применить") {
            DataManager.shared.customerHelper.promo = txt.text ?? ""
        }
        alert.appearance.showCloseButton = false
        alert.appearance.hideWhenBackgroundViewIsTapped = true
        alert.showEdit("Промокод", subTitle: "Введеный промокод будет использован при следующем заказе", closeButtonTitle: "Применить", colorStyle: 0xFF4748)
    }
    static let appearance = SCLAlertView.SCLAppearance(
        kCircleHeight: 80,
        kCircleIconHeight: 40,
        kTitleTop: 50,
        kTitleHeight: 20,
        kWindowWidth: UIScreen.main.bounds.width - 80,
        kWindowHeight: UIScreen.main.bounds.height - 200,
        kButtonHeight: 60,
        kTitleFont: FONT_ROBOTO_BOLD!.withSize(20),
        kTextFont: FONT_ROBOTO_REGULAR!.withSize(14),
        kButtonFont: FONT_ROBOTO_REGULAR!.withSize(16)
    )
    
    lazy var bonusCountLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_BOLD?.withSize(24)
        view.textColor = DEFAULT_COLOR_RED
        view.text = String(Int(DataManager.shared.customerHelper.bonus))
        view.textAlignment = .center
        return view
    }()
    
    private lazy var bonusTextLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_GRAY
        view.textAlignment = .center
        view.text = "бонусов"
        return view
    }()
    
    lazy var tabView: UIScrollView = {
        let view = UIScrollView()
        view.isScrollEnabled = true
        view.alwaysBounceHorizontal = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    override func setContent() {
        contentView.addSubview(tableView)    }
    
    override func updateConstraints() {
        super.updateConstraints()
        tableView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
        headerImageView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
        searchButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.left.equalToSuperview().offset(16)
        }
        chatButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.left.equalToSuperview().offset(16)
        }
        basketButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.right.equalToSuperview().offset(-16)
        }
        logoImageView.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-60)
            make.height.equalTo(30)
        }
        descriptionLabel.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(logoImageView.snp.bottom).offset(16)
        }
        topMenuLabel.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
        }
        bonusesView.snp.remakeConstraints { (make) in
            make.left.right.width.equalToSuperview()
            make.top.equalTo(-tableView.parallaxHeader.scrollView.contentOffset.y - 120)
        }
        whiteView.snp.remakeConstraints{ make in
            make.left.right.width.equalToSuperview()
            make.top.equalTo(30)
            make.height.equalTo(150)
        }
        cardView.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
            make.height.equalTo(60)
        }
        starView.snp.remakeConstraints { (make) in
            make.height.centerY.left.equalToSuperview()
            make.width.equalTo(28)
        }
        promoCodeButton.snp.remakeConstraints { (make) in
            make.centerY.equalTo(bonusesView.snp.centerY).offset(30)
            make.right.equalToSuperview().offset(-32)
            make.width.equalTo(110)
        }
        bonusCountLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(4)
            make.width.equalTo(bonusTextLabel.snp.width)
            make.left.equalTo(starView.snp.right).offset(16)
        }
        bonusTextLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(bonusCountLabel.snp.bottom).offset(0)
            make.left.equalTo(bonusCountLabel.snp.left)
        }
        tabView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.width.equalTo(10000)
            make.top.equalTo(bonusesView.snp.top).offset(70)
            make.height.equalTo(50)
        }
    }
}
