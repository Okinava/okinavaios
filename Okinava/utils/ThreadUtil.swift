//
//  ThreadUtil.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import RxSwift

class ThreadUtil {
    public static let shared = ThreadUtil()
    let backScheduler = SerialDispatchQueueScheduler.init(qos: .utility)
    let mainScheduler = MainScheduler.init()
    let backOpetarion = DispatchQueue.global(qos: .utility)
}
