//
//  DataManager.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD
import RxSwift
import RxCocoa

class DataManager {
    static var shared: DataManager!
    private var iikoHelper: IikoHelper = IikoHelper()
    let customerHelper = CustomerHelper()
    private var dbHelper: DBHelper = DBHelper()
    private var smartHelper: SmartHelper = SmartHelper()
    private var pushHelper: PushHelper
    private var connectionStateChange: Any?
    
    init(app: UIApplication, appDelegate: AppDelegate) { pushHelper = PushHelper(app: app, appDelegate: appDelegate) }
    
    func updateNomenclature() -> Observable<SQLCommand>{
        return iikoHelper.getNomenclature()
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.backScheduler)
            .do(onNext: { n in self.dbHelper.clearNomenclature()})
            .map{ n -> [SQLCommand] in return DBMapper.cast(nomenclature: n) }
            .concatMap{ o -> Observable<SQLCommand> in return Observable.from(o) }
            .do(onNext: { (sql) in self.dbHelper.add(sql) })
    }
    
    func isHaveNomenclature() -> Observable<Bool> {
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.isHaveNomenclature()); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getGroups() -> Observable<[Groups]>{
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.getGroups()); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getProducts() -> Observable<[Products]>{
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.getProducts()); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func isFavourite(product: Products) -> Bool{
        return dbHelper.isFavourite(product: product)
    }
    
    func addFavourite(product: Products) -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.addFavourite(product: product)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func removeFavourite(product: Products) -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.removeFavourite(product: product)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func inOrder(product: Products) -> Observable<Int64> {
        return Observable.create{ obs -> Disposable in
            obs.onNext(self.dbHelper.inOrder(product: product)); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func checkProduct(product: Products) -> Observable<(Bool,String?)> {
        if product.productCategoryId != "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e" { return Observable.just((true,nil)) }
        return Observable.zip(getTime(),getSales())
            .concatMap({ (time, salesNeed) -> Observable<(Bool,String?)> in
                let date = Date(timeIntervalSince1970: TimeInterval(time / 10 / 1000))
                let hour = Calendar(identifier: .gregorian).component(Calendar.Component.hour, from: date)
                let weekday = Calendar(identifier: .gregorian).component(Calendar.Component.weekday, from: date)
                if hour >= 13 && hour <= 15 && weekday != 7 && weekday != 1 && salesNeed {
                    return Observable.just((true,nil))
                }else{
                    return Observable.just((false,"Акцию можно заказать в период в 13:00-16:00, предварительный заказ можно сделать позвонив в колл-центр  \n8 (843) 233-44-33"))
                }
            })
    }
    
    func addOrderAndCheck(product p: Products, modificator m: String = "", count: Int64 = 1) -> Observable<(Bool,String?)> {
        return checkProduct(product: p)
            .do(onNext: { result in if result.0 { _ = self.addOrder(product: p.id, modificator: m, count: count).subscribe() } })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func addOrder(product p: String, modificator m: String, count: Int64) -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in _ = self.dbHelper.addOrder(product: p, modificator: m, count: count)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func addOrder(history: IikoDeliveryHistory) -> Observable<Bool>{
        return Observable.create{ obs -> Disposable in
            var result = true
            history.items.forEach { (item) in
                result = result && self.dbHelper.addOrder(product: item.id ?? "", modificator: item.modifiers.first?.id ?? "", count: item.amount ?? 1)
            }
            obs.onNext(result); obs.onCompleted(); return Disposables.create { }
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func removeOrder(product p: Products, modificator m: String = "", count: Int64? = nil) -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.removeOrder(product: p, modificator: m, count: count)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func clearOrder() -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.clearOrder()
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getModificators(product p: Products) -> Observable<[Products]> {
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.getModificators(product: p)); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getModificatorsNO(product p: Products) -> [Products]{
        return self.dbHelper.getModificators(product: p)
    }
    
    func getOrderCount() -> Observable<Int> {
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.OrderCount()); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getOrder() -> Observable<[(Products,Products?,Int64)]>{
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.getOrder()); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getPromos() -> Observable<[smartPromo]> {
        return smartHelper.getPromos()
    }
    
    func getCustomer(phone: String) -> Observable<iikoCustomer> {
        return iikoHelper.getCustomer(phone: phone)
    }
    
    func setCustomer(name: String, date: String? = nil, sex: Int) -> Observable<String> {
        var par: [String:Any?]
        if let d = date  { par = [ "customer" : [ "phone":DataManager.shared.customerHelper.phone, "name" : name, "sex":sex, "birthday" : d ] ] }
        else { par = [ "customer" : [ "phone":DataManager.shared.customerHelper.phone, "name" : name, "sex":sex] ] }
        return iikoHelper.setCustomer(json: par)
    }
    
    func setFCM() {
        smartHelper.postFCMTokenToSR(customer: customerHelper, token: pushHelper.getFcmToken() ?? "no token")
    }
    
    func addAddress(street: String, home: String, entrance: String = "", apartment: String = "", floor: String = "", doorphone: String = "", comment: String = "") -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.addAdress(street: street, home: home, entrance: entrance, apartment: apartment,
                                                                               floor: floor, doorphone: doorphone, comment: comment, phone: self.customerHelper.phone)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func updateAddress(id: Int64, street: String, home: String, entrance: String = "", apartment: String = "", floor: String = "", doorphone: String = "", comment: String = "") -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.updateAdress(street: street, home: home, entrance: entrance, apartment: apartment,
                                                                                  floor: floor, doorphone: doorphone, comment: comment, id: id)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func removeAddress(id: Int64) -> Observable<Any?> {
        return Observable.create{ obs -> Disposable in self.dbHelper.removeAdress(id: id)
            obs.onNext(nil); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getAddress() -> Observable<[Address]> {
        return Observable.create{ obs -> Disposable in obs.onNext(self.dbHelper.getAdress(phone: self.customerHelper.phone)); obs.onCompleted(); return Disposables.create { }}.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getStreets() -> Observable<iikoStreets> {
        return iikoHelper.getStreets()
    }
    
    func checkAddress(street: String, home: String) -> Observable<Bool> {
        return iikoHelper.checkAdress(address:  iikoAddress(street: street, home: home, entrance: "", apartment: "", floor: "", doorphone: "", comment: ""))
    }
    
    func getHistory() -> Observable<[IikoDeliveryHistory]> {
        return iikoHelper.getHistory(phone: customerHelper.phone)
    }
    
    func getTerminals() -> Observable<IikoTerminals> {
        return iikoHelper.getTerminals()
    }
    
    func getPushs() -> Observable<[(key:String,value:[smartPush])]> {
        return smartHelper.getPushs(phone: customerHelper.phone)
            .map({ (smartPushs) -> [(key:String,value:[smartPush])] in return self.group(smartPushs: smartPushs) })
    }
    
    private func group(smartPushs: smartPushs) -> [(key:String,value:[smartPush])] {
        var result: [String:[smartPush]] = [:]
        if smartPushs.data == nil { return [] }
        smartPushs.data.forEach { (push) in
            let t = String(push.datetime.prefix(10))
            if result.keys.contains(t){ result[t]?.append(push) }
            else{ result.updateValue([push], forKey: t) }
            if (smartPush.getFrom(text: push.orders)?.deliveryStatus == "DELIVERED" || smartPush.getFrom(text: push.orders)?.deliveryStatus == "CLOSED")
                && smartPush.getFrom(text: push.orders)!.text.contains("Спасибо") {
                var sp = smartPush()
                sp.datetime = push.datetime
                sp.phone = push.phone
                sp.orders = "{\"deliveryStatus\":\"MARK\",\"text\":\"Заказ доставлен. Пожалуйста, оцените качество нашего сервиса.\"}"
                result[t]?.append(sp)
                if self.dbHelper.isStar(id: push.id){
                    var star = smartPush()
                    star.datetime = push.datetime
                    star.phone = push.phone
                    star.orders = self.dbHelper.getStar(id: push.id)
                    result[t]?.append(star)
                }else{
                    var star = smartPush()
                    star.datetime = push.datetime
                    star.id = push.id
                    star.phone = ""
                    star.orders = push.orders
                    result[t]?.append(star)
                }
            }
        }
        let r = Array(result).sorted(by: { (o1, o2) -> Bool in
            let d = DateFormatter(); d.dateFormat = "yyyy-MM-dd"
            return d.date(from: o1.key)! < d.date(from: o2.key)!
        })
        return r
    }
    
    static func dateString(date: String)-> String{
        let d = DateFormatter(); d.dateFormat = "yyyy-MM-dd"
        let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: d.date(from: date)!)
        let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: d.date(from: date)!)
        let ma = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]
        return "\(day)\n\(ma[month-1])"
    }
    
    func getTime() -> Observable<Int64> {
        return smartHelper.getTime()
    }
    
    func getRestorans() -> Observable<[smartRestorans]> {
        return smartHelper.getRestorans()
    }
    
    func getSalesNomen() -> Observable<[Products]> {
        return smartHelper.getSalesNomen()
    }
    
    func getSales() -> Observable<Bool> {
        return smartHelper.getSales()
    }
    
    func addOrderFinale(_ order: IikoOrders) -> Observable<Bool> {
        return Observable.zip(iikoHelper.add(order: order),smartHelper.add(order: order))
            .map{ (o) -> Bool in return o.0 || o.1 }
    }
    
    func sberPay(amount: Int64) -> Observable<String> {
        return smartHelper.getTime()
            .concatMap { (time) -> Observable<String> in
                return self.smartHelper.sberPay(amount: amount, orderNumber: time) }
    }
    
    func applePay(token: String) -> Observable<String> {
        return smartHelper.getTime()
            .concatMap { (time) -> Observable<String> in
                return self.smartHelper.applePay(token: token, orderNumber: time) }
    }
    
    func star(comment: String, deliveryId: String, time: String, id: String, f: Int, s: Int, t: Int) -> Observable<Bool> {
        var text = "Оставил отзыв. Спасибо за доставку!"
        if comment.count > 3 { text = comment }
        return iikoHelper.star(comment: text, deliveryId: deliveryId, f: f, s: s, t: t)
            .do(onNext: { b in
                if b { self.dbHelper.add(Star(id: id, time: time, text: text)) }
            })
    }
    
    func checkSale(_ order: IikoOrders) -> Observable<String?> {
        return iikoHelper.checkSale(order: order)
    }
    
    func version() -> Observable<Bool> {
        return smartHelper.version()
    }
}
