//
//  OK_BasketButton.swift
//  Okinava
//
//  Created by _ on 11.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_BasketButton: ZFRippleButton {

    var isWithShadow: Bool = true
    let notifyView = UIView()
    override func draw(_ rect: CGRect) {
        
        self.setImage(#imageLiteral(resourceName: "icon_basket"), for: .normal)
        self.backgroundColor = .white
        
        
        self.layer.cornerRadius = 4
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 0
        self.layer.backgroundColor = UIColor.clear.cgColor
        if isWithShadow {
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 8
        }
        
        self.addSubview(notifyView)
        notifyView.alpha = 0
        notifyView.backgroundColor = UIColor().hexStringToUIColor(0xFF4748)
        notifyView.layer.cornerRadius = 4
        notifyView.layer.shadowColor = UIColor().hexStringToUIColor(0xFF4748).cgColor
        notifyView.layer.shadowOpacity = 0.5
        notifyView.layer.shadowRadius = 2
        
        notifyView.layer.shadowOffset = CGSize(width: 0, height: 1)
        notifyView.snp.makeConstraints { (make) in
            make.width.height.equalTo(8)
            make.right.equalToSuperview().offset(4)
            make.top.equalToSuperview().offset(-4)
        }
        
        self.addTarget(self, action: #selector(goToBasket), for: .touchUpInside)
        _ = DataManager.shared.getOrderCount().observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { count in self.setNotify(visible: count != 0) })
        setNotify (visible: false)
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_CLEAR, object: nil, queue: OperationQueue.main){ notification in self.setNotify(visible: false) }
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_ADD, object: nil, queue: OperationQueue.main){ notification in self.setNotify(visible: true) }
    }
    
    public func setNotify (visible: Bool){
        self.alpha = visible ? 1 : 0
        self.notifyView.alpha = visible ? 1 : 0
    }
    
    @objc func goToBasket() {
        NavigationHelper.shared.showNext(CartViewController())
    }

}
