//
//  OK_TopNavigationBlock.swift
//  Okinava
//
//  Created by _ on 13.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
class OK_TopNavigationBlock: UIView {
    
    
    var leftButton: UIButton = UIButton()
    var title: UILabel = UILabel()
    var rightButton: UIButton = UIButton()
    public var navigationTitle: String? {
        set { self.title.text = newValue}
        get { return self.title.text }
        
    }
    
    public func prepareView(){
        self.backgroundColor = .white
        self.addSubview(self.leftButton)
        self.addSubview(self.title)
        self.addSubview(self.rightButton)
        
        
        self.leftButton.backgroundColor = .clear
        self.leftButton.layer.shadowOpacity = 0
        
        self.rightButton.backgroundColor = .clear
        self.rightButton.layer.shadowOpacity = 0
        
        self.title.font = FONT_ROBOTO_MEDIUM?.withSize(19)
        self.title.textAlignment = .center
        
        self.title.adjustsFontSizeToFitWidth = true
        
        self.leftButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(32)
        }
        self.rightButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(32)
        }
        self.title.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
    }
    override func draw(_ rect: CGRect) {
        
    }
    
}
