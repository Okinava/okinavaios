//
//  PromoScreenViewController.swift
//  Okinava
//
//  Created by _ on 22.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class PromoScreenViewController: SwipeRightToPopViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    private var promos: [smartPromo]
    private var promo: smartPromo
    private let backButton = OK_BackButton()
    private let shareButton = OK_ShareButton()
    private lazy var mainSlider: UICollectionView = {
        var mainCollectionViewLayout =  UICollectionViewFlowLayout.init()
        mainCollectionViewLayout.minimumLineSpacing = 0
        mainCollectionViewLayout.minimumInteritemSpacing = 0
        mainCollectionViewLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: mainCollectionViewLayout)
        view.setCollectionViewLayout(mainCollectionViewLayout, animated: true)
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.backgroundColor = .white
        view.register(PromoCell.self, forCellWithReuseIdentifier: PromoCellID)
        view.delegate = self
        view.dataSource = self
        view.isPagingEnabled = true
        return view
    }()

    init(promo: smartPromo, promos: [smartPromo]) {
        self.promos = [promo]
        self.promo = promo
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor  = .white
        view.addSubview(mainSlider)
        view.addSubview(backButton)
        view.addSubview(shareButton)
        backButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(share), for: .touchUpInside)
        updateViewConstraints()
//        if let p = promos.enumerated().first(where: {$0.element.id == promo.id}){
//            _ = Observable<Int>.timer(0.5, scheduler: ThreadUtil.shared.backScheduler)
//                .observeOn(ThreadUtil.shared.mainScheduler)
//                .subscribe(onNext: {t in
//                    self.mainSlider.scrollToItem(at: IndexPath(row: p.offset, section: 0), at: .right, animated: true)
//                })
//        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        mainSlider.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
            make.top.equalToSuperview().offset(-10)
        }
        backButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.left.equalToSuperview().offset(16)
        }
        shareButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return promos.count }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromoCellID, for: indexPath) as! PromoCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard case let cell as PromoCell = cell else { return }
        cell.setData(promo: promos[indexPath.row], index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: collectionView.frame.height)
    }
    
    @objc private func share(){
        guard case let cell as PromoCell = mainSlider.visibleCells[0] else { return }
        let firstActivityItem = cell.productTitleLabel.text!
        let secondActivityItem : NSURL = NSURL(string: "http//okinavakazan.ru")!
        let image : UIImage = cell.headerImageView.image!
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.saveToCameraRoll
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }

}
