//
//  DBModel.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite

class DB {
    private let VERSION: Int64 = 7
    let db: Connection
    let groups: Table
    let products: Table
    let modifiers: Table
    let favourites: Table
    let orders: Table
    let address: Table
    let star: Table

    init() {
        let path : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        db = try! Connection("\(path)/oki.sqlite3")
        if db.userVersion != VERSION {
            db.userVersion = VERSION
            try! db.run(Groups.createTable(db).drop(ifExists: true))
            try! db.run(Products.createTable(db).drop(ifExists: true))
            try! db.run(Modifiers.createTable(db).drop(ifExists: true))
            try! db.run(Favourites.createTable(db).drop(ifExists: true))
            try! db.run(Orders.createTable(db).drop(ifExists: true))
            try! db.run(Address.createTable(db).drop(ifExists: true))
            try! db.run(Star.createTable(db).drop(ifExists: true))
        }
        groups = Groups.createTable(db)
        products = Products.createTable(db)
        modifiers = Modifiers.createTable(db)
        favourites = Favourites.createTable(db)
        orders = Orders.createTable(db)
        address = Address.createTable(db)
        star = Star.createTable(db)
    }
    
    func getTable(_ tableName: String) -> Table{
        switch tableName{
        case Groups.tableName(): return groups
        case Products.tableName(): return products
        case Modifiers.tableName(): return modifiers
        case Orders.tableName(): return orders
        case Favourites.tableName(): return favourites
        case Address.tableName(): return address
        case Star.tableName(): return star
        default: return products
        }
    }
}

class DBMapper {
    static func cast(group g: IikoGroup) -> Groups{
        return Groups(id: g.id, name: g.name, order: g.order, parentGroup: g.parentGroup,
                      image: g.images.count == 0 ? "" : g.images[0].imageUrl, isIncludedInMenu: g.isIncludedInMenu)
    }
    
    static func cast(product p: IikoProduct) -> Products{
        return Products(id: p.id, name: p.name, code: p.code, product_description: p.product_description, order: p.order, parentGroup: p.parentGroup, groupId: p.groupId, productCategoryId: p.productCategoryId, price: p.price, carbohydrateAmount: p.carbohydrateAmount, energyAmount: p.energyAmount, fatAmount: p.fatAmount, fiberAmount: p.fiberAmount, carbohydrateFullAmount: p.carbohydrateFullAmount, energyFullAmount: p.energyFullAmount, fatFullAmount: p.fatFullAmount, fiberFullAmount: p.fiberFullAmount, weight: p.weight, type: p.type, isIncludedInMenu: p.isIncludedInMenu, measureUnit: p.measureUnit, doNotPrintInCheque: p.doNotPrintInCheque, useBalanceForSell: p.useBalanceForSell,
                        image: p.images.count == 0 ? "" : p.images[0].imageUrl)
    }
    
    static func cast(modifiers m: IikoModifier, productId: String, modifiersId: String) -> Modifiers{
        return Modifiers(id: m.modifierId, maxAmount: m.maxAmount, minAmount: m.minAmount, defaultAmount: m.defaultAmount, modifiersId: modifiersId, parentProduct: productId)
    }
    
    static func cast(nomenclature n: IikoNomenclature) -> [SQLCommand]{
        var sqls: [SQLCommand] = []
        n.groups.forEach{ g in sqls.append( cast(group: g) )}
        n.products.forEach{ p in
            sqls.append( cast(product: p) )
            if let m = p.groupModifiers.first {
                m.childModifiers.forEach{ cm in
                    sqls.append( cast(modifiers: cm, productId: p.id, modifiersId: m.modifierId) )
                }
            }
        }
        return sqls
    }
}
















