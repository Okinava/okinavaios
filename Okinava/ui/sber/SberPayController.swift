//
//  SberPayController.swift
//  Okinava
//
//  Created by Timerlan on 08.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

class SberPayController: BaseController<SberPayView> , UIWebViewDelegate, NSURLConnectionDelegate  {
    private let url: String
    private let order: IikoOrders
    
    init( url: String, order: IikoOrders ){
        self.url = url
        self.order = order
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xf4f4f4)
        mainView.webView.delegate = self
        mainView.webView.backgroundColor = UIColor().hexStringToUIColor(0xf4f4f4)
        mainView.webView.loadRequest(URLRequest(url: URL(string: url)!));
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if (request.url?.absoluteString.contains("smart-resto-success"))!{
            let spiner = self.showModalSpinner()
            _ = DataManager.shared.addOrderFinale(order)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { result in
                    self.hideModalSpinner(indicator: spiner)
                    if result {
                        NavigationHelper.shared.showNext(ThankYouViewController())
                    }else{
                        Crashlytics.sharedInstance().recordError(IikoError.init("код оплаты \(SmartHelper.orderNumber ?? 0)"), withAdditionalUserInfo: self.order.toJSON())
                        AlertUtil.alert({}, mes: "К сожалению, нам не удалось создать заказ. Ваш код оплаты \(SmartHelper.orderNumber ?? 0). Позвоните нам и сообщите этот код для завершения создания заказа \n8 (843) 233-44-33")
                    }
                })
        }else if (request.url?.absoluteString.contains("smart-resto-error"))!{
            AlertUtil.alert({
                NavigationHelper.shared.back()
            }, mes: "Не удалось совершить оплату. Попробуйте повторить позже!")
        }
        return true
    }
}
