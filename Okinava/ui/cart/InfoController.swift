//
//  InfoController.swift
//  Okinava
//
//  Created by Timerlan on 30.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation


import UIKit
import SnapKit

class InfoController: BaseController<InfoView> {
    override func loadView() {
        super.loadView()
        mainView.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
}

class InfoView: BaseView{
    lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.prepareView()
        view.navigationTitle = "Информации о доставке"
        view.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        view.rightButton.removeFromSuperview()
        return view
    }()
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.addSubview(label)
        return view
    }()
    lazy var label: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.text = te
        return view
    }()
    
    override func setContent() {
        super.setContent()
        contentView.addSubview(topNavigationView)
        contentView.addSubview(scrollView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        topNavigationView.snp.remakeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.topMargin)
        }
        scrollView.snp.remakeConstraints{ make in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.right.left.bottom.width.equalToSuperview()
        }
        label.snp.remakeConstraints{ make in
            make.top.left.equalToSuperview().offset(16)
            make.right.bottom.equalToSuperview().offset(-16)
            make.width.equalToSuperview().offset(-32)
        }
    }
    
    lazy var te = "УСЛОВИЯ ДОСТАВКИ" +
        "\n\n" +
        "Прием заказов" +
        "\n\n" +
        "Вс-Чт     с 10:00 до 00:20" +
        "\n\n" +
    "Пт, Сб   с 10:00 до 01:20" +
    "\n\n" +
    "Минимальная сумме заказа 600 рублей - доставка бесплатная." +
    "\n\n" +
    "Наши кафе есть в каждом районе города, поэтому  среднее время доставки 60 минут." +
    "\n\n" +
    "У нас Вы можете заказ еды к определенному времени." +
    "\n\n" +
    "В случае, если по приезду курьера с доставкой Клиента не выходит на связь или не открывает дверь, курьер обязан подождать 10 мин. После чего он уезжает. Время повторной доставки с Вашим заказом обговаривается с администратором." +
    "\n\n" +
    "В случае, когда Клиент оплатил заказ и не забрал его по каким либо причинам в течение 2-х часов, деньги за заказ не возвращаются. Сам заказ утилизируется, за истечением срока годности." +
    "\n\n" +
    "При заказе от 3 000 и выше оператор вправе попросить предоплату с Клиента, удобным для него способом. Время доставки заказа осуществляется с момента внесения предоплаты." +
    "\n\n" +
    "ЧЕРНЫЙ СПИСОК" +
    "\n\n" +
    "Компания вправе добавить Клиента в черный список без объяснения причины." +
    "\n\n" +
    "Причины, по которым Клиент может быть внесен в черный список компании:" +
    "\n\n" +
    "•             Отказ от оплаты готового заказа;" +
    "\n\n" +
    "•             Указание заведомо неверных данных;" +
    "\n\n" +
    "•             Неадекватное поведение или действие гостя;" +
    "\n\n" +
    "•             Причинение морального или физического ущерба организации или ее сотрудникам;" +
    "\n\n" +
    "•             Злоупотребление лояльностью компании;" +
    "\n\n" +
    "•             Мошеннические действия по отношению к компании." +
    "\n\n" +
    "У Клиента есть возможность «выйти» из черного списка компании путем оформления заказа и оплаты его on-line, без совершения конфликтных ситуаций." +
    "\n\n" +
    "ОПЛАТА ЗАКАЗА" +
    "\n\n" +
    "Оплатить заказ можно 2 способами:" +
    "\n\n" +
    "·         Наличными" +
    "\n\n" +
    "·         On-line" +
    "\n\n" +
    "К оплате принимаются платёжные системы Visa, MasterCard, Мир." +
    "\n\n" +
    "Доставка или выдача при самовывозе товара, оплаченного пластиковой картой, осуществляется от времени зачисления денег на счет компании." +
    "\n\n" +
    "Отмена заказа" +
    "\n\n" +
    "При удалении товаров из оплаченного заказа или при аннулировании заказа целиком Вы можете заказать другой товар на такую же сумму, либо полностью вернуть всю сумму на карту. Возврат денежный средств осуществляется в течение 3 –х рабочих календарных дней, далее в зависимости от банка Клиента."
}
