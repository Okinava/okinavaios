//
//  iikoAdres.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//
import Gloss

//MARK: iikoAddress
public class iikoAddress:  Glossy  {
    var street: String = ""
    var home: String = ""
    var entrance: String = ""
    var apartment: String = ""
    var floor: String = ""
    var doorphone: String = ""
    var comment: String = ""
    
    init(street: String, home: String, entrance: String, apartment: String, floor: String, doorphone: String, comment: String) {
        self.street = street
        self.home = home
        self.entrance = entrance
        self.apartment = apartment
        self.floor = floor
        self.doorphone = doorphone
        self.comment = comment
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let street : String = "street" <~~ json {  self.street = street }
        if let home : String = "home" <~~ json {  self.home = home }
        if let entrance : String = "entrance" <~~ json {  self.entrance = entrance }
        if let apartment : String = "apartment" <~~ json {  self.apartment = apartment }
        if let floor : String = "floor" <~~ json {  self.floor = floor }
        if let doorphone : String = "doorphone" <~~ json {  self.doorphone = doorphone }
        if let comment : String = "comment" <~~ json {  self.comment = comment }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "city" ~~> "Казань",
            "street" ~~> street,
            "home" ~~> home,
            "entrance" ~~> entrance,
            "apartment" ~~> apartment,
            "floor" ~~> floor,
            "doorphone" ~~> doorphone,
            "comment" ~~> comment,
            ])
    }
}
