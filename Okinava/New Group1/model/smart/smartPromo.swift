//
//  smartPromo.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import Gloss

//MARK: - smartPromo
public struct smartPromo: Glossy {
    
    public var actionOrder : String!
    public var active : String!
    public var descriptionField : String!
    public var howItMake : [HowItMake]!
    public var id : String!
    public var image : String!
    public var title : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        actionOrder = "action_order" <~~ json
        active = "active" <~~ json
        descriptionField = "description" <~~ json
        howItMake = "how_it_make" <~~ json
        id = "id" <~~ json
        image = "image" <~~ json
        title = "title" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "action_order" ~~> actionOrder,
            "active" ~~> active,
            "description" ~~> descriptionField,
            "how_it_make" ~~> howItMake,
            "id" ~~> id,
            "image" ~~> image,
            "title" ~~> title,
            ])
    }
    
}


//MARK: - HowItMake
public struct HowItMake: Glossy {
    
    public var actionDesc : String!
    public var actionTitle : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        actionDesc = "action_desc" <~~ json
        actionTitle = "action_title" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "action_desc" ~~> actionDesc,
            "action_title" ~~> actionTitle,
            ])
    }
    
}
