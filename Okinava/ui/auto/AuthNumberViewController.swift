//
//  AuthNumberViewController2.swift
//  Okinava
//
//  Created by _ on 20.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import InputMask

class AuthNumberViewController: SwipeRightToPopViewController {
    private let topNavigationView = OK_TopNavigationBlock()
    private let numberInputWrapper = UIView()
    private let controlsWrapper  = UIView()
    private let numberTitleLabel = UILabel()
    private let numberInputTextField = UITextField()
    private let nextButton = ZFRippleButton()
    private let nextButtonForAccessory = ZFRippleButton()
    private let agreementTextLabel = UILabel()
    private var userPhoneNumber = ""
    var maskedDelegate: MaskedTextFieldDelegate!
    private var need: Bool
    init(need: Bool) {
        self.need = need
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.prepareView()
        self.title = "Профиль"
        self.hideKeyboardWhenTappedAround()
    }
    
    private func prepareView(){
        self.view.backgroundColor = .white
        self.view.addSubview(self.topNavigationView)
        self.view.addSubview(self.numberInputWrapper)
        self.view.addSubview(self.controlsWrapper)
        
        self.numberInputWrapper.addSubview(self.numberTitleLabel)
        self.numberInputWrapper.addSubview(self.numberInputTextField)
        
        self.controlsWrapper.addSubview(self.nextButton)
        self.controlsWrapper.addSubview(self.agreementTextLabel)
        
        
        
        self.topNavigationView.prepareView()
        self.topNavigationView.backgroundColor = DEFAULT_BACKGROUND
        self.topNavigationView.navigationTitle = "Добавление телефона"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            if #available(iOS 11, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            } else {
                make.top.equalToSuperview().offset(16)
            }
            
        }
        
        
        self.numberInputWrapper.snp.makeConstraints { (make) in
            make.height.equalToSuperview().multipliedBy(0.5)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.topNavigationView.snp.bottom)
        }
        
        self.controlsWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(self.numberInputWrapper.snp.bottom).offset(20)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        
        
        self.numberTitleLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().multipliedBy(1)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        self.numberInputTextField.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.8)
            make.centerX.equalToSuperview()
            make.top.equalTo(self.numberTitleLabel.snp.bottom).offset(5)
            
        }
        self.numberTitleLabel.textAlignment = .center
        self.numberTitleLabel.text = "Ваш номер телефона"
        self.numberTitleLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.numberTitleLabel.font = FONT_ROBOTO_REGULAR?.withSize(14)
        
        self.numberInputTextField.textAlignment = .center
        self.numberInputTextField.font = FONT_ROBOTO_MEDIUM?.withSize(22)
        self.numberInputTextField.textColor = DEFAULT_COLOR_BLACK
        
        
        self.agreementTextLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.agreementTextLabel.textAlignment = .center
        self.agreementTextLabel.numberOfLines = 0
        self.agreementTextLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        
        
        self.agreementTextLabel.text = "Нажимая «Далее», вы соглашаетесь со сбором и обработкой персональных данных"
        
        self.nextButton.setTitle("Далее", for: .normal)
        self.nextButton.setTitle("Введите номер", for: .disabled)
        self.nextButton.layer.cornerRadius = 4
        self.nextButton.backgroundColor = DEFAULT_COLOR_RED
        self.nextButtonForAccessory.setTitle("Далее", for: .normal)
        self.nextButtonForAccessory.setTitle("Введите номер", for: .disabled)
        self.nextButtonForAccessory.layer.cornerRadius = 4
        self.nextButtonForAccessory.backgroundColor = DEFAULT_COLOR_RED
        
        self.nextButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().inset(16)
            make.height.equalTo(45)
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            
        }
        self.agreementTextLabel.snp.makeConstraints { (make) in
            make.width.equalTo(self.nextButton.snp.width)
            make.top.equalTo(self.nextButton.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
            
        }
        
        
        
        maskedDelegate = MaskedTextFieldDelegate(primaryFormat: "{+7} ([000]) [000] [00] [00]")
        maskedDelegate.listener = self
        self.numberInputTextField.delegate = maskedDelegate
        self.numberInputTextField.keyboardType = .numberPad
        maskedDelegate.put(text: "+7", into: self.numberInputTextField)
        
        self.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        self.nextButtonForAccessory.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        
        
        self.nextButtonForAccessory.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45)
        self.numberInputTextField.inputAccessoryView = self.nextButtonForAccessory
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    @objc private func nextButtonTapped(){
        navigationController?.show(AuthCodeViewController(phone: userPhoneNumber, need: self.need), sender: nil)
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    @objc func keyboardWasShown(notification: NSNotification){ nextButton.alpha = 0 }
    @objc  func keyboardWillBeHidden(notification: NSNotification){ nextButton.alpha = 1 }
}
extension AuthNumberViewController: MaskedTextFieldDelegateListener {
    open func textField(
        _ textField: UITextField,
        didFillMandatoryCharacters complete: Bool,
        didExtractValue value: String
        ) {
        if (complete ) {
            self.nextButton.backgroundColor = DEFAULT_COLOR_RED
            self.nextButton.isEnabled = true
            
            self.nextButtonForAccessory.backgroundColor = DEFAULT_COLOR_RED
            self.nextButtonForAccessory.isEnabled = true
            
            
            self.userPhoneNumber = value
            self.userPhoneNumber.removeFirst(2)
            
        } else {
            self.nextButton.backgroundColor = DEFAULT_COLOR_GRAY
            self.nextButton.isEnabled = false
            self.nextButtonForAccessory.backgroundColor = DEFAULT_COLOR_GRAY
            self.nextButtonForAccessory.isEnabled = false
            self.userPhoneNumber = ""
        }
        
    }
}
