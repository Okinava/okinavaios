//
//  SearchStreetController.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import SnapKit

class SearchStreetController: SwipeRightToPopViewController, UITableViewDataSource, UITableViewDelegate {
    private let searchView = UIView()
    private let searchBackButton  = OK_BackButton()
    private let searchTextField  = UITextField()
    private let resultsTableView = UITableView()
    private var searchString = ""
    private var streets : [iikoStreet] = []
    private var filStreets : [iikoStreet] = []
    private var void: ((_ text: String)->Void)
    
    init(void: @escaping ((_ text: String)->Void)){
        self.void = void
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        self.loadData()
        self.resultsTableView.delegate = self
        self.resultsTableView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.resultsTableView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize.height) }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.resultsTableView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
        }
    }
    private func loadData(){
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.getStreets()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ s in
                self.hideModalSpinner(indicator: spiner)
                self.streets = s.streets;
                self.filterProducts()
            }, onError: { t in
                self.hideModalSpinner(indicator: spiner)
                AlertUtil.alert({ self.navigationController?.popViewController(animated: true)
                }, mes: t.localizedDescription)
            })
    }
    
    private func prepareView(){
        self.view.backgroundColor = .white
        self.resultsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        self.view.addSubview(self.searchView)
        self.searchView.addSubview(self.searchBackButton)
        self.searchView.addSubview(self.searchTextField)
        self.view.addSubview(self.resultsTableView)
        self.searchBackButton.isWithShadow = false
        self.searchView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().inset(16)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset( UIApplication.shared.statusBarFrame.height)
            make.height.equalTo(40)
        }
        self.searchBackButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.height.width.equalTo(32)
            make.centerY.equalToSuperview()
        }
        self.searchTextField.snp.makeConstraints { (make) in
            make.left.equalTo(self.searchBackButton.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.height.equalTo(36)
            make.right.equalToSuperview()
        }
        self.resultsTableView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.searchView.snp.bottom).offset(5)
            make.bottom.equalToSuperview()
        }
        self.searchBackButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        self.searchTextField.placeholder = "Введите адрес"
        self.searchTextField.delegate = self
        self.searchTextField.textColor = DEFAULT_COLOR_BLACK
        self.searchTextField.font = FONT_ROBOTO_MEDIUM?.withSize(20)
        self.resultsTableView.separatorStyle = .singleLine
        self.searchTextField.becomeFirstResponder()
    }
    
    private func filterProducts(){
        filStreets = streets.filter({ $0.name.lowercased().contains(searchString.lowercased()) || searchString.count == 0})
        UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.resultsTableView.reloadData() })
    }
    
    @objc func backButtonTapped (){ self.navigationController?.popViewController(animated: true) }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return filStreets.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.textLabel?.text = filStreets[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        void(filStreets[indexPath.row].name)
        navigationController?.popViewController(animated: true)
    }
}
extension SearchStreetController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.searchTextField) {
            self.searchString = textField.text! + string
            self.filterProducts()
        }
        return true
    }
}


