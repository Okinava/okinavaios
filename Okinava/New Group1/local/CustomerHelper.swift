//
//  CustomerHelper.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation


class CustomerHelper {
    private let defaults = UserDefaults.standard
    private let PHONE = "PHONE"
    private let NAME = "NAME"
    private let STATUS = "STATUS"
    private let BONUS = "BONUS"
    private let CARD = "CARD"
    private let SEX = "SEX"
    private let DATE = "DATE"
    private let ID = "ID"
    private let PROMO = "PROMO"
    private let MAKR = "MAKR"

    
    var id : String {
        get { return defaults.string(forKey: ID) ?? "" }
        set { set(value: newValue, forKey: ID)}
    }
    
    var phone : String {
        get { return defaults.string(forKey: PHONE) ?? "" }
        set { set(value: newValue, forKey: PHONE)}
    }
    
    var name : String {
        get { return defaults.string(forKey: NAME) ?? "Нет имени" }
        set { set(value: newValue, forKey: NAME)}
    }
    
    var status : String {
        get { return defaults.string(forKey: STATUS) ?? "Новый" }
        set { set(value: newValue, forKey: STATUS)}
    }
    
    var bonus : Double{
        get{ return defaults.double(forKey: BONUS) }
        set{ set(value: newValue, forKey: BONUS) }
    }
    
    var card : String{
        get{ return defaults.string(forKey: CARD) ?? "Нет карты" }
        set{ set(value: newValue, forKey: CARD) }
    }
    
    var sex : Int{
        get{ return defaults.integer(forKey: SEX) }
        set{ set(value: newValue, forKey: SEX) }
    }
    
    var date : String{
        get{ return defaults.string(forKey: DATE) ?? "Дата рождения не указана"}
        set{ set(value: newValue, forKey: DATE) }
    }
    
    var promo : String{
        get{ return defaults.string(forKey: PROMO) ?? ""}
        set{ set(value: newValue, forKey: PROMO) }
    }
    
    var mark : Bool{
        get{ return defaults.bool(forKey: MAKR) }
        set{ set(value: newValue, forKey: MAKR) }
    }
    
    func setAll(customer c: iikoCustomer){
        id = c.id
        name = c.name
        if let _ = c.birthday { date = c.birthday! }
        sex = c.sex
        c.categories.forEach { (c) in status = c.name }
        c.walletBalances.forEach { (wb) in bonus = wb.balance }
        c.cards.forEach { (ca) in card = ca.Number }
        NotificationCenter.default.post(name: NotificationUtil.CUSTOMER, object: nil)
    }
    
    func setDefault(phone p: String){
        id = ""
        phone = p
        name = "Нет имени"
        date = "Дата рождения не указана"
        sex = 0
        status = "Новый"
        bonus = 0
        card = "Нет карты"
        NotificationCenter.default.post(name: NotificationUtil.CUSTOMER, object: nil)
    }
    
    private func set(value: Any, forKey: String) {
        defaults.set(value, forKey: forKey)
        defaults.synchronize()
    }
}
