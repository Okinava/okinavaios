//
//  ChatController.swift
//  Okinava
//
//  Created by Timerlan on 05.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import RxSwift
import HCSStarRatingView

class ChatController: BaseController<ChatView>, UITableViewDataSource, UITableViewDelegate {
    var items: [(key:String,value:[smartPush])] = []
    
    override func loadView() {
        super.loadView()
        mainView.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        mainView.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshData()
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    @objc func refreshData(){
        mainView.refreshControl.beginRefreshing()
        _ = DataManager.shared.getPushs()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { pushs in
                self.mainView.refreshControl.endRefreshing()
                self.items = pushs
                self.mainView.tableView.dataSource = self
                self.mainView.tableView.delegate = self
                self.mainView.tableView.reloadData()
                _ = Observable<Int>.timer(0.01, scheduler: ThreadUtil.shared.backScheduler)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe(onNext: { t in
                        if self.items.count == 0 { return }
                        self.mainView.tableView.scrollToRow(at: IndexPath(row: self.items.last!.value.count - 1, section: self.items.count - 1), at: .none, animated: false)
                    })
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int { return items.count }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return items[section].value.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatCellId, for: indexPath as IndexPath) as! ChatCell
        cell.info = items[indexPath.section].value[indexPath.row]
        cell.cs = self
        if items[indexPath.section].value[indexPath.row].phone.count != 0 {
            if let push = smartPush.getFrom(text: items[indexPath.section].value[indexPath.row].orders){
                cell.label.text = push.text
                cell.fchat()
                cell.time.text = String(items[indexPath.section].value[indexPath.row].datetime.suffix(8).prefix(5))
            }else{
                cell.label.text = items[indexPath.section].value[indexPath.row].orders
                cell.schat()
                cell.time.text = String(items[indexPath.section].value[indexPath.row].datetime.suffix(8).prefix(5))
            }
            return cell
        }
        cell.star()
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = DataManager.dateString(date: items[section].key)
        label.textAlignment = .center
        label.backgroundColor = .white
        label.numberOfLines = 0
        label.textColor = DEFAULT_COLOR_DARK_GRAY
        label.font = FONT_ROBOTO_REGULAR?.withSize(16)
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

class ChatView: BaseView {
    lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.prepareView()
        view.navigationTitle = "Чат"
        view.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        view.rightButton.removeFromSuperview()
        return view
    }()
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.addSubview(refreshControl)
        view.separatorStyle = .none
        view.alwaysBounceVertical = true
        view.rowHeight = UITableView.automaticDimension
        view.register(ChatCell.self, forCellReuseIdentifier: ChatCellId)
        view.backgroundColor = .white
        return view
    }()
    lazy var refreshControl = UIRefreshControl()
    override func setContent() {
        super.setContent()
        contentView.addSubview(topNavigationView)
        contentView.addSubview(tableView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        topNavigationView.snp.remakeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.topMargin)
        }
        tableView.snp.remakeConstraints{ make in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.right.left.bottom.width.equalToSuperview()
        }
    }
}

let ChatCellId = "ChatCellId"
class ChatCell: UITableViewCell {
    var info: smartPush?
    var cs: ChatController?
//    private lazy var starView = JNStarRateView.init(frame: CGRect(x: 0,y: 0,width: 220,height: 30))//默认的是5颗星，分数为0分
    private lazy var starView: HCSStarRatingView = {
        let view = HCSStarRatingView()
        view.tintColor = DEFAULT_COLOR_RED
        view.addTarget(self, action: #selector(didChangeValue(sender:)), for: UIControl.Event.valueChanged)
        return view
    }()
    @objc private func didChangeValue(sender: Any){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        let i = info!
        NavigationHelper.shared.showNext(MarkController(info: i, void: {
            UIView.transition(with: self.cs!.mainView, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                _ = DataManager.shared.getPushs()
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe(onNext: { pushs in
                        self.cs!.items = pushs
                        self.cs!.mainView.tableView.reloadData()
                    })
            })
        }))
    }
    private lazy var lineT : UIView = { let view = UIView(); view.backgroundColor = DEFAULT_COLOR_GRAY; return view }()
    private lazy var lineB : UIView = { let view = UIView(); view.backgroundColor = DEFAULT_COLOR_GRAY; return view }()
    private lazy var cont: UIView = {
        let view = UIView()
        view.layer.shadowRadius = 4
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowOpacity = 0.1
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        return view
    }()
    lazy var label: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textColor = DEFAULT_COLOR_GRAY
        view.font = FONT_ROBOTO_REGULAR?.withSize(16)

//        view.lineBreakMode = .byCharWrapping
        return view
    }()
    lazy var time: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.numberOfLines = 1
        return view
    }()
    
    func star(){
        cont.removeFromSuperview()
        time.removeFromSuperview()
        addSubview(starView)
        addSubview(lineB)
        addSubview(lineT)
        lineT.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(10)
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        }
        starView.snp.remakeConstraints{ make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(30)
            make.bottom.equalToSuperview().offset(-30)
            make.height.equalTo(40)
            make.width.equalTo(280)
        }
        lineB.snp.remakeConstraints{ make in
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    func fchat(){
        starView.removeFromSuperview()
        lineT.removeFromSuperview()
        lineB.removeFromSuperview()
        addSubview(cont)
        cont.addSubview(label)
        addSubview(time)
        backgroundColor = .white
        selectionStyle = .none
        cont.snp.remakeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
            make.right.lessThanOrEqualToSuperview().offset(-40)
            make.bottom.equalToSuperview().offset(-25)
        }
        label.snp.remakeConstraints{ make in
            make.top.left.equalToSuperview().offset(15)
            make.right.bottom.equalToSuperview().offset(-15)
        }
        time.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(0)
        }
        cont.layer.backgroundColor = UIColor.white.cgColor
        label.textColor = DEFAULT_COLOR_DARK_GRAY
    }
    
    func schat(){
        starView.removeFromSuperview()
        lineT.removeFromSuperview()
        lineB.removeFromSuperview()
        addSubview(cont)
        cont.addSubview(label)
        addSubview(time)
        backgroundColor = .white
        selectionStyle = .none
        cont.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.greaterThanOrEqualToSuperview().offset(40)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-25)
        }
        label.snp.remakeConstraints{ make in
            make.top.left.equalToSuperview().offset(15)
            make.right.bottom.equalToSuperview().offset(-15)
        }
        time.snp.remakeConstraints{ make in
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(0)
        }
        cont.layer.backgroundColor = DEFAULT_COLOR_RED.cgColor
        label.textColor = .white
    }
}
