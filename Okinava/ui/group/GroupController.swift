//
//  GroupController.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import RxSwift


class GroupController: BaseController<GroupView>, UITableViewDataSource, UITableViewDelegate, UIViewControllerPreviewingDelegate {
    private var currentPageIndex: Int = 0;
    fileprivate var tabsViewList: [ViewPagerTabView] = []
    private var isIndicatorAdded = false
    fileprivate lazy var tabIndicator : UIView = { let view = UIView(); view.backgroundColor = UIColor.black; return view; }()
    fileprivate var products: [Products] = []
    fileprivate var groups: [Groups] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Observable.zip(DataManager.shared.getGroups(),DataManager.shared.getProducts())
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { (gs,ps) in self.set(groups: gs, products: ps) })
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_ADD, object: nil, queue: OperationQueue.main){ notification in
            self.update(o: notification.object as! Orders)
        }
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_REMOVE, object: nil, queue: OperationQueue.main){ notification in
            self.update(o: notification.object as! Orders)
        }
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_UPDATE, object: nil, queue: OperationQueue.main){ notification in
            self.update(o: notification.object as! Orders)
        }
        NotificationCenter.default.addObserver(forName: NotificationUtil.FAVOURITES_UPD, object: nil, queue: OperationQueue.main){ notification in self.reload()}
        NotificationCenter.default.addObserver(forName: NotificationUtil.CUSTOMER, object: nil, queue: OperationQueue.main){ notification in self.bonus() }        
    }
    
    private func bonus(){ mainView.bonusCountLabel.text = String(Int(DataManager.shared.customerHelper.bonus)) + "" }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    func scrollTableViewToTop() {
        _ = DataManager.shared.getProducts().observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { p in
                self.products = p
                self.mainView.tableView.reloadData()
                self.mainView.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            })
    }
    
    private func update(o: Orders){
        mainView.basketButton.setNotify(visible: true)
        products.enumerated().forEach { (p) in
            if p.element.id == o.productId { mainView.tableView.reloadRows(at: [IndexPath(row: p.offset, section: 0)], with: .fade) }
        }
    }
    
    private var mCell: UITableViewCell?
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        mainView.scrollViewDidScroll(scrollView)
        if mainView.tableView.visibleCells.count < 3 || products.count == 0 { return }
        let ceil: Int = mainView.tableView.visibleCells.count / 2 + 1
        if mainView.tableView.visibleCells[ceil] == mCell { return }; mCell = mainView.tableView.visibleCells[ceil]
        if let pos = mainView.tableView.indexPath(for: mainView.tableView.visibleCells[ceil])?.row,
            let g = groups.enumerated().first(where: { $0.element.id == products[pos].parentGroup || $0.element.id == products[pos].groupId || $0.element.id == products[pos].productCategoryId }),
            g.offset != currentPageIndex{
            self.tabsViewList[currentPageIndex].removeHighlight()
            UIView.animate(withDuration: 0.5, animations: { self.tabsViewList[g.offset].addHighlight() })
            UIView.animate(withDuration: 0.3, animations: {
                if g.offset > self.currentPageIndex && self.tabsViewList.count > g.offset + 1 {
                    InteractionFeedbackManager.shared.makeFeedback(level: .light)
                    self.mainView.tabView.scrollRectToVisible(self.tabsViewList[g.offset + 1].frame, animated: false)
                }else if g.offset < self.currentPageIndex && -1 < g.offset - 1 {
                    InteractionFeedbackManager.shared.makeFeedback(level: .light)
                    self.mainView.tabView.scrollRectToVisible(self.tabsViewList[g.offset - 1].frame, animated: false)
                }
                self.tabIndicator.frame = CGRect(x: self.tabsViewList[g.offset].frame.origin.x, y: 47, width: self.tabsViewList[g.offset].frame.width, height: 3)
                self.tabIndicator.layoutIfNeeded()
                self.currentPageIndex = g.offset
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {return products.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath) as! ProductTableViewCell
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: cell)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as ProductTableViewCell = cell else { return }
        if (products[indexPath.row].id == "-1") { cell.setData(name: products[indexPath.row].name) }
        else{ cell.setData(product: products[indexPath.row]) }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return products[indexPath.row].id == "-1" ? 30 : 146
    }
    
    func set(groups: [Groups], products: [Products]){
        var xPosition: CGFloat = 0
        for (index,eachTab) in groups.enumerated() {
            let eachLabelWidth = CGFloat(eachTab.name.count * 12) + 40
            let tab = ViewPagerTabView()
            tab.frame = CGRect(x: xPosition, y: 0, width: eachLabelWidth, height: 50)
            tab.addTarget(self, action: #selector(tabTapped), for: .touchUpInside)
            tab.setup(eachTab.name)
            tab.tag = index
            tabsViewList.append(tab)
            mainView.tabView.addSubview(tab)
            xPosition += eachLabelWidth
        }
        mainView.tabView.contentSize = CGSize(width: xPosition, height: 50)
        setupCurrentPageIndicator(currentIndex: 0, previousIndex: currentPageIndex)
        self.products = products
        self.groups = groups
        mainView.tableView.register(ProductTableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
        mainView.tableView.reloadData()
        mainView.searchButton.addTarget(self, action: #selector(search), for: .touchUpInside)
    }
    
    @objc func search(){
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(SearchScreenViewController(), animated: true)
    }
    
    @objc func tabTapped(sender : UIButton) {
        setupCurrentPageIndicator(currentIndex: sender.tag, previousIndex: currentPageIndex)
    }
    
    fileprivate func setupCurrentPageIndicator(currentIndex: Int, previousIndex: Int) {
        self.currentPageIndex = currentIndex
        self.tabsViewList[previousIndex].removeHighlight()
        UIView.animate(withDuration: 0.5, animations: { self.tabsViewList[currentIndex].addHighlight() })
        
        let tabIndicatorFrame = CGRect(x: tabsViewList[currentIndex].frame.origin.x, y: 47, width: tabsViewList[currentIndex].frame.width, height: 3)
        if !isIndicatorAdded {
            tabIndicator.frame = CGRect(x: tabsViewList[currentIndex].frame.origin.x, y: 47, width: tabsViewList[currentIndex].frame.width, height: 3)
            mainView.tabView.addSubview(tabIndicator)
            isIndicatorAdded = true
        }
        mainView.tabView.bringSubviewToFront(tabIndicator)
        UIView.animate(withDuration: 0.3, animations: {
            if self.currentPageIndex > previousIndex && self.tabsViewList.count > self.currentPageIndex + 1 {
                self.mainView.tabView.scrollRectToVisible(self.tabsViewList[self.currentPageIndex + 1].frame, animated: false)
            }else if self.currentPageIndex < previousIndex && -1 < self.currentPageIndex - 1 {
                self.mainView.tabView.scrollRectToVisible(self.tabsViewList[self.currentPageIndex - 1].frame, animated: false)
            }
            if let pos = self.products.enumerated().first(where: { (p) -> Bool in return p.element.code == self.groups[currentIndex].id }){
                var k = pos.offset + 2
                if pos.offset == 0 { k = 0 }
                self.mainView.tableView.scrollToRow(at: IndexPath(row: k < self.products.count ? k : pos.offset, section: 0), at: .top, animated: true)
            }else if self.currentPageIndex == 0 && self.products.count > 0 {
                 self.mainView.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            self.tabIndicator.frame = tabIndicatorFrame
            self.tabIndicator.layoutIfNeeded()
        })
    }
    
    //MARK: - свайпы туда-сюда
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (products[indexPath.row].id == "-1") { return nil }
        let favAction = self.contextualToggleFavAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [favAction])
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (products[indexPath.row].id == "-1") { return nil }
        let addToCartAction = self.contextualToggleAddToCartAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [addToCartAction])
        return swipeConfig
    }
    
    func reload()  {
        _ = DataManager.shared.getProducts().observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { p in
                self.products = p
                 UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.mainView.tableView.reloadData() })
            })
    }
    
    func contextualToggleFavAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "")
        { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            completionHandler(true)
            var p = self.products[indexPath.row]
            if DataManager.shared.isFavourite(product: self.products[indexPath.row]){
                _ = DataManager.shared.removeFavourite(product: p)
                    .concatMap({ (a) -> Observable<Int> in return Observable.timer(0.5, scheduler: ThreadUtil.shared.backScheduler) })
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe{
                        if let e = self.products.enumerated().first(where: { $0.element.id == p.id && $0.element.parentGroup == "0000" }){
                            self.products.remove(at: e.offset)
                            if !self.products.contains(where: {$0.parentGroup == "0000"}){ self.products.remove(at: 0) }
                            UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.mainView.tableView.reloadData() })
                        }
                    }
            }else{
                _ = DataManager.shared.addFavourite(product: p)
                    .concatMap({ (a) -> Observable<Int> in return Observable.timer(0.5, scheduler: ThreadUtil.shared.backScheduler) })
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe{
                        if self.products[0].name != "Избранное" { self.products.insert(Products(id: "-1", name: "Избранное", code: "0000"), at: 0) }
                        p.parentGroup = "0000"
                        self.products.insert(p, at: 1)
                        UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.mainView.tableView.reloadData() })
                    }
            }
        }
        if DataManager.shared.isFavourite(product: products[indexPath.row]) {
            action.image  = #imageLiteral(resourceName: "icon_star")
            action.backgroundColor = DEFAULT_COLOR_YELLOW
        }else{
            action.backgroundColor = DEFAULT_COLOR_YELLOW
            action.image  = #imageLiteral(resourceName: "icon_star_bordered")
        }
        return action
    }

    func contextualToggleAddToCartAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            completionHandler(true)
            let prod = self.products[indexPath.row]
            _ = DataManager.shared.getModificators(product: prod)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { m in
                    if m.count == 0{
                        UIView.transition(with: self.view, duration: 0.0, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
                            _ = DataManager.shared.addOrderAndCheck(product: prod)
                                .observeOn(ThreadUtil.shared.mainScheduler)
                                .subscribe(onNext: { res in
                                    if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                                })
                        })
                    }else{
                        var mes: [String] = []
                        var voids: [(()->Void)] = []
                        m.forEach { (p) in
                            var name = p.name
                            if name.first == "-" { name.remove(at: name.startIndex) }
                            mes.append(name + " + " + p.price.cleanValue + " " + RUBLE_LETTER)
                            voids.append({
                                _ = DataManager.shared.addOrderAndCheck(product: prod, modificator: p.id)
                                    .observeOn(ThreadUtil.shared.mainScheduler)
                                    .subscribe(onNext: { res in
                                        if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                                        else{ AlertUtil.alert({}, mes: "\(prod.name) (\(name)) добавлен в корзину", title: "Ура!") }
                                    })
                            })
                        }
                        AlertUtil.myAlert("Выберите модификатор для " + prod.name, messages: mes, voids: voids)
                    }
                })
        }
        action.image  = #imageLiteral(resourceName: "icon_basket")
        return action
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        guard let p = (previewingContext.sourceView as! ProductTableViewCell).product else { return }
        if (p.id == "-1") { return  }
        if (previewingContext.sourceView.isKind(of: ProductTableViewCell.self)){
            navigationController?.tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(
                ProductsController(products: products.filter{$0.id != "-1" && $0.parentGroup != "0000" }, product: p), animated: true)
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let p = (previewingContext.sourceView as! ProductTableViewCell).product else { return nil }
        if (p.id == "-1") { return nil }
        if (previewingContext.sourceView.isKind(of: ProductTableViewCell.self))
        {
            let pc = ProductViewController()
            pc.product = p
            return pc
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (products[indexPath.row].id == "-1") { return }
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(
            ProductsController(products: products.filter{ $0.id != "-1" && $0.parentGroup != "0000" }, product: products[indexPath.row]), animated: true)
    }
}


