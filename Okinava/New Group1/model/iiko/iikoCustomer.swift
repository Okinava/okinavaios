//
//  Customer.swift
//  Okinava
//
//  Created by _ on 27.04.2018.
//  Copyright © 2018 _. All rights reserved.
//


import Foundation
import Gloss

//MARK: - IikoCustomer
public class iikoCustomer: Glossy {
    var id: String = DataManager.shared.customerHelper.id
    var name: String = "Нет имени"
    var phone: String = ""
    var birthday: String?
    var shouldReceivePromoActionsInfo: Bool = true
    var sex: Int = 0
    var isBlocked: Bool = false
    var walletBalances: [iikoWalletBalance] = []
    var categories: [iikoCategory] = []
    var cards: [iikoCards] = []
    
    init(phone: String, name: String) {
        self.phone = phone
        self.name = name
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let name : String = "name" <~~ json {  self.name = name }
        if let phone : String = "phone" <~~ json {  self.phone = phone }
        if let birthday : String = "birthday" <~~ json {  self.birthday = birthday }
        if let sex : Int = "sex" <~~ json {  self.sex = sex }
        if let isBlocked : Bool = "birthday" <~~ json {  self.isBlocked = isBlocked }
        if let walletBalances : [iikoWalletBalance] = "walletBalances" <~~ json {  self.walletBalances = walletBalances }
        if let categories : [iikoCategory] = "categories" <~~ json {  self.categories = categories }
        if let cards : [iikoCards] = "cards" <~~ json {  self.cards = cards }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "phone" ~~> phone,
            "birthday" ~~> birthday,
            "shouldReceivePromoActionsInfo" ~~> shouldReceivePromoActionsInfo,
            "sex" ~~> sex,
            ])
    }
}

//MARK: - iikoWalletBalance
class iikoWalletBalance: Glossy {
    var balance: Double = 0
    var wallet: iikoWallet?
  
    //MARK: Decodable
    public required init?(json: JSON){
        if let balance : Double = "balance" <~~ json {  self.balance = balance }
        if let wallet : iikoWallet = "wallet" <~~ json {  self.wallet = wallet }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "balance" ~~> balance,
            "wallet" ~~> wallet,
            ])
    }
}

//MARK: - iikoWallet
class iikoWallet: Glossy {
    var id: String = ""
    var name: String = ""
    var programType: String = ""
    var type: String = ""
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let name : String = "name" <~~ json {  self.name = name }
        if let programType : String = "programType" <~~ json {  self.programType = programType }
        if let type : String = "type" <~~ json {  self.type = type }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "programType" ~~> programType,
            "type" ~~> type,
            ])
    }
}

//MARK: - iikoCategory
class iikoCategory: Glossy {
    var id: String = ""
    var isActive: Bool = false
    var isDefaultForNewGuests: Bool = true
    var name: String = ""
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let isActive : Bool = "isActive" <~~ json {  self.isActive = isActive }
        if let isDefaultForNewGuests : Bool = "isDefaultForNewGuests" <~~ json {  self.isDefaultForNewGuests = isDefaultForNewGuests }
        if let name : String = "name" <~~ json {  self.name = name }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "isActive" ~~> isActive,
            "isDefaultForNewGuests" ~~> isDefaultForNewGuests,
            "name" ~~> name,
            ])
    }
}

//MARK: - iikoCards
class iikoCards: Glossy {
    var id: String = ""
    var Track: String = ""
    var Number: String = "нет"
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let Track : String = "Track" <~~ json {  self.Track = Track }
        if let Number : String = "Number" <~~ json {  self.Number = Number }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "Track" ~~> Track,
            "Number" ~~> Number,
            ])
    }
}


extension Glossy{
    func toData() -> Data?{
        return try? JSONSerialization.data(withJSONObject: self.toJSON() ?? [:], options: [])
    }
}
