//
//  File.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import SnapKit

class BaseView: UIView{
    lazy var contentView: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(contentView)
        setContent()
        updateConstraints()
    }
    
    func setContent(){}
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        contentView.snp.remakeConstraints{make in
            make.top.left.right.bottom.equalTo(0)
            make.width.equalTo(UIScreen.main.bounds.width)
        }
    }
}



