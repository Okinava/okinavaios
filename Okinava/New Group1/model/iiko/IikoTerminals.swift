//
//  IikoTerminals.swift
//  Okinava
//
//  Created by Timerlan on 06.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import Gloss

//MARK: - IikoTerminals
public struct IikoTerminals: Glossy {
    
    public var deliveryTerminals : [IikoDeliveryTerminal]!
    
    //MARK: Decodable
    public init?(json: JSON){
        deliveryTerminals = "deliveryTerminals" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "deliveryTerminals" ~~> deliveryTerminals,
            ])
    }
    
}

//MARK: - DeliveryTerminal
public struct IikoDeliveryTerminal: Glossy {
    
    public var address : String!
    public var deliveryRestaurantName : String!
    public var deliveryTerminalId : String!
    public var externalRevision : Int!
    public var name : String!
    public var organizationId : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        address = "address" <~~ json
        deliveryRestaurantName = "deliveryRestaurantName" <~~ json
        deliveryTerminalId = "deliveryTerminalId" <~~ json
        externalRevision = "externalRevision" <~~ json
        name = "name" <~~ json
        organizationId = "organizationId" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "address" ~~> address,
            "deliveryRestaurantName" ~~> deliveryRestaurantName,
            "deliveryTerminalId" ~~> deliveryTerminalId,
            "externalRevision" ~~> externalRevision,
            "name" ~~> name,
            "organizationId" ~~> organizationId,
            ])
    }
    
}

