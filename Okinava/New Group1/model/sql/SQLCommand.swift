//
//  SQLCommand.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite

protocol SQLCommand {
    func insert(_ db: Connection, table: Table)
    func update(_ db: Connection, table: Table)
    func delete(_ db: Connection, table: Table)
    func isWasInDb(_ db: Connection, table: Table) -> Bool
    static func fromRow<T:SQLCommand>(_ row: Row) -> T
    static func createTable(_ db: Connection) -> Table
    func tableName() -> String
    static func tableName() -> String
}
extension Connection {
    public var userVersion: Int64 {
        get { return try! scalar("PRAGMA user_version") as! Int64 }
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}

