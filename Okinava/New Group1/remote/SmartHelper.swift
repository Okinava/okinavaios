//
//  SmartHelper.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import RxSwift
import SwiftHTTP
import RxCocoa

class SmartHelper{
    private let url = "http://smart-resto.ru"
    
    private var promos: [smartPromo]?
    private func getPromo() -> Observable<[smartPromo]> {
        return Observable<[smartPromo]>.create{ obs -> Disposable in
            if let data = UserDefaults.standard.data(forKey: "smartPromo"),
                let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]]{
                var result: [smartPromo] = []
                jsons.forEach{ result.append(smartPromo(json: $0)!) }
                obs.onNext(result)
            }
            HTTP.GET("\(self.url)/okinava_actions.php?token=98HQq2no", parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer(), completionHandler: { response in
                if let jsons = try? JSONSerialization.jsonObject(with: response.data) as! [[String : Any]]{
                    UserDefaults.standard.setValue(response.data, forKey: "smartPromo")
                    var result: [smartPromo] = []
                    jsons.forEach{ result.append(smartPromo(json: $0)!) }
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }else if let data = UserDefaults.standard.data(forKey: "smartPromo"),
                    let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]]{
                    var result: [smartPromo] = []
                    jsons.forEach{ result.append(smartPromo(json: $0)!) }
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить акции."))
            })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getTime() -> Observable<Int64> {
        return Observable<Int64>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/unixtime.php"){ response in
                if let time = Int64(response.text ?? "") {
                    obs.onNext(time)
                }else{
                    obs.onNext(Int64(Date().timeIntervalSince1970 * 1000 * 10))
                }
                 obs.onCompleted()
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getSales() -> Observable<Bool> {
        return Observable<Bool>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/okinava_mp/json.php?type=actionStatus"){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any], let data = json["data"] as? Bool{
                    obs.onNext(data)
                }else{
                    obs.onNext(false)
                }
                obs.onCompleted()
            }
            return Disposables.create{}
            }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getSalesNomen() -> Observable<[Products]> {
        return Observable<[Products]>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/okinava_mp/json.php?type=getActionGoods"){ response in
                var result: [Products] = []
                if let jsons = try? JSONSerialization.jsonObject(with: response.data) as! [[String : Any]] {
                    UserDefaults.standard.setValue(response.data, forKey: "ProductsSales")
                    jsons.forEach{ (o) in result.append(DBMapper.cast(product: IikoProduct(json: o)!)) }
                }else if let data = UserDefaults.standard.data(forKey: "ProductsSales"),
                    let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]]{
                    jsons.forEach{ (o) in result.append(DBMapper.cast(product: IikoProduct(json: o)!)) }
                }
                obs.onNext(result); obs.onCompleted()
            }
            return Disposables.create{}
            }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    
    func getPromos() -> Observable<[smartPromo]> {
        if promos != nil { return Observable.just(promos!) }
        return getPromo()
            .do(onNext: { (o) in self.promos = o })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func postFCMTokenToSR(customer c: CustomerHelper, token: String) {
        let phone =  c.phone.replacingOccurrences(of: "+", with: "")
        let postData = [
            "name" : c.name,
            "phone": phone,
            "udid": token,
            "date": c.date
        ]
        HTTP.POST("\(self.url)/okinava_mp/script.php?key=tujrt8kgdgdgdgndyr8k&os=ios", parameters: postData, headers: ["Content-Type":"application/x-www-form-urlencoded"])
    }

    func add(order: IikoOrders) -> Observable<Bool> {
        let version = "" + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String)
        return Observable<Bool>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "\(self.url)/okinava_mp/orders.php?key=v33GeIu&type=app&os=ios&build=\(version)")!
            request.httpMethod = "POST"
            let json = order.toJSON()!
            do { request.httpBody = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(false); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(false); obs.onCompleted(); return}
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        print("timrlm"); print(json)
                        obs.onNext(true); obs.onCompleted()
                    }else{
                        obs.onNext(false); obs.onCompleted()
                    }
                } catch let error {
                    print("timrlm " + error.localizedDescription)
                    obs.onNext(false); obs.onCompleted()
                }
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    

    func getPushs(phone: String) -> Observable<smartPushs> {
        return Observable<smartPushs>.create{ obs -> Disposable in
            if let data = UserDefaults.standard.data(forKey: "smartPushs\(phone)"),
                let json = try? JSONSerialization.jsonObject(with: data) as! [String : Any], let result = smartPushs(json: json){
                obs.onNext(result)
            }
            HTTP.GET("\(self.url)/okinava_mp/json.php?type=getPushHistory&phone=\(phone)"){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any],
                    let result = smartPushs(json: json){
                    UserDefaults.standard.setValue(response.data, forKey: "smartPushs\(phone)")
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }else if let data = UserDefaults.standard.data(forKey: "smartPushs\(phone)"),
                    let json = try? JSONSerialization.jsonObject(with: data) as! [String : Any], let result = smartPushs(json: json){
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить список ресторанов."))
            }
            return Disposables.create{}
        }
    }

    
    private func getRestoran() -> Observable<[smartRestorans]> {
        return Observable<[smartRestorans]>.create{ obs -> Disposable in
            if let data = UserDefaults.standard.data(forKey: "smartRestorans"),
                let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]]{
                var result: [smartRestorans] = []
                jsons.forEach{ result.append(smartRestorans(json: $0)!) }
                obs.onNext(result)
            }
            HTTP.GET("\(self.url)/okinava_address.php?token=m00KijeE2"){ response in
                if let jsons = try? JSONSerialization.jsonObject(with: response.data) as! [[String : Any]]{
                    UserDefaults.standard.setValue(response.data, forKey: "smartRestorans")
                    var result: [smartRestorans] = []
                    jsons.forEach{ result.append(smartRestorans(json: $0)!) }
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }else if let data = UserDefaults.standard.data(forKey: "smartRestorans"),
                    let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]]{
                    var result: [smartRestorans] = []
                    jsons.forEach{ result.append(smartRestorans(json: $0)!) }
                    obs.onNext(result)
                    obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить список ресторанов."))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getRestorans() -> Observable<[smartRestorans]> {
        return getRestoran().subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    public static var orderNumber: Int64?
    func sberPay(amount: Int64, orderNumber: Int64) -> Observable<String> {
        return Observable<String>.create{ obs -> Disposable in
            SmartHelper.orderNumber = orderNumber
            HTTP.GET("https://securepayments.sberbank.ru/payment/rest/register.do?userName=okinavakzn-api&password=Smart-resto2018*&returnUrl=https://smart-resto-success.ru&failUrl=https://smart-resto-error.ru&amount=\(amount)&orderNumber=\(orderNumber)"){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any],
                    let resp = sberResponse(json: json), let url = resp.formUrl {
                    obs.onNext(url)
                    obs.onCompleted()
                }
                obs.onError(IikoError("К сожалению в данный момент невозможно совершить оплату картой онлайн. Пожалуйста выберите другой тип оплаты"))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func applePay(token: String, orderNumber: Int64) -> Observable<String> {
        return Observable<String>.create{ obs -> Disposable in
            SmartHelper.orderNumber = orderNumber
            let session = URLSession.shared
            var request = URLRequest(urlString: "https://securepayments.sberbank.ru/payment/applepay/payment.do")!
            request.httpMethod = "POST"
            let json : [String:Any] = [
                "merchant" : "okinavakzn",
                "orderNumber" : orderNumber,
                "paymentToken" : token
            ]
            do { request.httpBody = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onError(IikoError("")); obs.onCompleted(); return }
                guard let data = data else { obs.onError(IikoError("")); obs.onCompleted(); return }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any], let success = json["success"] as? Int, success == 1 {
                        obs.onNext("true"); obs.onCompleted(); return
                    }
                } catch { }
                obs.onError(IikoError("")); obs.onCompleted(); return
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func version() -> Observable<Bool> {
        return Observable<Bool>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "http://smart-resto.ru/okinava_mp/json.php?type=getVersion&os=ios")!
            request.httpMethod = "GET"
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(true); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(true); obs.onCompleted(); return }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let version1 = json["data"] as? String,
                        let v = Bundle.main.infoDictionary?["CFBundleShortVersionString"],
                        let version2 = v as? String {
                        var v1 = 0, v2 = 0;
                        version1.split{ $0 == "."}.map(String.init).forEach({ (i) in if let o = Int(i) { v1 = v1 * 1000 + o } })
                        version2.split{ $0 == "."}.map(String.init).forEach({ (i) in if let o = Int(i) { v2 = v2 * 1000 + o }  })
                        obs.onNext(v1 <= v2); obs.onCompleted(); return
                    }
                } catch { }
                obs.onNext(true); obs.onCompleted(); return
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    

}
