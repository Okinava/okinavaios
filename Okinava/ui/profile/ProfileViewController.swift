//
//  ProfileViewController.swift
//  Okinava
//
//  Created by _ on 01.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class ProfileViewController: UIViewController {
    private lazy var profileTableView: UITableView = {
        let view = UITableView()
        view.register(HistoryOrderInfoTableViewCell.self, forCellReuseIdentifier: "historyOrderInfoCell")
        view.dataSource = self
        view.delegate = self
        view.addSubview(refreshControl)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.separatorStyle = .none
        view.tableHeaderView = self.headerProfileView
        return view
    }()
    private lazy var labelForStatusLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_GRAY
        view.text = "Статус"
        return view
    }()
    private lazy var labelForBalanceLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_GRAY
        view.text = "Бонусы"
        return view
    }()
    private lazy var headerProfileView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 128))
        view.addSubview(profileView)
        return view
    }()
    private lazy var profileView: UIView = {
        let view = UIView()
        view.layer.shadowRadius = 4
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowOpacity = 0.1
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addSubview(profileImageView)
        view.addSubview(profileNameLabel)
        view.addSubview(labelForStatusLabel)
        view.addSubview(profileStatusLabel)
        view.addSubview(labelForBalanceLabel)
        view.addSubview(profileBalanceLabel)
        view.addSubview(profileMoreButton)
        view.addSubview(profileEditButton)
        view.addSubview(dividerBeforeCustomerInfo)
        view.addSubview(customerInfoView)
        view.addSubview(dividerBeforeAddresses)
        view.addSubview(addressesInfoView)
        return view
    }()
    private lazy var profileImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "no_avatar")
        view.layer.cornerRadius = 4
        return view
    }()
    private lazy var profileNameLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var profileBonusImage = UIImageView(image: #imageLiteral(resourceName: "ic_card"))
    private lazy var profileBonusLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var profileStatusLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var profileBalanceLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_RED
        return view

    }()
    private lazy var profileMoreButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "profile_more"), for: .normal)
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.addTarget(self, action: #selector(profileMoreInfoTapped), for: .touchUpInside)
        return view
    }()
    private lazy var profileEditButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "icon_edit_profile"), for: .normal)
        view.alpha = 0.5
        view.contentMode = .scaleAspectFit
        view.addTarget(self, action: #selector(profileEditButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var dividerBeforeCustomerInfo = UIView()
    private lazy var dividerBeforeAddresses = UIView()
    private lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.leftButton = OK_ChatButton()
        view.navigationTitle = "Профиль"
        view.rightButton = OK_BasketButton()
        view.prepareView()
        return view
    }()
    private lazy var refreshControl = UIRefreshControl()
    private lazy var customerInfoView: UIView = {
        let view = UIView()
        view.addSubview(imageViewForPhone)
        view.addSubview(profileBonusImage)
        view.addSubview(imageViewForSex)
        view.addSubview(imageViewForBDay)
        view.addSubview(customerPhoneLabel)
        view.addSubview(profileBonusLabel)
        view.addSubview(customerSexLabel)
        view.addSubview(customerBDayLabel)
        return view
    }()
    private lazy var labelForAddresses: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = "Адреса"
        return view
    }()
    private lazy var addressesInfoView: UIView = {
        let view = UIView()
        view.addSubview(labelForAddresses)
        view.addSubview(profileAddressStackView)
        return view
    }()
    private lazy var customerPhoneLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var customerSexLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var customerBDayLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var imageViewForPhone = UIImageView(image: #imageLiteral(resourceName: "icon_phone"))
    private lazy var imageViewForSex =  UIImageView(image: #imageLiteral(resourceName: "icon_user"))
    private lazy var imageViewForBDay  = UIImageView(image: #imageLiteral(resourceName: "ic_date"))
    private lazy var profileAddressStackView = UIStackView()
    private lazy var imagePicker = UIImagePickerController()
    
    
    var profileMoreInfoOppened = false
    let sectionHeaderTitleArray = ["Мои заказы"]
//    private var historyOrderInfos : [DeliveryHistoryOrderInfo] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Профиль"
        view.backgroundColor = .white
        if (DataManager.shared.customerHelper.phone.count < 9){
            navigationController?.isNavigationBarHidden = false
            prepareGuestView()
        } else {
            self.navigationController?.isNavigationBarHidden = true
            self.prepareProfileView()
            self.prepareData()
            self.prepareHistory();
        }
        NotificationCenter.default.addObserver(forName: NotificationUtil.ADDRESS, object: nil, queue: OperationQueue.main){ notification in self.updateAddress() }
        NotificationCenter.default.addObserver(forName: NotificationUtil.CUSTOMER, object: nil, queue: OperationQueue.main){ notification in
            self.prepareData(); self.prepareHistory()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
        if (DataManager.shared.customerHelper.phone.count < 9){
            navigationController?.isNavigationBarHidden = false
            prepareGuestView()
        } else {
            self.navigationController?.isNavigationBarHidden = true
            self.prepareProfileView()
            self.prepareData()
            self.prepareHistory();
        }
    }
    
    private func prepareGuestView(){
        let sushiImageView = { () -> UIImageView in
            let imageView = UIImageView()
            imageView.image = #imageLiteral(resourceName: "mobile-phone")
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            return imageView
        }()
        let descriptionLabel = { () -> UILabel in
            let label = UILabel()
            label.text = "Чтобы получить бонусы за заказ, ввести промокод и увидеть историю ваших заказов, укажите номер телефона."
            label.textAlignment = .center
            label.textColor = DEFAULT_COLOR_DARK_GRAY
            label.font = FONT_ROBOTO_LIGHT?.withSize(14)
            label.numberOfLines = 4
            return label
        }()
        
        let toAuthScreenButton = { ()-> ZFRippleButton in
            let button = ZFRippleButton()
            button.setTitle( "Указать телефон", for: .normal)
            button.titleLabel?.textColor = .white
            button.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
            button.backgroundColor = DEFAULT_COLOR_RED
            button.tintColor = DEFAULT_COLOR_RED
            button.layer.cornerRadius = 4
            button.addTarget(self, action: #selector(authButtonTapped), for: .touchUpInside)
            return button
        }()
        let parentView = UIView()
        view.addSubview(parentView)
        parentView.backgroundColor = DEFAULT_BACKGROUND
        parentView.addSubview(sushiImageView)
        parentView.addSubview(descriptionLabel)
        parentView.addSubview(toAuthScreenButton)
        parentView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.9)
            make.centerX.centerY.equalToSuperview()
        }
        sushiImageView.snp.makeConstraints { (make) in
            make.centerX.top.equalToSuperview()
            make.height.width.equalTo(64)
        }
        descriptionLabel.snp.makeConstraints{(make) in
            make.width.centerX.equalToSuperview()
            make.top.equalTo(sushiImageView.snp.bottom).offset(25)
        }
        toAuthScreenButton.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(25)
            make.width.centerX.bottom.equalToSuperview()
            make.height.equalTo(43)
        }
    }
    @objc func authButtonTapped(){
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(AuthNumberViewController(need: false), animated: true)
    }
    
    private func prepareProfileView(){
        profileAddressStackView.axis = .vertical
        dividerBeforeCustomerInfo.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        dividerBeforeAddresses.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        addressesInfoView.isHidden = true
        customerInfoView.isHidden = true
        dividerBeforeAddresses.isHidden = true
        dividerBeforeCustomerInfo.isHidden = true
        imageViewForPhone.contentMode = .scaleAspectFit
        imageViewForSex.contentMode = .scaleAspectFit
        imageViewForBDay.contentMode = .scaleAspectFit
        profileBonusImage.contentMode = .scaleAspectFit
      
        
        view.addSubview(self.topNavigationView)
        view.addSubview(self.profileTableView)
        view.backgroundColor = .white
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        self.profileTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(16)
            make.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        self.profileMoreButton.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        self.profileImageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(72)
            make.left.top.equalToSuperview().offset(15)
            
        }
        self.profileNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.profileImageView.snp.right).offset(15)
            make.top.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-16)
            
        }
        labelForStatusLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.profileNameLabel.snp.left)
            make.width.equalTo(45)
            make.top.equalTo(self.profileNameLabel.snp.bottom).offset(5)
        }
        labelForBalanceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.profileNameLabel.snp.left)
            make.width.equalTo(45)
            make.top.equalTo(labelForStatusLabel.snp.bottom).offset(5)
        }
        
        self.profileStatusLabel.snp.makeConstraints { (make) in
            make.left.equalTo(labelForStatusLabel.snp.right).offset(5)
            make.top.equalTo(labelForStatusLabel.snp.top)
            
        }
        self.profileBalanceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(labelForBalanceLabel.snp.right).offset(5)
            make.top.equalTo(labelForBalanceLabel.snp.top)
            
        }
        self.profileEditButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.width.height.equalTo(20)
            
        }
        dividerBeforeCustomerInfo.snp.makeConstraints { (make) in
            make.height.equalTo(1   )
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
        }
        self.customerInfoView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalTo(dividerBeforeCustomerInfo.snp.bottom).offset(15)
        }
        imageViewForPhone.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(16)
        }
        self.profileBonusImage.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.top.equalTo(imageViewForPhone.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            
        }
        imageViewForSex.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(profileBonusImage.snp.bottom).offset(16)
        }
        imageViewForBDay.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(imageViewForSex.snp.bottom).offset(16)
            make.bottom.equalToSuperview().offset(-15)
        }
        self.customerPhoneLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageViewForPhone.snp.right).offset(10)
            make.centerY.equalTo(imageViewForPhone)
            make.right.equalToSuperview().offset(-20)
        }
        self.profileBonusLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.profileBonusImage.snp.right).offset(10)
            make.centerY.equalTo(self.profileBonusImage)
            make.right.equalToSuperview().offset(-20)
        }
        self.customerSexLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageViewForSex.snp.right).offset(10)
            make.centerY.equalTo(imageViewForSex)
            make.right.equalToSuperview().offset(-20)
        }
        self.customerBDayLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageViewForBDay.snp.right).offset(10)
            make.centerY.equalTo(imageViewForBDay)
            make.right.equalToSuperview().offset(-20)
        }
        dividerBeforeAddresses.snp.makeConstraints { (make) in
            make.top.equalTo(self.customerInfoView.snp.bottom).offset(15)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(1)
        }
        profileView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalToSuperview().offset(-16)
        }
        addressesInfoView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalTo(dividerBeforeAddresses.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
        }
        labelForAddresses.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview()
        }
        profileAddressStackView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalTo(labelForAddresses.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    @objc func refreshData(){
        _ = DataManager.shared.getCustomer(phone: DataManager.shared.customerHelper.phone)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { cus in
                DataManager.shared.customerHelper.setDefault(phone: DataManager.shared.customerHelper.phone)
                DataManager.shared.customerHelper.setAll(customer: cus)
                DataManager.shared.setFCM()
            }, onError: { t in
                self.refreshControl.endRefreshing()
                if t.localizedDescription == "Не удалось получить ответ"{
                    //AlertUtil.alert({}, mes: "Не удалось обновить пользователя.")
                    return
                }
                DataManager.shared.customerHelper.setDefault(phone: DataManager.shared.customerHelper.phone)
                DataManager.shared.setFCM()
            })
    }
    
    private var historyOrderInfos: [IikoDeliveryHistory] = []
    private func prepareData(){
        profileBalanceLabel.text = String(Int(DataManager.shared.customerHelper.bonus))
        profileStatusLabel.text = DataManager.shared.customerHelper.status
        profileNameLabel.text = DataManager.shared.customerHelper.name
        profileBonusLabel.text = DataManager.shared.customerHelper.card
        customerPhoneLabel.text = DataManager.shared.customerHelper.phone
        customerBDayLabel.text = DataManager.shared.customerHelper.date
        switch DataManager.shared.customerHelper.sex {
        case 1:  customerSexLabel.text  = "Мужской"; break
        case 2:  customerSexLabel.text  = "Женский"; break
        default: customerSexLabel.text  = "пол не указан"
        }
        updateAddress()
    }
    
    private func prepareHistory(){
        refreshControl.beginRefreshing()
        _ = DataManager.shared.getHistory()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { os in
                self.refreshControl.endRefreshing()
                self.historyOrderInfos = os
                self.profileTableView.reloadData()
            }, onError: { t in
                if (DataManager.shared.customerHelper.phone.count < 9){ return }
                self.refreshControl.endRefreshing()
                //AlertUtil.alert({}, mes: "Не удалось обновить историю заказов")
            })
    }
    
    private func updateAddress(){
        _ = DataManager.shared.getAddress()
        .observeOn(ThreadUtil.shared.mainScheduler)
        .subscribe(onNext: { addreses in
            self.profileAddressStackView.arrangedSubviews.forEach{ u in
                self.profileAddressStackView.removeArrangedSubview(u)
                u.isHidden = true
            }
            addreses.forEach{ a in
                let addressView = OK_ContactView(image: #imageLiteral(resourceName: "icon_home"), title: "\(a.street) \(a.home)")
                addressView.isSelecting(visible: false)
                self.profileAddressStackView.addArrangedSubview(addressView)
                addressView.snp.makeConstraints { (make) in make.height.equalTo(46) }
                addressView.tag = Int(a.id)
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.editAddressTapped))
                addressView.addGestureRecognizer(gesture)
            }
            let addAddressView = OK_ContactView(image: #imageLiteral(resourceName: "icon_plus_red"), title: "Добавить адрес")
            addAddressView.isSelecting(visible: false)
            let addButton = ZFRippleButton()
            addAddressView.addSubview(addButton)
            self.profileAddressStackView.addArrangedSubview(addAddressView)
            addAddressView.snp.makeConstraints { (make) in make.height.equalTo(46) }
            addButton.snp.makeConstraints { (make) in make.edges.equalToSuperview() }
            addButton.addTarget(self, action: #selector(self.addAddressButtonTapped), for: .touchUpInside)
            _ = Observable<Int>.timer(0.2, scheduler: ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { t in
                    self.profileMoreInfoOppened = !self.profileMoreInfoOppened
                    self.profileMoreInfoTapped()
                })
        })
    }
    @objc func addAddressButtonTapped(){
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(AddAddressViewController(), animated: true)
    }
    @objc func editAddressTapped(_ sender: UITapGestureRecognizer){
        _ = DataManager.shared.getAddress()
        .observeOn(ThreadUtil.shared.mainScheduler)
        .subscribe(onNext: { addreses in
            let addressVC = AddAddressViewController()
            addressVC.address = addreses.first(where: {$0.id == Int64(sender.view?.tag ?? 0)})
            self.navigationController?.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(addressVC, animated: true)
        })
    }
    
    @objc func profileEditButtonTapped(){
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(EditProfileViewController(), animated: true)
    }
    
    @objc func profileMoreInfoTapped(){
        var newRect = self.profileTableView.tableHeaderView!.frame
        if self.profileMoreInfoOppened {
            newRect.size.height = 128
            self.profileMoreButton.setImage(#imageLiteral(resourceName: "profile_more"), for: .normal  )
        } else {
            newRect.size.height = 360 + self.profileAddressStackView.frame.height
            self.profileMoreButton.setImage(#imageLiteral(resourceName: "profile_less"), for: .normal  )
        }
        self.profileMoreInfoOppened = !self.profileMoreInfoOppened
        self.addressesInfoView.isHidden = !self.profileMoreInfoOppened
        self.customerInfoView.isHidden = !self.profileMoreInfoOppened
        self.dividerBeforeCustomerInfo.isHidden = !self.profileMoreInfoOppened
        self.dividerBeforeAddresses.isHidden = !self.profileMoreInfoOppened
        let header = self.profileTableView.tableHeaderView!
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            header.frame = newRect
            self.profileTableView.tableHeaderView = header
        })
    }
}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource, UIViewControllerPreviewingDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return historyOrderInfos.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Мои заказы"
        default:
            return ""
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView()
        returnedView.backgroundColor = DEFAULT_BACKGROUND
        
        let label = UILabel()
        returnedView.addSubview(label)
        label.font = FONT_ROBOTO_BOLD?.withSize(18)
        label.textColor = DEFAULT_COLOR_BLACK
        
        label.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-5)
        }
        label.text = self.sectionHeaderTitleArray[section]
        
        
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyOrderInfoCell", for: indexPath as IndexPath) as! HistoryOrderInfoTableViewCell
        cell.setData(orderInfo: historyOrderInfos[indexPath.row])
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: cell)
        }
        return cell
    }
    
    //MARK: - свайпы туда-сюда
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let addToCartAction = self.contextualToggleRepeat(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [addToCartAction])
        return swipeConfig
    }
    func contextualToggleRepeat(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            completionHandler(true)
            let orderInfo = self.historyOrderInfos[indexPath.row]
            _ = DataManager.shared.clearOrder().subscribe(onNext: { b in
                AlertUtil.repeatOrder({
                    _ = DataManager.shared.addOrder(history: orderInfo)
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .subscribe(onNext: { b in
                            _ = DataManager.shared.getOrder()
                                .observeOn(ThreadUtil.shared.mainScheduler)
                                .subscribe(onNext: { r in
                                    if r.count == 0 {
                                        AlertUtil.alert({}, mes: "Невозможно повторить заказ, в данном заказе нет активных товаров")
                                        return
                                    }
                                    if !b {
                                        AlertUtil.alert({
                                            NavigationHelper.shared.showNext(CartViewController())
                                        }, mes: "В данном заказе не все позиции товаров активны для продажи")
                                    }else{
                                        NavigationHelper.shared.showNext(CartViewController())
                                    }
                                })
                        })
                })
            })
        }
        action.image  = #imageLiteral(resourceName: "icon_basket")
        return action
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let orderInfo = (previewingContext.sourceView as! HistoryOrderInfoTableViewCell).orderInfo else { return nil }
//        navigationController?.tabBarController?.tabBar.isHidden = true
        return HistoryDetailController(orderInfo: orderInfo)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        guard let orderInfo = (previewingContext.sourceView as! HistoryOrderInfoTableViewCell).orderInfo else { return }
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(HistoryDetailController(orderInfo: orderInfo), animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(HistoryDetailController(orderInfo: historyOrderInfos[indexPath.row]), animated: true)
    }
}
