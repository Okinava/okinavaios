//
//  ThankYouViewController.swift
//  Okinava
//
//  Created by _ on 02.05.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit

class ThankYouViewController: UIViewController {
    public var goMainButton = ZFRippleButton()
    public var centerButton = ZFRippleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        DataManager.shared.customerHelper.promo = "";
        _ = DataManager.shared.clearOrder().subscribe()
    }
    
    private func prepareView()
    {
        self.view.backgroundColor = UIColor().hexStringToUIColor(0xfff9f8);
        let happyImage = { () -> UIImageView in
            let iv = UIImageView()
            iv.image = #imageLiteral(resourceName: "littlesushis")
            iv.contentMode = .scaleAspectFit
            return iv
        }()
        let thankYouLabel = { () -> UILabel in
            let l = UILabel()
            l.numberOfLines = 0
            l.text = "Спасибо за заказ :)"
            l.textAlignment = .center
            l.font = UIFont.systemFont(ofSize: 14, weight: .light)
            return l
        }()
        goMainButton = { ()-> ZFRippleButton in
            let b = ZFRippleButton()
            b.setTitle("Хорошо!", for: .normal)
            b.layer.cornerRadius = 7
            b.backgroundColor = DEFAULT_COLOR_RED
            b.titleLabel?.textColor = .white
            return b
        }()
        self.view.addSubview(happyImage)
        self.view.addSubview(thankYouLabel)
        self.view.addSubview(goMainButton)
        
        self.view.addSubview(self.centerButton)
        happyImage.clipsToBounds = true
       
        
        goMainButton.addTarget(self, action: #selector(goToMain(_:)), for: .touchDown)
        happyImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(200)
        }
        thankYouLabel.snp.makeConstraints { (make) in
            make.top.equalTo(happyImage.snp.bottom).offset(20)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
       goMainButton.snp.makeConstraints { (make) in
            make.top.equalTo(thankYouLabel.snp.bottom).offset(20)
            make.width.equalToSuperview().multipliedBy(0.6)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
        }
    }
    @objc func goToMain(_ sender: UIButton){
        if DataManager.shared.customerHelper.mark {
            NavigationHelper.shared.showMain()
            return;
        }
        let alert = UIAlertController.init(title: "Отзыв", message: "Помогите нам стать лучше. Оставить отзыв? ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { a in
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1186347586"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                DataManager.shared.customerHelper.mark = true
            }
            NavigationHelper.shared.showMain()
        }))
        alert.addAction(UIAlertAction(title: "Нет", style: .default, handler: { a in
            NavigationHelper.shared.showMain()
        }))
        alert.addAction(UIAlertAction(title: "Больше не спрашивать", style: .destructive, handler: { a in
            DataManager.shared.customerHelper.mark = true
            NavigationHelper.shared.showMain()
        }))
        NavigationHelper.shared.present(alert, animated: true, completion: nil)
    }
}
