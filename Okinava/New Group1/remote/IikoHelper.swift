//
//  IikoHelper.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import RxSwift
import SwiftHTTP
import RxCocoa

class IikoHelper{
    public static let StatusBlacklisted = "5527ee9e-31ba-47f5-9909-0773580d8765"
    private let url = "https://iiko.biz:9900/api/0"
    private let orgId = "50322c02-141c-11e5-80d2-d8d38565926f";
    static let orgId = "50322c02-141c-11e5-80d2-d8d38565926f";
    private let login = "SRmobile";
    private let pass = "PytEdmefkiwoyf9";
    
    private func getAccessToken() -> Observable<String> {
        return Observable<String>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/auth/access_token?user_id=\(self.login)&user_secret=\(self.pass)",parameters: nil,headers: nil,requestSerializer: JSONParameterSerializer(), completionHandler: { response in
                if let text = response.text, text.count > 10 {
                    obs.onNext(String(text.dropLast().dropFirst())); obs.onCompleted();
                    return
                }
                obs.onError(IikoError("Не удалось получить ответ")); obs.onCompleted()
            })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func getNomenclature(token: String) -> Observable<IikoNomenclature> {
        return Observable<IikoNomenclature>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/nomenclature/\(self.orgId)?access_token=\(token)",parameters: nil,headers: nil,requestSerializer: JSONParameterSerializer(), completionHandler: { response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any],
                    let nomen = IikoNomenclature(json: json) {
                    obs.onNext(nomen); obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить ответ"))
            })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getNomenclature() -> Observable<IikoNomenclature> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<IikoNomenclature> in return self.getNomenclature(token: token) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func getCustomer(token: String, phone: String) -> Observable<iikoCustomer>{
        return Observable<iikoCustomer>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/customers/get_customer_by_phone?organization=\(self.orgId)&access_token=\(token)&phone=\(phone)"){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any],
                    let cus = iikoCustomer(json: json) {
                    obs.onNext(cus); obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить пользователя"))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getCustomer(phone: String) -> Observable<iikoCustomer> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<iikoCustomer> in return self.getCustomer(token: token, phone: phone) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func setCustomer(token: String, json cc: [String: Any?]) -> Observable<String>{
        return Observable<String>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "\(self.url)/customers/create_or_update?organization=\(self.orgId)&access_token=\(token)")!
            request.httpMethod = "POST" //set http method as POST
            do { request.httpBody = try JSONSerialization.data(withJSONObject: cc, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(""); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(""); obs.onCompleted(); return}
                do { if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] { print(json) }
                } catch let error {  print(error.localizedDescription) }
                obs.onNext(""); obs.onCompleted()
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func setCustomer(json cc: [String:Any?]) -> Observable<String> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<String> in return self.setCustomer(token: token, json: cc) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func getStreets(token: String) -> Observable<iikoStreets>{
        return Observable<iikoStreets>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/cities/cities?organization=\(self.orgId)&access_token=\(token)"){ response in
                if let jsons = try? JSONSerialization.jsonObject(with: response.data) as! [[String : Any]], jsons.count > 0 , let cus = iikoStreets(json: jsons[0]){
                    UserDefaults.standard.setValue(response.data, forKey: "iikoStreets")
                    obs.onNext(cus); obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Не удалось получить список улиц"))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getStreets() -> Observable<iikoStreets> {
        return Observable<iikoStreets>.create{ obs -> Disposable in
            if let data = UserDefaults.standard.data(forKey: "iikoStreets"),
                let jsons = try? JSONSerialization.jsonObject(with: data) as! [[String : Any]], jsons.count > 0 , let cus = iikoStreets(json: jsons[0]){
                obs.onNext(cus); obs.onCompleted()
            }
            _ = self.getAccessToken()
                .concatMap({ (token) -> Observable<iikoStreets> in return self.getStreets(token: token) })
                .subscribe(onNext: { o in obs.onNext(o); obs.onCompleted() }, onError: { t in obs.onError(t); obs.onCompleted() })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func checkAdress(address: iikoAddress, token: String) -> Observable<Bool>{
        return Observable<Bool>.create{ obs -> Disposable in
            HTTP.POST("\(self.url)/orders/checkAddress?organizationId=\(self.orgId)&access_token=\(token)", parameters: address.toJSON()){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any], let result = json["addressInZone"] as? Bool{
                    obs.onNext(result); obs.onCompleted()
                    return
                }
                obs.onError(IikoError("Невозможно совержить доставку на данный адрес. Введите другой адрес"))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func checkAdress(address: iikoAddress) -> Observable<Bool> {
        return getAccessToken()
            .catchError({ (e) -> Observable<String> in return Observable.just("qqqq") })
            .concatMap({ (token) -> Observable<Bool> in
                if token == "qqqq" { return Observable.just(true) }
                else { return self.checkAdress(address: address, token: token) }
            })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func getHistory(phone: String, token: String) -> Observable<[IikoDeliveryHistory]>{
        return Observable<[IikoDeliveryHistory]>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/orders/deliveryHistoryByPhone?organization=\(self.orgId)&access_token=\(token)&phone=\(phone)",parameters: nil,headers: nil,requestSerializer: JSONParameterSerializer(), completionHandler: { response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any], let result = IikoHistory(json: json), let arrayT = result.customersDeliveryHistory{
                    var array: [IikoDeliveryHistory] = []
                    arrayT.forEach({ (i) in array.append(contentsOf: i.deliveryHistory) })
                    UserDefaults.standard.setValue(response.data, forKey: "IikoHistory" + phone)
                    array = array.sorted(by: { (o1, o2) -> Bool in
                        let d = DateFormatter(); d.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        return d.date(from: o1.date)! > d.date(from: o2.date)!
                    })
                    obs.onNext(array)
                    obs.onCompleted()
                }else{
                    obs.onError(IikoError("Не удалось получить историю заказов"))
                    obs.onCompleted()
                }
            })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getHistory(phone: String) -> Observable<[IikoDeliveryHistory]> {
        return Observable<[IikoDeliveryHistory]>.create{ obs in
            if let data = UserDefaults.standard.data(forKey: "IikoHistory" + phone),
                let json = try? JSONSerialization.jsonObject(with: data) as! [String : Any],
                let result = IikoHistory(json: json), let arrayT = result.customersDeliveryHistory{
                var array: [IikoDeliveryHistory] = []
                arrayT.forEach({ (i) in array.append(contentsOf: i.deliveryHistory) })
                array = array.sorted(by: { (o1, o2) -> Bool in
                    let d = DateFormatter(); d.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    return d.date(from: o1.date)! > d.date(from: o2.date)!
                })
                obs.onNext(array)
            }
            _ = self.getAccessToken()
                .concatMap({ (token) -> Observable<[IikoDeliveryHistory]> in return self.getHistory(phone: phone, token: token) })
                .subscribe(onNext: { o in obs.onNext(o); obs.onCompleted() }, onError: { t in
                    obs.onError(IikoError("Не удалось обновить историю заказов"))
                    obs.onCompleted()
                })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func getTerminals(token: String) -> Observable<IikoTerminals>{
        return Observable<IikoTerminals>.create{ obs -> Disposable in
            HTTP.GET("\(self.url)/deliverySettings/getDeliveryTerminals?organization=\(self.orgId)&access_token=\(token)"){ response in
                if let json = try? JSONSerialization.jsonObject(with: response.data) as! [String : Any], let result = IikoTerminals(json: json){
                    if result.deliveryTerminals == nil {
                        obs.onError(IikoError("Не удалось получить список терминалов. Проверьте интернет соединение"))
                    } else {
                        UserDefaults.standard.setValue(response.data, forKey: "IikoTerminals")
                        obs.onNext(result); obs.onCompleted()
                    }
                    return
                }
                obs.onError(IikoError("Не удалось получить список терминалов. Проверьте интернет соединение"))
            }
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func getTerminals() -> Observable<IikoTerminals> {
        return Observable<IikoTerminals>.create{ obs in
            if let data = UserDefaults.standard.data(forKey: "IikoTerminals"),
                let json = try? JSONSerialization.jsonObject(with: data) as! [String : Any], let result = IikoTerminals(json: json), result.deliveryTerminals != nil{
                obs.onNext(result); obs.onCompleted()
            }
            _ = self.getAccessToken()
                .concatMap({ (token) -> Observable<IikoTerminals> in return self.getTerminals(token: token) })
                .subscribe(onNext: { o in obs.onNext(o); obs.onCompleted() }, onError: { t in obs.onError(t); obs.onCompleted() })
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func add(order: IikoOrders, token: String) -> Observable<Bool>{
        return Observable<Bool>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "\(self.url)/orders/add?access_token=\(token)")!
            request.httpMethod = "POST"
            let json = order.toJSON()!
            do { request.httpBody = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(false); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(false); obs.onCompleted(); return}
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        print("timrlm"); print(json)
                        obs.onNext(true); obs.onCompleted()
                    }else{
                        obs.onNext(false); obs.onCompleted()
                    }
                } catch let error {
                    print("timrlm " + error.localizedDescription)
                    obs.onNext(false); obs.onCompleted()
                }
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func add(order: IikoOrders) -> Observable<Bool> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<Bool> in return self.add(order: order, token: token) })
            .catchError({ (e) -> Observable<Bool> in return Observable.just(false) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func star(comment: String, deliveryId: String, token: String, f: Int, s: Int, t: Int) -> Observable<Bool>{
        return Observable<Bool>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "\(self.url)/orders/sendDeliveryOpinion?access_token=\(token)")!
            request.httpMethod = "POST"
            let json: [String : Any] = [ "organization":self.orgId, "deliveryId":deliveryId, "comment":comment,
                        "marks": [
                            ["surveyItemId": "63768149-1859-9605-014d-f98e67010002", "mark": f],
                            ["surveyItemId": "63768149-1859-9605-014d-f98e67010003", "mark": s],
                            ["surveyItemId": "63768149-1859-9605-014d-f98e67010004", "mark": t]
                        ]
                ]
            do { request.httpBody = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(false); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(false); obs.onCompleted(); return}
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                        print("timrlm"); print(json)
                        obs.onNext(true); obs.onCompleted()
                    }else{
                        obs.onNext(false); obs.onCompleted()
                    }
                } catch let error {
                    print("timrlm " + error.localizedDescription)
                    obs.onNext(false); obs.onCompleted()
                }
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func star(comment: String, deliveryId: String, f: Int, s: Int, t: Int) -> Observable<Bool> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<Bool> in return self.star(comment: comment, deliveryId: deliveryId, token: token, f: f, s: s, t: t) })
            .catchError({ (e) -> Observable<Bool> in return Observable.just(false) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    private func checkSale(order: IikoOrders, token: String) -> Observable<String?>{
        return Observable<String?>.create{ obs -> Disposable in
            let session = URLSession.shared
            var request = URLRequest(urlString: "\(self.url)/orders/calculate_checkin_result?access_token=\(token)")!
            request.httpMethod = "POST"
            let json = order.toJSON()!
            do { request.httpBody = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            } catch let error { print(error.localizedDescription) }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { obs.onNext(nil); obs.onCompleted(); return }
                guard let data = data else { obs.onNext(nil); obs.onCompleted(); return}
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let result = smartCheckSales(json: json),
                        let loyatyResult = result.loyatyResult,
                        let array = loyatyResult.programResults{
                        var text = ""
                        array.forEach({ (o) in
                            if let name = o["name"]{ text += "\(name); "  }
                        })
                        print("timrlm"); print(json)
                        obs.onNext(text.count == 0 ? nil : text); obs.onCompleted()
                    }else{
                        obs.onNext(nil); obs.onCompleted()
                    }
                } catch let error {
                    print("timrlm " + error.localizedDescription)
                    obs.onNext(nil); obs.onCompleted()
                }
            })
            task.resume()
            return Disposables.create{}
        }.subscribeOn(ThreadUtil.shared.backScheduler)
    }
    
    func checkSale(order: IikoOrders) -> Observable<String?> {
        return getAccessToken()
            .concatMap({ (token) -> Observable<String?> in return self.checkSale(order: order, token: token) })
            .catchError({ (e) -> Observable<String?> in return Observable.just(nil) })
            .subscribeOn(ThreadUtil.shared.backScheduler)
    }
}


extension LocalizedError where Self: CustomStringConvertible { var errorDescription: String? { return description } }
class IikoError: LocalizedError, CustomStringConvertible {
    let desc: String
    init(_ str: String) { desc = str }
    var description: String {
        let format = NSLocalizedString("%@", comment: "Error description")
        return String.localizedStringWithFormat(format, desc)
    }
    var e: Error{ get{ return self as Error }}
}


