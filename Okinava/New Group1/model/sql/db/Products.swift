//
//  Products.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite

struct Products: SQLCommand {
    var id: String
    var name: String
    var code: String
    var product_description: String
    var order: Int64
    var parentGroup: String
    var groupId: String
    var productCategoryId: String
    var price: Double
    var carbohydrateAmount: Double
    var energyAmount: Double
    var fatAmount: Double
    var fiberAmount: Double
    var carbohydrateFullAmount: Double
    var energyFullAmount: Double
    var fatFullAmount: Double
    var fiberFullAmount: Double
    var weight: Double
    var type: String
    var isIncludedInMenu: Bool
    var measureUnit : String
    var doNotPrintInCheque: Bool
    var useBalanceForSell: Bool
    var image: String
    
    init(id: String = "", name: String = "", code: String = "", product_description: String = "", order: Int64 = 0, parentGroup: String = "", groupId: String = "", productCategoryId: String = "", price: Double = 0, carbohydrateAmount: Double = 0, energyAmount: Double = 0, fatAmount: Double = 0, fiberAmount: Double = 0, carbohydrateFullAmount: Double = 0, energyFullAmount: Double = 0, fatFullAmount: Double = 0, fiberFullAmount: Double = 0, weight: Double = 0, type: String = "", isIncludedInMenu: Bool = false, measureUnit : String = "", doNotPrintInCheque: Bool = false, useBalanceForSell: Bool = false, image: String = "") {
        self.id = id
        self.name = name
        self.code = code
        self.product_description = product_description
        self.order = order
        self.parentGroup = parentGroup
        self.groupId = groupId
        self.productCategoryId = productCategoryId
        self.price = price
        self.carbohydrateAmount = carbohydrateAmount
        self.energyAmount = energyAmount
        self.fatAmount = fatAmount
        self.fiberAmount = fiberAmount
        self.carbohydrateFullAmount = carbohydrateFullAmount
        self.energyFullAmount = energyFullAmount
        self.fatFullAmount = fatFullAmount
        self.fiberFullAmount = fiberFullAmount
        self.weight = weight
        self.type = type
        self.isIncludedInMenu = isIncludedInMenu
        self.measureUnit = measureUnit
        self.doNotPrintInCheque = doNotPrintInCheque
        self.useBalanceForSell = useBalanceForSell
        self.image = image
    }
    
    static let id = Expression<String>("id")
    static let name = Expression<String>("name")
    static let code = Expression<String>("code")
    static let product_description = Expression<String>("product_description")
    static let order = Expression<Int64>("order")
    static let parentGroup = Expression<String>("parentGroup")
    static let groupId = Expression<String>("groupId")
    static let productCategoryId = Expression<String>("productCategoryId")
    static let price = Expression<Double>("price")
    static let carbohydrateAmount = Expression<Double>("carbohydrateAmount")
    static let energyAmount = Expression<Double>("energyAmount")
    static let fatAmount = Expression<Double>("fatAmount")
    static let fiberAmount = Expression<Double>("fiberAmount")
    static let carbohydrateFullAmount = Expression<Double>("carbohydrateFullAmount")
    static let energyFullAmount = Expression<Double>("energyFullAmount")
    static let fatFullAmount = Expression<Double>("fatFullAmount")
    static let fiberFullAmount = Expression<Double>("fiberFullAmount")
    static let weight = Expression<Double>("weight")
    static let type = Expression<String>("type")
    static let isIncludedInMenu = Expression<Bool>("isIncludedInMenu")
    static let measureUnit = Expression<String>("measureUnit")
    static let doNotPrintInCheque = Expression<Bool>("doNotPrintInCheque")
    static let useBalanceForSell = Expression<Bool>("useBalanceForSell")
    static let image = Expression<String>("image")

    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Products.id <- id,
            Products.name <- name,
            Products.code <- code,
            Products.product_description <- product_description,
            Products.order <- order,
            Products.parentGroup <- parentGroup,
            Products.groupId <- groupId,
            Products.productCategoryId <- productCategoryId,
            Products.price <- price,
            Products.carbohydrateAmount <- carbohydrateAmount,
            Products.energyAmount <- energyAmount,
            Products.fatAmount <- fatAmount,
            Products.fiberAmount <- fiberAmount,
            Products.carbohydrateFullAmount <- carbohydrateFullAmount,
            Products.energyFullAmount <- energyFullAmount,
            Products.fatFullAmount <- fatFullAmount,
            Products.fiberFullAmount <- fiberFullAmount,
            Products.weight <- weight,
            Products.type <- type,
            Products.isIncludedInMenu <- isIncludedInMenu,
            Products.measureUnit <- measureUnit,
            Products.doNotPrintInCheque <- doNotPrintInCheque,
            Products.useBalanceForSell <- useBalanceForSell,
            Products.image <- image
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Products.id == id).update(
            Products.name <- name,
            Products.code <- code,
            Products.product_description <- product_description,
            Products.order <- order,
            Products.parentGroup <- parentGroup,
            Products.groupId <- groupId,
            Products.productCategoryId <- productCategoryId,
            Products.price <- price,
            Products.carbohydrateAmount <- carbohydrateAmount,
            Products.energyAmount <- energyAmount,
            Products.fatAmount <- fatAmount,
            Products.fiberAmount <- fiberAmount,
            Products.carbohydrateFullAmount <- carbohydrateFullAmount,
            Products.energyFullAmount <- energyFullAmount,
            Products.fatFullAmount <- fatFullAmount,
            Products.fiberFullAmount <- fiberFullAmount,
            Products.weight <- weight,
            Products.type <- type,
            Products.isIncludedInMenu <- isIncludedInMenu,
            Products.measureUnit <- measureUnit,
            Products.doNotPrintInCheque <- doNotPrintInCheque,
            Products.useBalanceForSell <- useBalanceForSell,
            Products.image <- image
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Products.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Products.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Products( id: row[id], name:  row[name], code:  row[code], product_description:  row[product_description], order:  row[order], parentGroup:  row[parentGroup], groupId:  row[groupId], productCategoryId:  row[productCategoryId], price:  row[price], carbohydrateAmount:  row[carbohydrateAmount], energyAmount:  row[energyAmount], fatAmount:  row[fatAmount], fiberAmount:  row[fiberAmount], carbohydrateFullAmount:  row[carbohydrateFullAmount], energyFullAmount:  row[energyFullAmount], fatFullAmount:  row[fatFullAmount], fiberFullAmount:  row[fiberFullAmount], weight:  row[weight], type:  row[type], isIncludedInMenu:  row[isIncludedInMenu], measureUnit:  row[measureUnit], doNotPrintInCheque:  row[doNotPrintInCheque], useBalanceForSell:  row[useBalanceForSell], image:  row[image]) as! T
    }
    
    func tableName() -> String{ return "Products"}
    static func tableName() -> String{ return "Products"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Products().tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(name)
            t.column(code)
            t.column(product_description)
            t.column(order)
            t.column(parentGroup)
            t.column(groupId)
            t.column(productCategoryId)
            t.column(price)
            t.column(carbohydrateAmount)
            t.column(energyAmount)
            t.column(fatAmount)
            t.column(fiberAmount)
            t.column(carbohydrateFullAmount)
            t.column(energyFullAmount)
            t.column(fatFullAmount)
            t.column(fiberFullAmount)
            t.column(weight)
            t.column(type)
            t.column(isIncludedInMenu)
            t.column(measureUnit)
            t.column(doNotPrintInCheque)
            t.column(useBalanceForSell)
            t.column(image)
        })
        return table
    }
}
