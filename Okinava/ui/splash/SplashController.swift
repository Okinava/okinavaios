//
//  SplashController.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import RxSwift
import Crashlytics

class SplashController: BaseController<SplashView> {
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        version()
    }
    
    private func version(){
        _ = DataManager.shared.version()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { b in
                if (!b){
                    self.version_er()
                }else{
                    self.verison_OK()
                }
            })
    }
    
    private func verison_OK(){
        _ = DataManager.shared.getSalesNomen()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ps in
                DBHelper.sales = ps
                self.start()
            })
    }
    
    private func version_er(){
        AlertUtil.alert({
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1186347586"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                self.version()
            }else{
                self.verison_OK()
            }
        }, mes: "К сожалению ваша версия приложения устарела, необходимо обновить приложение", title: "Ой!")
    }
    
    private func start(){
        _ = DataManager.shared.isHaveNomenclature()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { b in if b {
                NavigationHelper.shared.showMain()
                _ = DataManager.shared.updateNomenclature().subscribe()
                self.updateCustomer()
            } else { self.checkNomenclature() } })
    }
    
    private func updateCustomer(){
        let phone = DataManager.shared.customerHelper.phone
        if phone.count < 3 { return }
        _ = DataManager.shared.getCustomer(phone: phone)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { cus in
                if cus.categories.contains(where: {$0.id == IikoHelper.StatusBlacklisted}){
                    DataManager.shared.customerHelper.setDefault(phone: "")
                    return
                }
                Crashlytics.sharedInstance().setUserName(phone)
                Crashlytics.sharedInstance().setUserIdentifier(phone)
                DataManager.shared.customerHelper.setDefault(phone: phone)
                DataManager.shared.customerHelper.setAll(customer: cus)
                DataManager.shared.setFCM()
            },onError: { t in })
    }
    
    private func checkNomenclature(){
        _ = DataManager.shared.updateNomenclature()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onError: { (e) in
                AlertUtil.connectionAlert({ self.checkNomenclature() })
            }, onCompleted: {
                NavigationHelper.shared.showMain()
            }).subscribe()
    }
}
