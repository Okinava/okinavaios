//
//  smartPushs.swift
//  Okinava
//
//  Created by Timerlan on 05.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import Gloss

//MARK: - smartPushs
public struct smartPushs: Glossy {
    
    public var data : [smartPush]!
    public var status : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        data = "data" <~~ json
        status = "status" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "data" ~~> data,
            "status" ~~> status,
            ])
    }
    
}

//MARK: - smartPush
public struct smartPush: Glossy {
    
    public var datetime : String!
    public var id : String!
    public var orders : String!
    public var phone : String!
    public var type : String!
    
    public init(){}
    
    //MARK: Decodable
    public init?(json: JSON){
        datetime = "datetime" <~~ json
        id = "id" <~~ json
        orders = "orders" <~~ json
        phone = "phone" <~~ json
        type = "type" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "datetime" ~~> datetime,
            "id" ~~> id,
            "orders" ~~> orders,
            "phone" ~~> phone,
            "type" ~~> type,
            ])
    }
    
    static func getFrom(text: String) -> smartTextPush?{
        if let jsonData = try? JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? JSON {
            return smartTextPush(json: jsonData!)
        }
        return nil
    }
}


//MARK: - smartTextPush
public struct smartTextPush: Glossy {
    public var comment : String!
    public var deliveryStatus : String!
    public var orderId : String!
    public var orderType : String!
    public var receiver : String!
    public var sum : Int!
    public var text : String!
    
    public init(){
        
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        comment = "comment" <~~ json
        deliveryStatus = "deliveryStatus" <~~ json
        orderId = "orderId" <~~ json
        orderType = "orderType" <~~ json
        receiver = "receiver" <~~ json
        sum = "sum" <~~ json
        text = "text" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "comment" ~~> comment,
            "deliveryStatus" ~~> deliveryStatus,
            "orderId" ~~> orderId,
            "orderType" ~~> orderType,
            "receiver" ~~> receiver,
            "sum" ~~> sum,
            "text" ~~> text,
            ])
    }
    
}
