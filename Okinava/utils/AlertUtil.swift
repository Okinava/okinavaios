//
//  AlertUtil.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit

class AlertUtil {
    static func connectionAlert(_ completion: @escaping (()->Void), mes: String = "Отсутствует интернет соединение, проверьте подключение. Для первого запуска необходим интернет." ) {
        let alert = UIAlertController.init(title: "Ой!", message: mes, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: { a in completion() }))
        NavigationHelper.shared.present(alert, animated: true, completion: nil)
    }
    
    static func alert(_ completion: @escaping (()->Void), mes: String, title: String = "Ой!" ) {
        let alert = UIAlertController.init(title: title, message: mes, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: { a in completion() }))
        NavigationHelper.shared.present(alert, animated: true, completion: nil)
    }
    
    static func repeatOrder(_ completion: @escaping (()->Void) )  {
        let alert = UIAlertController.init(title: nil, message: "Вы хотите повторить этот заказ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { a in completion() }))
        alert.addAction(UIAlertAction(title: "Нет", style: .destructive, handler: nil ))
        NavigationHelper.shared.present(alert, animated: true, completion: nil)
    }
    
    static func star(star: Int, completion: @escaping (()->Void) )  {
        let alert = UIAlertController.init(title: nil, message: "Вы хотите оценить сервис на \(star)?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { a in completion() }))
        alert.addAction(UIAlertAction(title: "Нет", style: .destructive, handler: nil ))
        NavigationHelper.shared.present(alert, animated: true, completion: nil)
    }
    
    static func myAlert(_ title: String? = nil, messages: [String], voids: [(()->Void)?], complited: (()->Void)? = nil){
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for mes in messages.enumerated() {
            alert.addAction(UIAlertAction.init(title: mes.element, style: .default, handler: { (action) in voids[mes.offset]?() }))
        }
        alert.addAction(UIAlertAction.init(title: "Отмена", style: .cancel, handler: nil))
        NavigationHelper.shared.present(alert)
    }
}

