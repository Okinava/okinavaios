//
//  Nomenclature.swift
//  Okinava
//
//  Created by _ on 03.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Gloss

//MARK: - IikoNomenclature
class IikoNomenclature:  Glossy  {
    public var groups: [IikoGroup] = []
    public var products: [IikoProduct] = []
    public var revision: Int64 = 0
    public var uploadDate: String = ""

    //MARK: Decodable
    public required init?(json: JSON){
        if let groups : [IikoGroup] = "groups" <~~ json {  self.groups = groups }
        if let products : [IikoProduct] = "products" <~~ json {  self.products = products }
        if let revision : Int64 = "revision" <~~ json {  self.revision = revision }
        if let uploadDate : String = "uploadDate" <~~ json {  self.uploadDate = uploadDate }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "groups" ~~> groups,
            "products" ~~> products,
            "revision" ~~> revision,
            "uploadDate" ~~> uploadDate,
            ])
    }
}

//MARK: - IikoProduct
class IikoProduct: Glossy {
    public var id: String = ""
    public var name: String = ""
    public var code: String = ""
    public var product_description: String = ""
    public var order: Int64 = 0
    public var parentGroup: String = ""
    public var groupId: String = ""
    public var productCategoryId: String = ""
    public var price: Double = 0
    public var carbohydrateAmount: Double = 0
    public var energyAmount: Double = 0
    public var fatAmount: Double = 0
    public var fiberAmount: Double = 0
    public var carbohydrateFullAmount: Double = 0
    public var energyFullAmount: Double = 0
    public var fatFullAmount: Double = 0
    public var fiberFullAmount: Double = 0
    public var weight: Double = 0
    public var type: String = ""
    public var isIncludedInMenu: Bool = false
    public var measureUnit : String = ""
    public var doNotPrintInCheque: Bool = false
    public var useBalanceForSell: Bool = false
    public var images: [IikoImage] = []
    public var groupModifiers: [IikoModifier] = []
 
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let name : String = "name" <~~ json {  self.name = name }
        if let code : String = "code" <~~ json {  self.code = code }
        if let product_description : String = "description" <~~ json {  self.product_description = product_description }
        if let order : Int64 = "order" <~~ json {  self.order = order }
        if let parentGroup : String = "parentGroup" <~~ json {  self.parentGroup = parentGroup }
        if let groupId : String = "groupId" <~~ json {  self.groupId = groupId }
        if let productCategoryId : String = "productCategoryId" <~~ json {  self.productCategoryId = productCategoryId }
        if let price : Double = "price" <~~ json {  self.price = price }
        if let carbohydrateAmount : Double = "carbohydrateAmount" <~~ json {  self.carbohydrateAmount = carbohydrateAmount }
        if let energyAmount : Double = "energyAmount" <~~ json {  self.energyAmount = energyAmount }
        if let fatAmount : Double = "fatAmount" <~~ json {  self.fatAmount = fatAmount }
        if let fiberAmount : Double = "fiberAmount" <~~ json {  self.fiberAmount = fiberAmount }
        if let carbohydrateFullAmount : Double = "carbohydrateFullAmount" <~~ json {  self.carbohydrateFullAmount = carbohydrateFullAmount }
        if let energyFullAmount : Double = "energyFullAmount" <~~ json {  self.energyFullAmount = energyFullAmount }
        if let fatFullAmount : Double = "fatFullAmount" <~~ json {  self.fatFullAmount = fatFullAmount }
        if let fiberFullAmount : Double = "fiberFullAmount" <~~ json {  self.fiberFullAmount = fiberFullAmount }
        if let weight : Double = "weight" <~~ json {  self.weight = weight }
        if let isIncludedInMenu : Bool = "isIncludedInMenu" <~~ json {  self.isIncludedInMenu = isIncludedInMenu }
        if let measureUnit : String = "measureUnit" <~~ json {  self.measureUnit = measureUnit }
        if let doNotPrintInCheque : Bool = "doNotPrintInCheque" <~~ json {  self.doNotPrintInCheque = doNotPrintInCheque }
        if let useBalanceForSell : Bool = "useBalanceForSell" <~~ json {  self.useBalanceForSell = useBalanceForSell }
        if let images : [IikoImage] = "images" <~~ json {  self.images = images }
        if let groupModifiers : [IikoModifier] = "groupModifiers" <~~ json {  self.groupModifiers = groupModifiers }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "code" ~~> code,
            "order" ~~> order,
            "product_description" ~~> product_description,
            "parentGroup" ~~> parentGroup,
            "productCategoryId" ~~> productCategoryId,
            "price" ~~> price,
            "carbohydrateAmount" ~~> carbohydrateAmount,
            "energyAmount" ~~> energyAmount,
            "fatAmount" ~~> fatAmount,
            "fiberAmount" ~~> fiberAmount,
            "carbohydrateFullAmount" ~~> carbohydrateFullAmount,
            "energyFullAmount" ~~> energyFullAmount,
            "fatFullAmount" ~~> fatFullAmount,
            "fiberFullAmount" ~~> fiberFullAmount,
            "weight" ~~> weight,
            "isIncludedInMenu" ~~> isIncludedInMenu,
            "measureUnit" ~~> measureUnit,
            "doNotPrintInCheque" ~~> doNotPrintInCheque,
            "useBalanceForSell" ~~> useBalanceForSell,
            "images" ~~> images,
            "groupModifiers" ~~> groupModifiers,
            "groupId" ~~> groupId,
            ])
    }
}

//MARK: - IikoModifier
public class IikoModifier: Glossy {
    public var modifierId: String = ""
    public var maxAmount: Int64 = 0
    public var minAmount: Int64 = 0
    public var defaultAmount: Int64 = 0
    public var hideIfDefaultAmount: Bool = false
    public var required: Bool = false
    
    public var childModifiersHaveMinMaxRestrictions: Bool = false
    public var childModifiers: [IikoModifier] = []
    
    var id: String = ""
    var name: String = ""
    var amount: Int64 = 1
    var groupId: String = ""
    
    init(id: String, name: String, groupId: String) {
        self.id = id
        self.name = name
        self.groupId = groupId
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let modifierId : String = "modifierId" <~~ json {  self.modifierId = modifierId }
        if let maxAmount : Int64 = "maxAmount" <~~ json {  self.maxAmount = maxAmount }
        if let minAmount : Int64 = "minAmount" <~~ json {  self.minAmount = minAmount }
        if let defaultAmount : Int64 = "defaultAmount" <~~ json {  self.defaultAmount = defaultAmount }
        if let hideIfDefaultAmount : Bool = "hideIfDefaultAmount" <~~ json {  self.hideIfDefaultAmount = hideIfDefaultAmount }
        if let required : Bool = "required" <~~ json {  self.required = required }
        if let childModifiersHaveMinMaxRestrictions : Bool = "childModifiersHaveMinMaxRestrictions" <~~ json {  self.childModifiersHaveMinMaxRestrictions = childModifiersHaveMinMaxRestrictions }
        if let childModifiers : [IikoModifier] = "childModifiers" <~~ json {  self.childModifiers = childModifiers }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "modifierId" ~~> modifierId,
            "maxAmount" ~~> maxAmount,
            "minAmount" ~~> minAmount,
            "defaultAmount" ~~> defaultAmount,
            "hideIfDefaultAmount" ~~> hideIfDefaultAmount,
            "required" ~~> required,
            "childModifiersHaveMinMaxRestrictions" ~~> childModifiersHaveMinMaxRestrictions,
            "childModifiers" ~~> childModifiers,
            
            "id" ~~> id,
            "name" ~~> name,
            "amount" ~~> amount,
            "groupId" ~~> groupId,
            ])
    }
}


//MARK: - IikoGroup
class IikoGroup: Glossy {
    public var id: String = ""
    public var name: String = ""
    public var order: Int64 = 0
    public var parentGroup: String = ""
    public var images: [IikoImage] = []
    public var isIncludedInMenu: Bool = false
   
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : String = "id" <~~ json {  self.id = id }
        if let name : String = "name" <~~ json {  self.name = name }
        if let order : Int64 = "order" <~~ json {  self.order = order }
        if let parentGroup : String = "parentGroup" <~~ json {  self.parentGroup = parentGroup }
        if let images : [IikoImage] = "images" <~~ json {  self.images = images }
        if let isIncludedInMenu : Bool = "isIncludedInMenu" <~~ json {  self.isIncludedInMenu = isIncludedInMenu }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "order" ~~> order,
            "parentGroup" ~~> parentGroup,
            "images" ~~> images,
            "isIncludedInMenu" ~~> isIncludedInMenu,
            ])
    }
}

//MARK: - IikoGroupImage
class IikoImage: Glossy {
    public var imageId: String = ""
    public var imageUrl: String = ""
    public var uploadDate: String = ""
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let imageId : String = "imageId" <~~ json {  self.imageId = imageId }
        if let imageUrl : String = "imageUrl" <~~ json {  self.imageUrl = imageUrl }
        if let uploadDate : String = "uploadDate" <~~ json {  self.uploadDate = uploadDate }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "imageId" ~~> imageId,
            "imageUrl" ~~> imageUrl,
            "uploadDate" ~~> uploadDate,
            ])
    }
}



