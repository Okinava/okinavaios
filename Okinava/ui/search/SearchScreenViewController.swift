//
//  SearchScreenViewController.swift
//  Okinava
//
//  Created by _ on 30.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import RxSwift

class SearchScreenViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIViewControllerPreviewingDelegate  {
    let resultsTableView = UITableView()
    let searchView = UIView()
    let searchBackButton  = OK_BackButton()
    let searchTextField  = UITextField()
    var searchString = ""
    var products : [Products] = []
    var filProducts : [Products] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        self.loadData()
        self.resultsTableView.delegate = self
        resultsTableView.separatorStyle = .none
        self.resultsTableView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.resultsTableView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize.height) }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.resultsTableView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
        }
    }
    private func loadData(){
        _ = DataManager.shared.getProducts()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ p in self.products = p; self.filterProducts() })
    }

    private func prepareView(){
        self.view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        searchBackButton.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        resultsTableView.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        self.resultsTableView.register(ProductTableViewCell.self, forCellReuseIdentifier: "ProductTableViewCell")
        self.view.addSubview(self.searchView)
        self.searchView.addSubview(self.searchBackButton)
        self.searchView.addSubview(self.searchTextField)
        self.view.addSubview(self.resultsTableView)
        self.searchBackButton.isWithShadow = false
        self.searchView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().inset(16)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset( UIApplication.shared.statusBarFrame.height)
            make.height.equalTo(40)
        }
        self.searchBackButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.height.width.equalTo(32)
            make.centerY.equalToSuperview()
        }
        self.searchTextField.snp.makeConstraints { (make) in
            make.left.equalTo(self.searchBackButton.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.height.equalTo(36)
            make.right.equalToSuperview()
        }
        self.resultsTableView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.searchView.snp.bottom).offset(5)
            make.bottom.equalToSuperview()
        }
        self.searchBackButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        self.searchTextField.placeholder = "Поиск"
        self.searchTextField.delegate = self
        self.searchTextField.textColor = DEFAULT_COLOR_BLACK
        self.searchTextField.font = FONT_ROBOTO_MEDIUM?.withSize(20)
        self.resultsTableView.separatorStyle = .singleLine
        self.searchTextField.becomeFirstResponder()
    }
    
    private func filterProducts(){
        filProducts = products.filter({$0.parentGroup != "0000" && $0.code != "0000"}).filter({ $0.name.lowercased().contains(searchString.lowercased()) || searchString.count == 0})
         UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.resultsTableView.reloadData() })
    }

    @objc func backButtonTapped (){ self.navigationController?.popViewController(animated: true) }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath) as! ProductTableViewCell
        if( traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: cell)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as ProductTableViewCell = cell else { return }
        if (filProducts[indexPath.row].id == "-1") { cell.setData(name: filProducts[indexPath.row].name) }
        else{ cell.setData(product: filProducts[indexPath.row]) }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return filProducts[indexPath.row].id == "-1" ? 30 : 146
    }
    
    //MARK: - свайпы туда-сюда
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (filProducts[indexPath.row].id == "-1") { return nil }
        let favAction = self.contextualToggleFavAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [favAction])
        return swipeConfig
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (filProducts[indexPath.row].id == "-1") { return nil }
        let addToCartAction = contextualToggleAddToCartAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [addToCartAction])
        return swipeConfig
    }
    
    func contextualToggleFavAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "")
        { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            completionHandler(true)
            let p = self.filProducts[indexPath.row]
            if DataManager.shared.isFavourite(product: self.filProducts[indexPath.row]){
                _ = DataManager.shared.removeFavourite(product: p)
                    .concatMap({ (a) -> Observable<Int> in return Observable.timer(0.5, scheduler: ThreadUtil.shared.backScheduler) })
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe{
                         NotificationCenter.default.post(name: NotificationUtil.FAVOURITES_UPD, object: nil)
                    }
            }else{
                _ = DataManager.shared.addFavourite(product: p)
                    .concatMap({ (a) -> Observable<Int> in return Observable.timer(0.5, scheduler: ThreadUtil.shared.backScheduler) })
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe{
                        NotificationCenter.default.post(name: NotificationUtil.FAVOURITES_UPD, object: nil)
                    }
            }
        }
        if DataManager.shared.isFavourite(product: filProducts[indexPath.row]) {
            action.image  = #imageLiteral(resourceName: "icon_star")
            action.backgroundColor = DEFAULT_COLOR_YELLOW
        }else{
            action.backgroundColor = DEFAULT_COLOR_YELLOW
            action.image  = #imageLiteral(resourceName: "icon_star_bordered")
        }
        return action
    }
    
    func contextualToggleAddToCartAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "") { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            completionHandler(true)
            let prod = self.filProducts[indexPath.row]
            _ = DataManager.shared.getModificators(product: prod)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { m in
                    if m.count == 0{
                        UIView.transition(with: self.view, duration: 0.0, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
                            _ = DataManager.shared.addOrderAndCheck(product: prod)
                                .observeOn(ThreadUtil.shared.mainScheduler)
                                .subscribe(onNext: { res in
                                    if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                                    else{ self.filterProducts() }
                                })
                        })
                    }else{
                        var mes: [String] = []
                        var voids: [(()->Void)] = []
                        m.forEach { (p) in
                            var name = p.name
                            if name.first == "-" { name.remove(at: name.startIndex) }
                            mes.append(name + " + " + p.price.cleanValue + " " + RUBLE_LETTER)
                            voids.append({
                                _ = DataManager.shared.addOrderAndCheck(product: prod, modificator: p.id)
                                    .observeOn(ThreadUtil.shared.mainScheduler)
                                    .subscribe(onNext: { res in
                                        if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                                        else{
                                            AlertUtil.alert({}, mes: "\(prod.name) (\(name)) добавлен в корзину", title: "Ура!")
                                            self.filterProducts()
                                        }
                                    })
                            })
                        }
                        AlertUtil.myAlert("Выберите модификатор для " + prod.name, messages: mes, voids: voids)
                    }
                })
        }
        action.image  = #imageLiteral(resourceName: "icon_basket")
        return action
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        guard let p = (previewingContext.sourceView as! ProductTableViewCell).product else { return }
        if (p.id == "-1") { return  }
        if (previewingContext.sourceView.isKind(of: ProductTableViewCell.self)){
            navigationController?.pushViewController(
                ProductsController(products: filProducts.filter{$0.id != "-1"}, product: p), animated: true)
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let p = (previewingContext.sourceView as! ProductTableViewCell).product else { return nil }
        if (p.id == "-1") { return nil }
        if (previewingContext.sourceView.isKind(of: ProductTableViewCell.self))
        {
            let pc = ProductViewController()
            pc.product = p
            return pc
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (filProducts[indexPath.row].id == "-1") { return }
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(
            ProductsController(products: filProducts.filter{ $0.id != "-1"}, product: filProducts[indexPath.row]), animated: true)
    }
}
extension SearchScreenViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.searchTextField) {
            self.searchString = textField.text! + string
            self.filterProducts()
        }
        return true
    }
}

