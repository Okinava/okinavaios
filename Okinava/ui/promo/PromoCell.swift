//
//  PromoCell.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit


let PromoCellID = "ProductCell"
class PromoCell: UICollectionViewCell {
    private var promo: smartPromo!
    private var modiferProducts: [Products] = []
    private let max = 260 + UIApplication.shared.statusBarFrame.height
    private let min = 160 + UIApplication.shared.statusBarFrame.height
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        view.addSubview(headerImageView)
        return view
    }()
    lazy var headerImageView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "main_screen_background"))
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    private lazy var cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        view.addSubview(productTitleLabel)
        view.addSubview(productWeightLabel)
        return view
    }()
    lazy var productTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_BLACK
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.5
        return view
    }()
    private lazy var productWeightLabel: UILabel = {
        let view = UILabel()
        view.textColor = DEFAULT_COLOR_GRAY
        view.font = FONT_ROBOTO_REGULAR?.withSize(14)
        view.numberOfLines = 0
        return view
    }()
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.parallaxHeader.view = headerView
        view.parallaxHeader.height = max
        view.parallaxHeader.minimumHeight = min
        view.parallaxHeader.mode = .topFill
        view.alwaysBounceVertical = true
        view.addSubview(contentMainView)
        return view
    }()
    private lazy var contentMainView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(cardView)
        view.addSubview(moreInfoTitle)
        view.addSubview(howToGetTitle)
        view.addSubview(howToGetBackground)
        view.addSubview(moreInfoBackground)
        return view
    }()
    private lazy var howToGetBackground: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowOpacity =  0.08
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.layer.shadowRadius = 16
        view.layer.shadowColor = DEFAULT_COLOR_BLACK.cgColor
        view.addSubview(howToGetStackView)
        return view
    }()
    private lazy var moreInfoBackground: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowOpacity =  0.08
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.layer.shadowRadius = 16
        view.layer.shadowColor = DEFAULT_COLOR_BLACK.cgColor
        view.addSubview(moreInfoStackView)
        return view
    }()
    private lazy var moreInfoStackView: UIStackView = {
        let view = UIStackView()
        let moreInfoOne = OK_PromoMoreInfoElement(image: #imageLiteral(resourceName: "icon_phone"), title: "8 (843) 233-44-33", url: URL(string: "tel://+78432334433")!)
        let moreInfoTwo = OK_PromoMoreInfoElement(image: #imageLiteral(resourceName: "icon_web"), title: "www.okinavakazan.ru", url: URL(string: "http://okinavakazan.ru")!)
        moreInfoOne.backgroundColor = .white
        moreInfoTwo.backgroundColor = .white
        view.addArrangedSubview(moreInfoOne)
        view.addArrangedSubview(moreInfoTwo)
        moreInfoOne.snp.makeConstraints { (make) in make.height.equalTo(50) }
        moreInfoTwo.snp.makeConstraints { (make) in make.height.equalTo(50) }
        view.axis = .vertical
        return view
    }()
    private lazy var howToGetStackView = UIStackView()
    private lazy var howToGetTitle: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.textColor = DEFAULT_COLOR_BLACK
        view.text = "Как получить?"
        return view
    }()
    private lazy var moreInfoTitle: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.textColor = DEFAULT_COLOR_BLACK
        view.text = "Подробности"
        return view
    }()
    
    func setData(promo: smartPromo, index: Int) {
        addSubview(scrollView)
        backgroundColor = .white
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 600)
        update()
        self.promo = promo
        self.productTitleLabel.text = self.promo?.title
        self.productWeightLabel.text = self.promo?.descriptionField
        self.headerImageView.sd_setImage(with: URL(string: self.promo!.image), completed: nil)
        let howToOne = OK_HowToGetElement()
        howToOne.set(title: (self.promo?.howItMake[0].actionTitle)!)
        howToOne.set(description: (self.promo?.howItMake[0].actionDesc)!)
        howToOne.set(icon: #imageLiteral(resourceName: "ic_select"))
        let howToTwo = OK_HowToGetElement()
        howToTwo.set(title: (self.promo?.howItMake[1].actionTitle)!)
        howToTwo.set(description: (self.promo?.howItMake[1].actionDesc)!)
        howToTwo.set(icon: #imageLiteral(resourceName: "ic_order"))
        self.howToGetStackView.addArrangedSubview(howToOne)
        self.howToGetStackView.addArrangedSubview(howToTwo)
        howToOne.snp.makeConstraints { (make) in make.height.equalTo(55) }
        howToTwo.snp.makeConstraints { (make) in make.height.equalTo(55) }
        howToOne.backgroundColor = .white
        howToTwo.backgroundColor = .white
        if index > 2 { scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 10, height: 10), animated: false) }
    }
   
    private func update() {
        howToGetStackView.arrangedSubviews.forEach { (u) in
            howToGetStackView.removeArrangedSubview(u)
            u.isHidden = true
        }
        headerImageView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
        cardView.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(-60)
        }
        productTitleLabel.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.left.equalTo(16)
            make.height.equalTo(20)
            make.right.equalToSuperview().offset(-16)
        }
        productWeightLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(productTitleLabel.snp.bottom).offset(10)
            make.left.equalTo(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
        }
        scrollView.snp.remakeConstraints{ make in
            make.top.left.equalTo(0)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.bottom.equalTo(0)
        }
        contentMainView.snp.remakeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.bottom.equalTo(0).offset(-UIApplication.shared.statusBarFrame.height)
        }
        howToGetTitle.snp.makeConstraints { (make) in
            make.top.equalTo(cardView.snp.bottom).offset(16)
            make.left.equalToSuperview().inset(DEFAULT_OFFSET_SIZE)
        }
        howToGetBackground.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(DEFAULT_OFFSET_SIZE)
            make.top.equalTo(howToGetTitle.snp.bottom).offset(15)
        }
        howToGetStackView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview().inset(3)
        }
        moreInfoTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.howToGetStackView.snp.bottom).offset(30)
            make.left.right.equalToSuperview().inset(DEFAULT_OFFSET_SIZE)
        }
        moreInfoBackground.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(DEFAULT_OFFSET_SIZE)
            make.top.equalTo(moreInfoTitle.snp.bottom).offset(15)
            make.bottom.equalToSuperview().offset(-20)
        }
        moreInfoStackView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview().inset(3)
        }
        self.howToGetStackView.axis = .vertical
    }
}
