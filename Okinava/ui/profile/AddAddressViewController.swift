//
//  AddAddressViewController.swift
//  Okinava
//
//  Created by _ on 20.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift


class AddAddressViewController: SwipeRightToPopViewController, UITextFieldDelegate {
    private lazy var saveButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Сохранить", for: .normal)
        view.setTitleColor(.white, for: .normal  )
        view.backgroundColor = DEFAULT_COLOR_RED
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.layer.cornerRadius = 6
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addTarget(self, action: #selector(self.saveButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.prepareView()
        view.backgroundColor = .white
        view.navigationTitle = "Добавление адреса"
        view.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        view.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var scrollView = UIScrollView()
    private lazy var streetBlock = OK_EditAddressBlockView()
    private lazy var homeBlock = OK_EditAddressBlockView()
    private lazy var housingBlock = OK_EditAddressBlockView()
    private lazy var apartmentBlock = OK_EditAddressBlockView()
    private lazy var commentBlock = OK_EditAddressBlockView()
    private lazy var florBlock = OK_EditAddressBlockView()
    private lazy var dphoneBlock = OK_EditAddressBlockView()
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(streetBlock)
        view.addSubview(homeBlock)
        view.addSubview(housingBlock)
        view.addSubview(apartmentBlock)
        view.addSubview(florBlock)
        view.addSubview(dphoneBlock)
        view.addSubview(commentBlock)
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.08
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        return view
    }()
    var address: Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        self.setData()
        self.hideKeyboardWhenTappedAround()
    }
    
    private func prepareView(){
        view.backgroundColor = .white
        view.addSubview(topNavigationView)
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        scrollView.addSubview(saveButton)
        scrollView.alwaysBounceVertical = true
        homeBlock.backgroundColor = .white
        housingBlock.backgroundColor = .white
        apartmentBlock.backgroundColor = .white
        streetBlock.backgroundColor = .white
        streetBlock.titleLabel.text  = "Улица"
        streetBlock.valueTextField.delegate = self
        homeBlock.titleLabel.text  = "Дом"
        housingBlock.titleLabel.text  = "Подъезд (корпус)"
        apartmentBlock.titleLabel.text  = "Квартира"
        commentBlock.titleLabel.text = "Комментарий"
        commentBlock.backgroundColor = .white
        florBlock.titleLabel.text = "Этаж"
        florBlock.backgroundColor = .white
        dphoneBlock.titleLabel.text = "Домофон"
        dphoneBlock.backgroundColor = .white
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 600)
       
        if (address != nil){
            self.topNavigationView.rightButton.setImage(#imageLiteral(resourceName: "icon_trash"), for: .normal)
            self.topNavigationView.rightButton.addTarget(self, action: #selector(self.trashButtonTapped), for: .touchUpInside)
        } else {
            self.topNavigationView.rightButton.removeFromSuperview()
        }
        
        topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.bottom.equalToSuperview().offset(0)
            make.left.right.equalToSuperview()
        }
        contentView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-80)
            make.top.equalToSuperview().offset(10)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
        }
        streetBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            make.height.equalTo(80)
        }
        homeBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.streetBlock.snp.bottom)
            make.height.equalTo(80)
        }
        housingBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.homeBlock.snp.bottom)
            make.height.equalTo(80)
        }
        apartmentBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.housingBlock.snp.bottom)
            make.height.equalTo(80)
        }
        florBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.apartmentBlock.snp.bottom)
            make.height.equalTo(80)
        }
        dphoneBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.florBlock.snp.bottom)
            make.height.equalTo(80)
        }
        commentBlock.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.dphoneBlock.snp.bottom)
            make.height.equalTo(80)
            make.bottom.equalToSuperview().offset(-40)
        }
        saveButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.contentView.snp.bottom).offset(16)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
            make.height.equalTo(45)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        navigationController?.pushViewController(SearchStreetController(void: { text in
            self.streetBlock.valueTextField.text = text
        }), animated: true)
        return true
    }
    @objc private func trashButtonTapped() {
        _ = DataManager.shared.removeAddress(id: address!.id).observeOn(ThreadUtil.shared.mainScheduler).subscribe{
            self.goBackButtonTapped()
        }
    }
    private func setData(){
        if let a = address{
            self.streetBlock.valueTextField.text = a.street
            self.housingBlock.valueTextField.text = a.entrance
            self.homeBlock.valueTextField.text = a.home
            self.apartmentBlock.valueTextField.text = a.apartment
            self.florBlock.valueTextField.text = a.floor
            self.dphoneBlock.valueTextField.text = a.doorphone
            self.commentBlock.valueTextField.text = a.comment
        }
    }
    
    @objc func saveButtonTapped(){
        if (streetBlock.valueTextField.text ?? "").count == 0 || (homeBlock.valueTextField.text ?? "").count == 0{
            return
        }
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.checkAddress(street: streetBlock.valueTextField.text ?? "", home: homeBlock.valueTextField.text ?? "")
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ b in
                if !b {
                    self.hideModalSpinner(indicator: spiner)
                    AlertUtil.alert({}, mes: "Мы не доставляем по указанному адресу")
                    return
                }
                if let a = self.address {
                    _ = DataManager.shared.updateAddress(id: a.id, street: self.streetBlock.valueTextField.text!, home: self.homeBlock.valueTextField.text!,
                                                         entrance: self.housingBlock.valueTextField.text ?? "", apartment: self.apartmentBlock.valueTextField.text ?? "" ,
                                                         floor: self.florBlock.valueTextField.text ?? "", doorphone: self.dphoneBlock.valueTextField.text ?? "",
                                                         comment: self.commentBlock.valueTextField.text ?? "").observeOn(ThreadUtil.shared.mainScheduler)
                        .subscribe{
                            self.hideModalSpinner(indicator: spiner)
                            self.goBackButtonTapped()
                    }
                    return
                }
                _ = DataManager.shared.addAddress(street: self.streetBlock.valueTextField.text!, home: self.homeBlock.valueTextField.text!,
                                                  entrance: self.housingBlock.valueTextField.text ?? "", apartment: self.apartmentBlock.valueTextField.text ?? "",
                                                  floor: self.florBlock.valueTextField.text ?? "", doorphone: self.dphoneBlock.valueTextField.text ?? "",
                                                  comment: self.commentBlock.valueTextField.text ?? "").observeOn(ThreadUtil.shared.mainScheduler)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe{
                        self.hideModalSpinner(indicator: spiner)
                        self.goBackButtonTapped()
                    }
            },onError: { t in
                self.hideModalSpinner(indicator: spiner)
                AlertUtil.alert({}, mes: t.localizedDescription)
            })
    }
        
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.registerForKeyboardNotifications()
        dismissKeyboard()
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
}
extension AddAddressViewController {
    
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize!.height) }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
    }
}

