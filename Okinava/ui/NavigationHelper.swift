//
//  NavigationHelper.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import UIKit

class NavigationHelper {
    static let shared = NavigationHelper()
    var window = UIWindow(frame: UIScreen.main.bounds)
    var rootViewController : UINavigationController?
    
    func show(_ controller: UIViewController){
        UIView.transition(with: self.window, duration: 0.8, options: .transitionCurlDown, animations: {
            self.rootViewController = UINavigationController()
            self.rootViewController?.viewControllers = [controller]
            self.rootViewController?.isNavigationBarHidden = true
            self.window.rootViewController = self.rootViewController
        })
        self.window.makeKeyAndVisible()
    }
    
    func showMain(){
        if (UserDefaults.standard.value(forKey: UD_IS_FIRST_LAUNCH) == nil) {
            UserDefaults.standard.set(true, forKey: UD_IS_FIRST_LAUNCH)
            show(TutorialViewController())
        } else {
            show(MainTabViewController())
        }
    }
    
    func showNext(_ controller: UIViewController, animated: Bool = true){
        DispatchQueue.main.async { self.rootViewController?.pushViewController(controller, animated: animated) }
    }
    
    func back(animated: Bool = true){ self.rootViewController?.popViewController(animated: animated)}
    
    func present(_ alert: UIAlertController, animated: Bool = true, completion: (()->Void)? = nil){
        self.rootViewController?.present(alert, animated: animated, completion: completion)
    }
}
