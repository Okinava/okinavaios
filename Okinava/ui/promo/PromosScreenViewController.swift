//
//  PromoScreenViewController.swift
//  Okinava
//
//  Created by _ on 09.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class PromosScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.leftButton = OK_ChatButton()
        view.rightButton = OK_BasketButton()
        view.prepareView()
        view.navigationTitle = "Новости и акции"
        return view
    }()
    private lazy var promoTableView: UITableView = {
        let view = UITableView()
        view.register(PromoScreenTableViewCell.self, forCellReuseIdentifier: PromoScreenTableViewCellIdentifer)
        view.delegate = self
        view.dataSource = self
        view.separatorStyle = .none
        view.backgroundColor = DEFAULT_BACKGROUND
        return view
    }()
    public var promos: [smartPromo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = DEFAULT_BACKGROUND
        view.addSubview(topNavigationView)
        view.addSubview(promoTableView)
        topNavigationView.snp.remakeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
        }
        promoTableView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(topNavigationView.snp.bottom).offset(10)
            make.bottom.equalToSuperview()
        }
    }
    
    override func loadView() {
        super.loadView()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
        if promos.count == 0 {
            loadData()
        }
    }
    
    public func loadData(){
        _ = DataManager.shared.getPromos()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ps in
                self.promos = ps
                self.promoTableView.reloadData()
            }, onError: { t in
                AlertUtil.alert({}, mes: t.localizedDescription)
            })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return self.promos.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PromoScreenTableViewCellIdentifer, for: indexPath) as! PromoScreenTableViewCell
        cell.promo = self.promos[indexPath.row]
        cell.prepareView()
        cell.setData()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(PromoScreenViewController(promo: promos[indexPath.row], promos: promos), animated: true)
    }
    
    func openPromoScreenHandler(promo: smartPromo){
        navigationController?.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(PromoScreenViewController(promo: promo, promos: promos), animated: true)
    }
}
