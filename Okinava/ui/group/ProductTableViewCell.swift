//
//  ProductTableViewCell.swift
//  Okinava
//
//  Created by _ on 11.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift

class ProductTableViewCell: UITableViewCell {
    var product : Products!
    private var productCurrentCount = 0
    private var mod: [Products] = []

    private lazy var productImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()
    private lazy var productTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor().hexStringToUIColor(0x22222)
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.5
        return view
    }()
    private lazy var productDescriptionLabel: UILabel = {
        let view = UILabel()
        view.textColor  = UIColor().hexStringToUIColor(0xAEAEAE)
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.numberOfLines = 4
        return view
    }()
    private lazy var productToCartButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.titleLabel?.textColor = DEFAULT_COLOR_RED
        view.addTarget(self, action: #selector(self.addProductToCartTapped), for: .touchUpInside)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.setTitle("В корзину", for: .normal)
        view.tintColor = DEFAULT_COLOR_RED
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.isOpaque = false
        return view
    }()
    private lazy var productPriceLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor().hexStringToUIColor(0x222222)
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14  )
        return view
    }()
    private lazy var productCountView: UIView = {
        let view = UIView()
        view.alpha = 0
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.addSubview(self.countLessButton)
        view.addSubview(self.countLabel)
        view.addSubview(self.countMoreButton)
        return view
    }()
    private lazy var countMoreButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("+", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.addTarget(self, action: #selector(self.countMoreButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var countLessButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("-", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.addTarget(self, action: #selector(self.countLessButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var countLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = "1x"
        view.textAlignment = .center
        return view
    }()
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()

    public func setData(product: Products) {
        self.product = product
        selectionStyle = .none
        backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        productTitleLabel.text  = product.name
        productDescriptionLabel.text = product.product_description
        if product.image.count > 0 { self.productImageView.sd_setImage(with: URL(string: product.image)) }
        productPriceLabel.text = self.product.price.cleanValue + " " + RUBLE_LETTER
        productTitleLabel.textColor = UIColor().hexStringToUIColor(0x22222)
        productTitleLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        
        self.updateCountView()
        productImageView.isHidden = false
        productDescriptionLabel.isHidden = false
        productPriceLabel.isHidden = false
        productToCartButton.isHidden = false
        productCountView.isHidden = false
        line.isHidden = false
        addSubview(productImageView)
        addSubview(productTitleLabel)
        addSubview(productDescriptionLabel)
        addSubview(productPriceLabel)
        addSubview(productToCartButton)
        addSubview(productCountView)
        addSubview(line)
        update()
        
        _ = Observable.zip(DataManager.shared.inOrder(product: product),DataManager.shared.getModificators(product: product))
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { (count,mod) in
                self.mod = mod
                if mod.count == 0 {
                    self.productCurrentCount = Int(count)
                }else{
                    self.productCurrentCount = 0
                }
                self.updateCountView()
            })
    }
    
    public func setData(name: String) {
        selectionStyle = .none
        backgroundColor = UIColor().hexStringToUIColor(0xFAFAFA)
        productTitleLabel.text  = name
        productImageView.isHidden = true
        productDescriptionLabel.isHidden = true
        productPriceLabel.isHidden = true
        productToCartButton.isHidden = true
        productCountView.isHidden = true
        line.isHidden = true
        productTitleLabel.textColor = DEFAULT_COLOR_RED
        productTitleLabel.font = FONT_ROBOTO_LIGHT?.withSize(14)
      
        addSubview(productTitleLabel)
        productTitleLabel.snp.remakeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    func update() {
        productImageView.snp.remakeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(-15)
            make.width.height.equalTo(116)
        }
        productTitleLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(self.productImageView.snp.right).offset(16)
            make.top.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-16)
        }
        productDescriptionLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(self.productImageView.snp.right).offset(16)
            make.top.equalTo(self.productTitleLabel.snp.bottom).offset(2)
            make.right.equalToSuperview().offset(-10)
        }
        productPriceLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(self.productImageView.snp.right).offset(16)
            make.bottom.equalToSuperview().offset(-20)
        }
        productToCartButton.snp.remakeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-15)
            make.width.equalTo(95)
        }
        productCountView.snp.remakeConstraints { (make) in
            make.edges.equalTo(self.productToCartButton)
        }
        countMoreButton.snp.remakeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.3)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
        }
        countLessButton.snp.remakeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.3)
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        countLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(self.countMoreButton.snp.right)
            make.right.equalTo(self.countLessButton.snp.left)
            make.centerY.equalToSuperview()
        }
        line.snp.remakeConstraints{ make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
    
    @objc func countMoreButtonTapped(){
        _ = DataManager.shared.addOrderAndCheck(product: product)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { res in
                if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                else{
                    self.changeProductCount(count: 1)
                }
            })
    }
    @objc func countLessButtonTapped(){
       self.changeProductCount(count: -1)
        _ = DataManager.shared.removeOrder(product: product, count: 1).subscribe()
    }
    
    private func changeProductCount (count: Int){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        if mod.count == 0 { self.productCurrentCount  += count }
        self.updateCountView()
    }
    
    private func updateCountView(){
        if (self.productCurrentCount <= 0){
            self.productCurrentCount  = 0
            self.productToCartButton.alpha = 1
            self.productCountView.alpha = 0
        } else {
            self.productToCartButton.alpha = 0
            self.productCountView.alpha = 1
        }
        self.countLabel.text =  String(self.productCurrentCount) + "x"
    }
    @objc func addProductToCartTapped (){
        if mod.count == 0 {
            _ = DataManager.shared.addOrderAndCheck(product: product)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { res in
                    if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                    else{ self.changeProductCount(count: 1) }
                })
        }else{
            var mes: [String] = []
            var voids: [(()->Void)] = []
            mod.forEach { (p) in
                var name = p.name
                if name.first == "-" { name.remove(at: name.startIndex) }
                mes.append(name + " + " + p.price.cleanValue + " " + RUBLE_LETTER)
                voids.append({
                    _ = DataManager.shared.addOrderAndCheck(product: self.product, modificator: p.id)
                        .observeOn(ThreadUtil.shared.mainScheduler)
                        .subscribe(onNext: { res in
                            if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                            else{ AlertUtil.alert({}, mes: "\(self.product.name) (\(name)) добавлен в корзину", title: "Ура!") }
                        })
                })
            }
            AlertUtil.myAlert("Выберите модификатор для " + product.name, messages: mes, voids: voids)
        }
    }
}
