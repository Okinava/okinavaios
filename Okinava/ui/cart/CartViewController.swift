//
//  CartViewController.swift
//  Okinava
//
//  Created by _ on 01.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class CartViewController: UIViewController {
    public var cartItems: [(Products,Products?,Int64)] = []
    public let itemsTV = UITableView()
    public let checkoutButton = ZFRippleButton()
    
    public var nView = UIView()
    public let topNavigationView = OK_TopNavigationBlock()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Корзина"
        self.navigationController?.isNavigationBarHidden = true
        self.prepareView()
        self.updateData()
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_ADD, object: nil, queue: OperationQueue.main){ notification in self.updateSum()}
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_REMOVE, object: nil, queue: OperationQueue.main){ notification in self.updateSum()}
        NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_UPDATE, object: nil, queue: OperationQueue.main){ notification in self.updateSum()}
    }
    private var o: Any?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = o { NotificationCenter.default.removeObserver(o!) }
        o = NotificationCenter.default.addObserver(forName: NotificationUtil.ORDERS_CLEAR, object: nil, queue: OperationQueue.main){ notification in NavigationHelper.shared.back() }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = o { NotificationCenter.default.removeObserver(o!) }
    }

    private func prepareView(){
        self.view.backgroundColor = DEFAULT_BACKGROUND
        self.itemsTV.backgroundColor = DEFAULT_BACKGROUND
        let promoCode = UILabel()
        
        //self.itemsTV.tableFooterView = promoCodeButton
        
        //tableFooterView.addSubview(promoCodeButton)
        
        self.view.addSubview(promoCode)
        promoCode.text = "Промокод: " + DataManager.shared.customerHelper.promo
        promoCode.textColor = DEFAULT_COLOR_RED
        promoCode.font = FONT_ROBOTO_REGULAR?.withSize(14)
        
        itemsTV.delegate = self
        itemsTV.dataSource = self
        itemsTV.register(CartItemTableViewCell.self, forCellReuseIdentifier: "cartItemCell")
        itemsTV.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.view.addSubview(self.itemsTV)
        view.addSubview(nView)
        nView.snp.remakeConstraints{ make in
            make.top.left.bottom.height.equalToSuperview()
            make.width.equalTo(60)
        }
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        nView.addGestureRecognizer(panGestureRecognizer)
        self.view.addSubview(self.topNavigationView)
        self.view.addSubview(self.checkoutButton)

        
        checkoutButton.addTarget(self, action: #selector(checkoutButtonTapped), for: .touchUpInside)
        checkoutButton.setTitle("  Оформить заказ  ", for: .normal)
        checkoutButton.layer.cornerRadius = 6
        checkoutButton.backgroundColor = DEFAULT_COLOR_RED
        checkoutButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
        
        checkoutButton.layer.shadowRadius = 16
        checkoutButton.layer.shadowOpacity = 0.1
        checkoutButton.layer.shadowOffset = CGSize(width: 0, height: 8)
        
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            if #available(iOS 11, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            } else {
                make.top.equalToSuperview().offset(16)
            }
        }
        
        promoCode.snp.remakeConstraints { (make) in
            if (DataManager.shared.customerHelper.promo.count > 0){
                make.height.equalTo(20)
                make.bottom.equalTo(checkoutButton.snp.top).offset(-10)
            }else{
                make.height.equalTo(0)
                make.bottom.equalTo(checkoutButton.snp.top).offset(0)
            }
            make.centerX.equalToSuperview()
        }
        
        self.checkoutButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(16)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-10)
            make.height.equalTo(45)
        }
        
        itemsTV.snp.makeConstraints { (make) in
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(10)
            make.right.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.bottom.equalTo(promoCode.snp.top).offset(-10)
        }
        
        
        self.topNavigationView.prepareView()
        self.topNavigationView.navigationTitle = "Корзина"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
    }

    @objc func checkoutButtonTapped(){
        if DataManager.shared.customerHelper.phone.count < 3{
            NavigationHelper.shared.showNext(AuthNumberViewController(need: true), animated: true)
            return
        }
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.getCustomer(phone: DataManager.shared.customerHelper.phone)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { cus in
                DataManager.shared.customerHelper.setDefault(phone: DataManager.shared.customerHelper.phone)
                DataManager.shared.customerHelper.setAll(customer: cus)
                self.checkSales(spiner: spiner, bonus: DataManager.shared.customerHelper.bonus)
            }, onError: { t in
                self.checkSales(spiner: spiner, bonus: 0)
            })
    }
    
    private func checkSales(spiner: UIActivityIndicatorView, bonus: Double){
        _ = Observable.from(cartItems)
            .concatMap({ (res) -> Observable<(Bool,String?)> in return DataManager.shared.checkProduct(product: res.0) })
            .toArray()
            .subscribeOn(ThreadUtil.shared.backScheduler)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { res in
                if res.contains(where: { !$0.0 }) {
                    self.hideModalSpinner(indicator: spiner)
                    AlertUtil.alert({}, mes: "Нельзя создать заказа. У Вас в корзине акционный товар. Акцию можно заказать в период в 13:00-16:00, предварительный заказ можно сделать позвонив в колл-центр  \n8 (843) 233-44-33")
                }else{
                    self.openIfTime(spiner: spiner,bonus: bonus)
                }
            })
    }
    
    private func openIfTime(spiner: UIActivityIndicatorView, bonus: Double){
        _ = DataManager.shared.getTime()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { (time) in
                self.hideModalSpinner(indicator: spiner)
                NavigationHelper.shared.showNext(OrderingViewController(time: time, bonus: bonus), animated: true)
            }, onError: { t in
                self.hideModalSpinner(indicator: spiner)
                AlertUtil.alert({}, mes: t.localizedDescription)
            })
    }
    
    func updateSum(){
        UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.updateData() })
    }
    
    public func updateData(){
        _ = DataManager.shared.getOrder().observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { os in
                self.cartItems = os
                self.itemsTV.reloadData()
                var totalSum: Double = 0
                for item in self.cartItems { totalSum += ((item.0.price + (item.1 != nil ? item.1!.price : 0)) * Double(item.2)) }
                self.checkoutButton.setTitle( ("Оформить заказ на " +  totalSum.cleanValue + RUBLE_LETTER).uppercased(), for: .normal)
            })
    }

    @objc func goBackButtonTapped(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 136
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartItems.count
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = self.contextualToggleDeleteAction(forRowAtIndexPath: indexPath)
        let swipeConfig = UISwipeActionsConfiguration(actions: [deleteAction])
        return swipeConfig
    }
    func contextualToggleDeleteAction(forRowAtIndexPath indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Удалить")
        { (contextAction: UIContextualAction, sourceView: UIView, completionHandler: (Bool) -> Void) in
            let item = self.cartItems[indexPath.row]
            completionHandler(true)
            _ = DataManager.shared.removeOrder(product: item.0, modificator: item.1?.id ?? "")
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe{
                    self.cartItems.remove(at: indexPath.row)
                    InteractionFeedbackManager.shared.makeFeedback()
                     UIView.transition(with: self.view, duration: 0.4, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { self.updateData() })
                }
        }
        action.backgroundColor = .red
        return action
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartItemCell", for: indexPath as IndexPath) as! CartItemTableViewCell
        cell.setData(item: self.cartItems[indexPath.row])
        return cell
    }
}

extension CartViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

