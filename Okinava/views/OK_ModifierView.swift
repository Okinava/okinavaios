//
//  OK_ModifierView.swift
//  Okinava
//
//  Created by _ on 07.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit

class OK_ModifierView: UIView {
    
    var haveSelectingView  = false
    let selectingImageView = UIImageView(image: #imageLiteral(resourceName: "selected_icon"))
    var product: Products? = nil
    let textLabel = UILabel()

    var padding = 16
    init( title: String, product: Products){
        super.init(frame: CGRect.zero)
        self.textLabel.text = title
        self.isOpaque = false
        self.product = product
        self.selectingImageView.alpha = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        
        
        
        self.addSubview(textLabel)
        
      
        self.textLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(self.padding)
            make.right.equalToSuperview().offset(-self.padding)
        }
        
        if (self.haveSelectingView){
            self.addSubview(self.selectingImageView)
            self.selectingImageView.snp.makeConstraints { (make) in
                make.height.equalTo(8)
                make.width.equalTo(12)
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-self.padding)
            }
        }
        
        self.textLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.textLabel.textColor = DEFAULT_COLOR_BLACK
        self.textLabel.adjustsFontSizeToFitWidth = true
        
    }
    
    func isSelecting(visible: Bool) {
        if (self.haveSelectingView) {
            self.selectingImageView.alpha = visible  ? 1 : 0
        }
    }
}
