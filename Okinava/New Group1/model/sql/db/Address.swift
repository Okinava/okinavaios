//
//  Address.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import SQLite

struct Address: SQLCommand {
    var id: Int64
    var phone: String
    var street: String
    var home: String
    var entrance: String // падик
    var apartment: String//кв
    var floor: String//этаж
    var doorphone: String//дп
    var comment: String
    
    init(id: Int64,
        phone: String,
        street: String,
        home: String,
        entrance: String,
        apartment: String,
        floor: String,
        doorphone: String,
        comment: String
        ) {
        self.id = id
        self.phone = phone
        self.street = street
        self.home = home
        self.entrance = entrance
        self.apartment = apartment
        self.floor = floor
        self.doorphone = doorphone
        self.comment = comment
    }
    
    static let id = Expression<Int64>("id")
    static let phone = Expression<String>("phone")
    static let street = Expression<String>("street")
    static let home = Expression<String>("home")
    static let entrance = Expression<String>("entrance")
    static let apartment = Expression<String>("apartment")
    static let floor = Expression<String>("floor")
    static let doorphone = Expression<String>("doorphone")
    static let comment = Expression<String>("comment")

    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Address.phone <- phone,
            Address.street <- street,
            Address.home <- home,
            Address.entrance <- entrance,
            Address.apartment <- apartment,
            Address.floor <- floor,
            Address.doorphone <- doorphone,
            Address.comment <- comment
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(Address.id == id).update(
            Address.street <- street,
            Address.home <- home,
            Address.entrance <- entrance,
            Address.apartment <- apartment,
            Address.floor <- floor,
            Address.doorphone <- doorphone,
            Address.comment <- comment
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Address.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Address.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Address(id: row[id], phone: row[phone], street: row[street], home: row[home], entrance: row[entrance], apartment: row[apartment],
                       floor: row[floor], doorphone: row[doorphone], comment: row[comment]) as! T
    }
    
    func tableName() -> String{ return "Address"}
    static func tableName() -> String{ return "Address"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Address.tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: .autoincrement)
            t.column(phone)
            t.column(street)
            t.column(home)
            t.column(entrance)
            t.column(apartment)
            t.column(floor)
            t.column(doorphone)
            t.column(comment)
        })
        return table
    }
}
