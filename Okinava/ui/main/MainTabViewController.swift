//
//  MainTabViewController.swift
//  Okinava
//
//  Created by _ on 01.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController, UITabBarControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        hidesBottomBarWhenPushed = true
        view.tintColor = DEFAULT_COLOR_RED
        
        let productNavigation = UINavigationController()
        let productsVC = GroupController()
        let productsVCItem = UITabBarItem(title: "Меню", image: UIImage(named: "menu-menu"), selectedImage: UIImage(named: "menu-menu"))
        productNavigation.viewControllers = [productsVC]
        productNavigation.tabBarItem = productsVCItem
        productNavigation.isNavigationBarHidden = true
        
        let profileNavigation = UINavigationController()
        let profileVC = ProfileViewController()
        let profileVCItem = UITabBarItem(title: "Профиль", image: UIImage(named: "menu-profile"), selectedImage:  UIImage(named: "menu-profile"))
        profileNavigation.tabBarItem = profileVCItem
        profileNavigation.viewControllers = [profileVC]
        profileNavigation.isNavigationBarHidden = true
        
        
        let aboutNavigation = UINavigationController()
        let aboutVC = AboutViewViewController()
        let aboutVCItem = UITabBarItem(title: "Контакты", image: UIImage(named: "menu-contacts"), selectedImage:  UIImage(named: "menu-contacts"))
        aboutNavigation.tabBarItem = aboutVCItem
        aboutNavigation.viewControllers = [ aboutVC ]
        aboutNavigation.isNavigationBarHidden = true
        
        
        let promoVC = PromosScreenViewController()
        let promoNavigation = UINavigationController()
        let promoVCItem = UITabBarItem(title: "Акции", image: UIImage(named: "menu-promo"), selectedImage: UIImage(named: "menu-promo"))
        promoNavigation.tabBarItem = promoVCItem
        promoNavigation.isNavigationBarHidden = true
        promoNavigation.viewControllers = [promoVC]
        
        viewControllers = [productNavigation, promoNavigation,  profileNavigation, aboutNavigation]
    }

    // UITabBarControllerDelegate method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if (self.selectedIndex == 0 && viewController.children.count > 0){
            (viewController.children[0] as? GroupController)?.scrollTableViewToTop()
        }
    }
}

