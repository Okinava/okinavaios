//
//  OK_TextFieldBordered.swift
//  Okinava
//
//  Created by _ on 20.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
class OK_TextFieldBordered: UITextField {
    
    let padding = UIEdgeInsets(top: 2, left: 10, bottom: 2, right: 10);
    override func draw(_ rect: CGRect) {
        self.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        self.layer.borderWidth = 1
        self.backgroundColor = .white
        self.textColor = DEFAULT_COLOR_BLACK
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
