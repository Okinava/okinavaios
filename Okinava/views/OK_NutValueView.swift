//
//  OK_NutValueView.swift
//  Okinava
//
//  Created by _ on 21.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class OK_NutValueView: UIView {

    var keyLabel = UILabel()
    var valueLabel = UILabel()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.isOpaque = false
        self.backgroundColor = .white
        self.keyLabel.backgroundColor = .white
        self.valueLabel.backgroundColor = .white
        self.addSubview(keyLabel)
        self.addSubview(valueLabel)
        self.keyLabel.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.keyLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.layer.backgroundColor = UIColor.white.cgColor
        self.valueLabel.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.valueLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        
        self.keyLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(120)
            make.height.equalToSuperview()
        }
        self.valueLabel.snp.makeConstraints { (make) in
            make.left.equalTo(keyLabel.snp.right)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    

}
