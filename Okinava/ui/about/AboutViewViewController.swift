//
//  AboutViewViewController.swift
//  Okinava
//
//  Created by _ on 20.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import RxSwift

class AboutViewViewController: UIViewController {
    private let aboutTableView = UITableView()
    private let aboutTableHeader = UIView()
    public let topNavigationView = OK_TopNavigationBlock()
    private var terminals: [smartRestorans] = []
    let segmentsView = UISegmentedControl(items: ["Списком", "На карте"])
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Контакты"
        view.backgroundColor = .white
        aboutTableView.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        self.aboutTableView.separatorStyle = .none
        self.aboutTableView.register(DeliveryTerminalTableViewCell.self, forCellReuseIdentifier: DeliveryTerminalTableViewCellIdentifer)
        self.prepareView()
        self.aboutTableView.dataSource = self
        self.aboutTableView.delegate = self
        self.loadNewData()
        self.hidesBottomBarWhenPushed = true
    }
    
    @objc private func phoneTapped(){
        guard let number = URL(string: "tel://+78432334433") else { return }
        UIApplication.shared.open(number)
    }
    
    
    @objc private func emailTapped(){
        guard let mail = URL(string: "mailto:commerce@okinavakazan.ru") else { return }
        UIApplication.shared.open(mail)
    }
    
    @objc private func webTapped(){
        guard let web = URL(string: "http://okinavakazan.ru/") else { return }
        UIApplication.shared.open(web)
    }
    
    private func loadNewData(){
        _ = DataManager.shared.getRestorans()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { rs in
                self.terminals = rs
                self.aboutTableView.reloadData()
            }, onError: { t in
                AlertUtil.alert({}, mes: t.localizedDescription)
            })
    }
    
    @objc private func instTapped(){
        
        let instagramHooks = "instagram://user?username=okinavakazan"
        let instagramUrl = URL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl!) {
            
            UIApplication.shared.open(instagramUrl!, options: [:], completionHandler: nil)
        } else {
            
            UIApplication.shared.open(URL(string: "http://instagram.com/okinavakazan")!, options: [:], completionHandler: nil)
            
        }
    }
    
    @objc private func vkTapped(){
        
        let vkHooks = "vk://vk.com/okinava_kazan"
        let vkUrl = URL(string: vkHooks)
        if UIApplication.shared.canOpenURL(vkUrl!) {
            UIApplication.shared.open(vkUrl!, options: [:], completionHandler: nil)
        } else {
            
            UIApplication.shared.open(URL(string: "http://vk.com/okinava_kazan")!, options: [:], completionHandler: nil)
            
        }
    }
    
    private func prepareView(){
        
        let contactsWrapper = UIView()
        let phoneView = OK_ContactView(image: #imageLiteral(resourceName: "icon_phone"), title: "8 (843) 233-44-33")
        let emailView = OK_ContactView(image: #imageLiteral(resourceName: "icon_mail"), title: "commerce@okinavakazan.ru")
        let vkView = OK_ContactView(image: #imageLiteral(resourceName: "icon_vk"), title: "vk.com/okinava_kazan")
        let webView = OK_ContactView(image: #imageLiteral(resourceName: "icon_web"), title: "okinavakazan.ru")
        let instView = OK_ContactView(image: #imageLiteral(resourceName: "icon_inst"), title: "instagram.com/okinavakazan")
        phoneView.isSelecting(visible: false)
        emailView.isSelecting(visible: false)
        vkView.isSelecting(visible: false)
        webView.isSelecting(visible: false)
        instView.isSelecting(visible: false)
        
        let phoneTappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.phoneTapped))
        phoneView.addGestureRecognizer(phoneTappedGesture)
        
        let emailTappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.emailTapped))
        emailView.addGestureRecognizer(emailTappedGesture)
        
        let webTappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.webTapped))
        webView.addGestureRecognizer(webTappedGesture)
        
        let instTappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.instTapped))
        instView.addGestureRecognizer(instTappedGesture)
        
        let vkTappedGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.vkTapped))
        vkView.addGestureRecognizer(vkTappedGesture)
        
        
        
        let labelForSocials = UILabel()
        
        labelForSocials.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        labelForSocials.text  = "Окинава в интернете"
        let dividerView = UIView()
        dividerView.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        
        self.aboutTableHeader.frame.size = CGSize(width: self.aboutTableView.frame.width, height: 330)
        
        self.aboutTableView.backgroundColor = DEFAULT_BACKGROUND
        segmentsView.selectedSegmentIndex = 0
        segmentsView.setTitleTextAttributes([NSAttributedString.Key.font: FONT_ROBOTO_REGULAR?.withSize(16) as Any], for: .normal)
        contactsWrapper.backgroundColor = .white
        contactsWrapper.layer.cornerRadius = 8
        contactsWrapper.layer.shadowOffset = CGSize(width: 0, height: 4)
        contactsWrapper.layer.shadowRadius = 4
        contactsWrapper.layer.shadowOpacity = 0.1
        
        
        self.view.backgroundColor = DEFAULT_BACKGROUND
        self.aboutTableView.backgroundColor = .white
        self.view.addSubview(topNavigationView)
        self.view.addSubview(self.aboutTableView)
        aboutTableHeader.backgroundColor = .white
        self.aboutTableHeader.addSubview(contactsWrapper)
        self.aboutTableHeader.addSubview(segmentsView)
        contactsWrapper.addSubview(phoneView)
        contactsWrapper.addSubview(emailView)
        contactsWrapper.addSubview(dividerView)
        contactsWrapper.addSubview(labelForSocials)
        contactsWrapper.addSubview(vkView)
        contactsWrapper.addSubview(webView)
        contactsWrapper.addSubview(instView)
        
        self.aboutTableView.parallaxHeader.view = aboutTableHeader
        aboutTableView.parallaxHeader.height = 340
        segmentsView.backgroundColor = .white
        aboutTableView.parallaxHeader.minimumHeight = 60
        aboutTableView.parallaxHeader.mode = .bottom
        aboutTableView.alwaysBounceVertical = true
        
        self.topNavigationView.leftButton = OK_ChatButton()
        self.topNavigationView.rightButton = OK_BasketButton()
        self.topNavigationView.prepareView()
        self.topNavigationView.navigationTitle = "Контакты"
        
        
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        self.aboutTableView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.top.equalTo(topNavigationView.snp.bottom).offset(10)
            make.bottom.equalToSuperview().offset(0)
        }
        contactsWrapper.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
        }
        
        phoneView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(35)
        }
        emailView.snp.makeConstraints { (make) in
            make.top.equalTo(phoneView.snp.bottom).offset(5)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(35)
        }
        
        dividerView.snp.makeConstraints { (make) in
            make.top.equalTo(emailView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(1)
        }
        
        labelForSocials.snp.makeConstraints { (make) in
            make.top.equalTo(dividerView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            
            
        }
        
        vkView.snp.makeConstraints { (make) in
            make.top.equalTo(labelForSocials.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(35)
        }
        instView.snp.makeConstraints { (make) in
            make.top.equalTo(vkView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(35)
        }
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(instView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.left.equalToSuperview()
            make.height.equalTo(35)
            make.bottom.equalToSuperview().offset(-10)
        }
        
        segmentsView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(40)
            make.top.equalTo(contactsWrapper.snp.bottom).offset(20)
        }
        self.segmentsView.addTarget(self, action: #selector(self.segmentValueChanged), for: .valueChanged)
    }
    
    @objc func segmentValueChanged(segment: UISegmentedControl){
        if (self.segmentsView.selectedSegmentIndex == 1){
            navigationController?.tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(OnMapScreenViewController(), animated: true)
            self.segmentsView.selectedSegmentIndex  = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
    }
}
extension AboutViewViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.terminals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DeliveryTerminalTableViewCellIdentifer, for: indexPath) as! DeliveryTerminalTableViewCell
        
        cell.terminal = self.terminals[indexPath.row]
        cell.prepareView()
        cell.setData()
        cell.selectionStyle = .none
        return cell
    }
    
    
}
