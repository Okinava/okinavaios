//
//  iikoStreets.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import Foundation
import Gloss

//MARK: - iikoStreets
public struct iikoStreets: Glossy {
    
    public var streets : [iikoStreet]!
    
    //MARK: Decodable
    public init?(json: JSON){
        streets = "streets" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "streets" ~~> streets,
            ])
    }
    
}


//MARK: - Street
public struct iikoStreet: Glossy {
    public var name : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        name = "name" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> name
            ])
    }
    
}

