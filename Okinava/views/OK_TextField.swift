//
//  OK_TextField.swift
//  Okinava
//
//  Created by _ on 19.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
class OK_TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
