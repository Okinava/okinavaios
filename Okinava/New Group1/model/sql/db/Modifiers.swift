//
//  Modifiers.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite

struct Modifiers: SQLCommand {
    var id: String = ""
    var maxAmount: Int64 = 0
    var minAmount: Int64 = 0
    var defaultAmount: Int64 = 0
    var modifiersId: String = ""
    var parentProduct: String = ""
    
    init(
        id: String = "",
        maxAmount: Int64 = 0,
        minAmount: Int64 = 0,
        defaultAmount: Int64 = 0,
        modifiersId: String = "",
        parentProduct: String = ""
    ) {
        self.id = id
        self.maxAmount = maxAmount
        self.minAmount = minAmount
        self.defaultAmount = defaultAmount
        self.modifiersId = modifiersId
        self.parentProduct = parentProduct
    }
    
    static let id = Expression<String>("id")
    static let maxAmount = Expression<Int64>("maxAmount")
    static let minAmount = Expression<Int64>("minAmount")
    static let defaultAmount = Expression<Int64>("defaultAmount")
    static let modifiersId = Expression<String>("modifiersId")
    static let parentProduct = Expression<String>("parentProduct")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Modifiers.id <- id,
            Modifiers.maxAmount <- maxAmount,
            Modifiers.minAmount <- minAmount,
            Modifiers.defaultAmount <- defaultAmount,
            Modifiers.modifiersId <- modifiersId,
            Modifiers.parentProduct <- parentProduct
        ))
    }
    
    func update(_ db: Connection, table: Table) {
    }
    
    func delete(_ db: Connection, table: Table) {
        //try! db.run(table.filter(Modifiers.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return false
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Modifiers(
            id: row[id],
            maxAmount: row[maxAmount],
            minAmount: row[minAmount],
            defaultAmount: row[defaultAmount],
            modifiersId: row[modifiersId],
            parentProduct: row[parentProduct]
            ) as! T
    }
    
    func tableName() -> String{ return "Modifiers"}
    static func tableName() -> String{ return "Modifiers"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Modifiers().tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id)
            t.column(maxAmount)
            t.column(minAmount)
            t.column(defaultAmount)
            t.column(modifiersId)
            t.column(parentProduct)
        })
        return table
    }
}

