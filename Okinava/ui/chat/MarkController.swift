//
//  MarkController.swift
//  Okinava
//
//  Created by Timerlan on 08.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import HCSStarRatingView

class MarkController: BaseController<MarkView>, UITextFieldDelegate {
    private let info: smartPush
    private let void: (()->Void)
    
    init(info: smartPush, void: @escaping (()->Void)){
        self.info = info
        self.void = void
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
//        mainView.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 500)
        self.hideKeyboardWhenTappedAround()
        mainView.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        mainView.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    @objc func nextButtonTapped(){
        let sp = showModalSpinner()
        _ = DataManager.shared.star(comment: mainView.orderCommentTextField.text ?? "", deliveryId: smartPush.getFrom(text: info.orders)!.orderId, time: info.datetime, id: info.id,
                                    f: Int(mainView.fStarView.value),
                                    s: Int(mainView.sStarView.value),
                                    t: Int(mainView.tStarView.value))
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { b in
                self.hideModalSpinner(indicator: sp)
                self.void()
                NavigationHelper.shared.back()
            })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterFromKeyboardNotifications()
    }

    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWasShown(notification: NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.mainView.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize!.height + 30) }
        let bottomOffset = CGPoint(x: 0, y: 300)
        self.mainView.scrollView.setContentOffset(bottomOffset, animated: true)
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        self.mainView.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
    }
}
    


class MarkView: BaseView {
    lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.prepareView()
        view.navigationTitle = "Отзыв о доставке"
        view.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        view.rightButton.removeFromSuperview()
        return view
    }()
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.addSubview(content)
        view.addSubview(nextButton)
        return view
    }()
    lazy var content: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.08
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addSubview(fLabel)
        view.addSubview(fStarView)
        view.addSubview(sLabel)
        view.addSubview(sStarView)
        view.addSubview(tLabel)
        view.addSubview(tStarView)
        view.addSubview(labelForComments)
        view.addSubview(orderCommentTextField)
        return view
    }()
    lazy var fLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(15)
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.text = "Понравилось ли гостю качество еды?"
        view.numberOfLines = 0
        return view
    }()
    lazy var fStarView: HCSStarRatingView = {
        let view = HCSStarRatingView()
        view.tintColor = DEFAULT_COLOR_RED
        return view
    }()
    lazy var sLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(15)
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.text = "Понравилось ли гостю качество работы оператора коллцентра?"
        view.numberOfLines = 0
        return view
    }()
    lazy var sStarView: HCSStarRatingView = {
        let view = HCSStarRatingView()
        view.tintColor = DEFAULT_COLOR_RED
        return view
    }()
    lazy var tLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(15)
        view.textColor = DEFAULT_COLOR_DARK_GRAY
        view.text = "Понравились ли гостю скорость и качество доставки еды?"
        view.numberOfLines = 0
        return view
    }()
    lazy var tStarView: HCSStarRatingView = {
        let view = HCSStarRatingView()
        view.tintColor = DEFAULT_COLOR_RED
        return view
    }()
    private lazy var labelForComments : UILabel = {
        let view = UILabel()
        view.text = "Комментарий"
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    lazy var orderCommentTextField: OK_TextField = {
        let view = OK_TextField()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.placeholder = "Введите ваш комментарий"
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        return view
    }()
    lazy var nextButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Отправить", for: .normal)
        view.setTitleColor(.white, for: .normal  )
        view.backgroundColor = DEFAULT_COLOR_RED
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.layer.cornerRadius = 6
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        return view
    }()

    
    override func setContent() {
        super.setContent()
        contentView.addSubview(topNavigationView)
        contentView.addSubview(scrollView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        topNavigationView.snp.remakeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.topMargin)
        }
        scrollView.snp.remakeConstraints{ make in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.right.left.bottom.width.equalToSuperview()
        }
        content.snp.remakeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-90)
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(16)
            make.width.equalToSuperview().offset(-32)
            make.right.equalToSuperview().offset(-16)
        }
        fLabel.snp.remakeConstraints{ make in
            make.left.top.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-30)
            make.right.equalToSuperview().offset(-15)
        }
        fStarView.snp.remakeConstraints{ make in
            make.top.equalTo(fLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
            make.width.equalTo(280)
        }
        sLabel.snp.remakeConstraints{ make in
            make.top.equalTo(fStarView.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-30)
            make.right.equalToSuperview().offset(-15)
        }
        sStarView.snp.remakeConstraints{ make in
            make.top.equalTo(sLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
            make.width.equalTo(280)
        }
        tLabel.snp.remakeConstraints{ make in
            make.top.equalTo(sStarView.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-30)
            make.right.equalToSuperview().offset(-15)
        }
        tStarView.snp.remakeConstraints{ make in
            make.top.equalTo(tLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.height.equalTo(40)
            make.width.equalTo(280)
        }
        labelForComments.snp.remakeConstraints{ make in
            make.top.equalTo(tStarView.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-30)
            make.right.equalToSuperview().offset(-15)
        }
        orderCommentTextField.snp.remakeConstraints{ make in
            make.top.equalTo(labelForComments.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(15)
            make.width.equalToSuperview().offset(-30)
            make.right.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview().offset(-30)
        }
        nextButton.snp.remakeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-30)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(45)
        }
    }
}
