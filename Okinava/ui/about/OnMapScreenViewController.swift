//
//  OnMapScreenViewController.swift
//  Okinava
//
//  Created by _ on 02.05.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MapKit
import CoreLocation
class OnMapScreenViewController: UIViewController  {
    let locationManager = CLLocationManager()
    public var restaraunts: [smartRestorans] = []
    public let topNavigationView = OK_TopNavigationBlock()
    /*let restaraunts = [
     "Ресторан на Фучика": [
     "coords": CLLocationCoordinate2D(latitude: 55.762014, longitude: 49.232439),
     "address": "Фучика 88"
     ],
     
     "Ресторан Университетская": [
     "coords": CLLocationCoordinate2D(latitude: 55.788211, longitude: 49.121299),
     "address": "Университетская, 10"
     ],
     "Ресторан Копылова": [
     "coords": CLLocationCoordinate2D(latitude: 55.857882, longitude: 49.085789),
     "address": "Копылова, 14"
     ],
     "Ресторан Пр.Ямашева 82" : [
     "coords": CLLocationCoordinate2D(latitude: 55.827590, longitude: 49.145536),
     "address": "Пр.Ямашева, 82 "
     ],
     "Ресторан Пр.Ямашева 43": [
     "coords": CLLocationCoordinate2D(latitude: 55.826367, longitude: 49.118335),
     "address": "Пр.Ямашева, 43а"
     ],
     "Ресторан Бигичева": [
     "coords": CLLocationCoordinate2D(latitude: 55.783291, longitude: 49.222566),
     "address": "Бигичева, 3 "
     ],
     
     "Ресторан Глушко": [
     "coords": CLLocationCoordinate2D(latitude: 55.785189, longitude: 49.232214),
     "address": "Глушко, 17а"
     ]
     ]
     */
    let mainMap = MKMapView()
    let initialLocation = CLLocation(latitude: 55.806941, longitude: 49.175699)
    let regionRadius: CLLocationDistance = 10000
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.title = "На карте"
        self.prepareView()
        
        self.locationManager.requestWhenInUseAuthorization()
        print(CLLocationManager.locationServicesEnabled())
        if (CLLocationManager.locationServicesEnabled())
        {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()
        }
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.getRestorans()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { rs in
                self.hideModalSpinner(indicator: spiner)
                self.restaraunts = rs
                self.loadData()
            },onError: { e in
                self.hideModalSpinner(indicator: spiner)
                AlertUtil.alert({}, mes: e.localizedDescription)
            })
    }
    
    private func prepareView() {
        view.backgroundColor = .white
        self.view.addSubview(topNavigationView)
        self.view.addSubview(mainMap)
        self.topNavigationView.prepareView()
        self.topNavigationView.navigationTitle = "На карте"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.rightButton.removeFromSuperview()
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            if #available(iOS 11, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            } else {
                make.top.equalToSuperview().offset(16)
            }
        }
        
        mainMap.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.topNavigationView.snp.bottom)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        self.mainMap.showsUserLocation = true
        
        self.centerMapOnLocation(location: initialLocation)
    }
    
    private func loadData(){
        for rest in self.restaraunts {
            let coordString = rest.coordinates.components(separatedBy: " ")
            let coords =   CLLocationCoordinate2D(latitude:  Double(coordString[1])!, longitude: Double(coordString[0])!)
            self.mainMap.addAnnotation( RestaurantMapViewAnnotation(title: rest.name, locationName: rest.address, discipline:rest.address , coordinate: coords ))
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mainMap.setRegion(coordinateRegion, animated: true)
    }
    @objc func goBackButtonTapped(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension OnMapScreenViewController : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mainMap.setRegion(region, animated: true)
    }
}
