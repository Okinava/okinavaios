//
//  OK_ShareButton.swift
//  Okinava
//
//  Created by _ on 24.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation

import UIKit

class OK_ShareButton: UIButton {
    
    var isWithShadow : Bool = false
    override func draw(_ rect: CGRect) {
        self.setImage(#imageLiteral(resourceName: "ic_share_black"), for: .normal)
        self.backgroundColor = .white
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.cornerRadius = 4
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        
        if isWithShadow {
            
            self.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 8
            
        }
        
    }
    
    
    
}
