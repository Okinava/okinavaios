//
//  PaymentViewController.swift
//  Okinava
//
//  Created by _ on 19.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import PassKit
import SnapKit
import RxSwift
import Crashlytics

class PaymentViewController: SwipeRightToPopViewController {
    let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
    let ApplePaySwagMerchantID = "merchant.ru.smart-resto"
    public let topNavigationView = OK_TopNavigationBlock()
    private let customerNameLabel = UILabel()
    private let customerPhoneLabel = UILabel()
    private let addressLabel = UILabel()
    private let payTypeCashButton = OK_PaymentButton()
    private let payTypeDeliverButton = OK_PaymentButton()
    private let payTypeOnlineButton = OK_PaymentButton()
    private let payTypeApplePayButton = OK_PaymentButton()
    private let infButton = OK_PaymentButton()
    private let bonusesSlider = UISlider()
    private let bonusesCountTextField = OK_TextField()
    private let orderItemsStackView = UIStackView()
    private let orderTimeLabel = UILabel()
    private let byBonesesLabel = UILabel()
    private var totalPrice: Double = 0
    private var cartItems: [(Products,Products?,Int64)] = []
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private var previousValue: Float = 0
    private var isCheckoutPressed = false
    private let time: Date
    private let timeNow: Date
    private let address: Address?
    private let terminal: IikoDeliveryTerminal?
    private let bonus: Int
    private let comment: String
    private let bonusesSliderView = UIView()
    init(time: Date, address: Address? = nil, terminal: IikoDeliveryTerminal? = nil, bonus: Double, comment: String, timeNow: Date) {
        self.time = time
        self.timeNow = timeNow
        self.address = address
        self.terminal = terminal
        self.bonus = Int(bonus)
        self.comment = comment
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        let spin = showModalSpinner()
        _ = DataManager.shared.getOrder()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .do(onNext: { os in
                self.cartItems = os;
                self.setData()
            })
            .concatMap({ _ -> Observable<String?> in
                return DataManager.shared.checkSale(self.checkOutButtonTapped(paymentTypeName: .byCash))
            })
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { text in
                if text != nil{
                    let lView = UIView()
                    self.orderItemsStackView.addArrangedSubview(lView)
                    let label = UILabel()
                    label.numberOfLines = 0
                    label.textColor = DEFAULT_COLOR_RED
                    label.font = FONT_ROBOTO_MEDIUM?.withSize(12)
                    label.text = text ?? ""
                    lView.addSubview(label)
                    label.snp.makeConstraints { (make) in
                        make.left.equalToSuperview().offset(20)
                        make.width.equalToSuperview().offset(-40)
                        make.top.equalToSuperview().offset(20)
                        make.bottom.equalToSuperview()
                    }
                }
                self.hideModalSpinner(indicator: spin)
            })
    }
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.goBackButtonTapped()
        }
    }
    
    @objc func handlePan(_ panGesture: UIPanGestureRecognizer) {
        let percent = panGesture.location(in: view).x / bonusesSliderView.frame.width
        bonusesSlider.value = Float(Int(percent * CGFloat(bonus)))
        bonusesSliderValueChanged(bonusesSlider)
    }
    
    private func prepareView(){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        bonusesSliderView.addGestureRecognizer(panGesture)
        scrollView.alwaysBounceVertical = true
//        self.orderItemsStackView.alignment =  UIStackViewAlignment.top
        self.orderItemsStackView.axis = .vertical
        self.orderItemsStackView.distribution = .fillProportionally
        self.orderItemsStackView.alignment = .fill
        self.view.backgroundColor = DEFAULT_BACKGROUND
        self.contentView.backgroundColor = DEFAULT_BACKGROUND
        self.scrollView.backgroundColor = DEFAULT_BACKGROUND
        
        self.payTypeCashButton.addTarget(self, action: #selector(self.checkOutByCashTapped), for: .touchUpInside)
        self.payTypeDeliverButton.addTarget(self, action: #selector(self.checkOutByDeliveryCardTapped), for: .touchUpInside)
        self.payTypeOnlineButton.addTarget(self, action: #selector(self.payTypeOnlineButtonTapped), for: .touchUpInside)

        self.payTypeApplePayButton.addTarget(self, action: #selector(self.payAsApplePayButtonTapped), for: .touchUpInside)
        self.infButton.addTarget(self, action: #selector(self.infButtonTapped), for: .touchUpInside)
        let imageForCustomerName = UIImageView(image: #imageLiteral(resourceName: "icon_user"))
        let imageForCustomerPhone = UIImageView(image: #imageLiteral(resourceName: "icon_phone"))
        let imageForOrderTime = UIImageView(image: #imageLiteral(resourceName: "icon_clock"))
        let imageForAddress = UIImageView(image: #imageLiteral(resourceName: "icon_home"))
        let orderDetailView = UIView()
        let orderItemsView = UIView()
        let labelForDetails = UILabel()
        let labelForItems = UILabel()
        
        imageForCustomerPhone.contentMode = .scaleAspectFit
        imageForCustomerName.contentMode = .scaleAspectFit
        
        orderDetailView.backgroundColor = .white
        orderDetailView.layer.cornerRadius = 8
        orderDetailView.layer.shadowOffset = CGSize(width: 0, height: 8 )
        orderDetailView.layer.shadowRadius = 16
        orderDetailView.layer.shadowOpacity = 0.08
        
        orderItemsView.backgroundColor = .white
        orderItemsView.layer.cornerRadius = 8
        orderItemsView.layer.shadowOffset = CGSize(width: 0, height: 8 )
        orderItemsView.layer.shadowRadius = 16
        orderItemsView.layer.shadowOpacity = 0.08
        
        labelForItems.text = "Товары"
        labelForItems.font = FONT_ROBOTO_BOLD?.withSize(18)
        labelForItems.textColor = DEFAULT_COLOR_BLACK
        
        labelForDetails.text = "Данные о заказе"
        labelForDetails.font = FONT_ROBOTO_BOLD?.withSize(18)
        labelForDetails.textColor = DEFAULT_COLOR_BLACK
        
        self.payTypeCashButton.setTitle("Наличными", for: .normal)
        self.payTypeCashButton.layer.backgroundColor = UIColor.white.cgColor
        self.payTypeCashButton.setTitleColor(DEFAULT_COLOR_BLACK, for: .normal)
        self.payTypeCashButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        self.payTypeCashButton.layer.shadowOffset = CGSize(width: 0, height: -1)
        self.payTypeCashButton.layer.shadowColor = UIColor().hexStringToUIColor(0xEBEBEB).cgColor
        self.payTypeCashButton.layer.shadowRadius = 0
        
        self.payTypeDeliverButton.setTitle("Картой курьеру", for: .normal)
        self.payTypeDeliverButton.layer.backgroundColor = UIColor.white.cgColor
        self.payTypeDeliverButton.setTitleColor(DEFAULT_COLOR_BLACK, for: .normal)
        self.payTypeDeliverButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        self.payTypeDeliverButton.layer.shadowOffset = CGSize(width: 0, height: -1)
        self.payTypeDeliverButton.layer.shadowColor = UIColor().hexStringToUIColor(0xEBEBEB).cgColor
        self.payTypeDeliverButton.layer.shadowRadius = 0
        payTypeDeliverButton.isHidden = true
        
        
        
        self.payTypeOnlineButton.setTitle("Картой онлайн", for: .normal)
        self.payTypeOnlineButton.layer.backgroundColor = UIColor.white.cgColor
        self.payTypeOnlineButton.setTitleColor(DEFAULT_COLOR_BLACK, for: .normal)
        self.payTypeOnlineButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)

        self.payTypeApplePayButton.setTitle("Apple Pay", for: .normal)
        self.payTypeApplePayButton.layer.backgroundColor = UIColor.white.cgColor
        self.payTypeApplePayButton.setTitleColor(DEFAULT_COLOR_BLACK, for: .normal)
        self.payTypeApplePayButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)

        self.infButton.setTitle("Информации о доставке", for: .normal)
        self.infButton.layer.backgroundColor = UIColor.white.cgColor
        self.infButton.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        self.infButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(13)
        infButton.rightArrow.isHidden = true
        infButton.line.isHidden = true
        
        
        
        self.addressLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.addressLabel.textColor = DEFAULT_COLOR_BLACK
        self.addressLabel.adjustsFontSizeToFitWidth = true
        
        
        
        self.customerNameLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.customerNameLabel.textColor = DEFAULT_COLOR_BLACK
        self.customerNameLabel.adjustsFontSizeToFitWidth = true
        
        self.customerPhoneLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.customerPhoneLabel.textColor = DEFAULT_COLOR_BLACK
        self.customerPhoneLabel.adjustsFontSizeToFitWidth = true
        
        self.orderTimeLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.orderTimeLabel.textColor = DEFAULT_COLOR_BLACK
        self.orderTimeLabel.adjustsFontSizeToFitWidth = true
        
        
        self.view.addSubview(self.topNavigationView)
        self.view.addSubview(self.scrollView)
        self.scrollView.addSubview(self.contentView)
        self.contentView.addSubview(labelForDetails)
        self.contentView.addSubview(orderDetailView)
        self.contentView.addSubview(labelForItems)
        self.contentView.addSubview(orderItemsView)
        self.contentView.addSubview(self.payTypeCashButton)
        self.contentView.addSubview(self.payTypeDeliverButton)
        self.contentView.addSubview(self.payTypeOnlineButton)
        self.contentView.addSubview(self.payTypeApplePayButton)
        self.contentView.addSubview(self.infButton)

        
        
        orderDetailView.addSubview(imageForCustomerName)
        orderDetailView.addSubview(self.customerNameLabel)
        orderDetailView.addSubview(imageForCustomerPhone)
        orderDetailView.addSubview(self.customerPhoneLabel)
        
        orderDetailView.addSubview(imageForAddress)
        orderDetailView.addSubview(self.addressLabel)
        
        orderDetailView.addSubview(imageForOrderTime)
        orderDetailView.addSubview(self.orderTimeLabel)
        
        orderItemsView.addSubview(self.orderItemsStackView)
        
        
        self.topNavigationView.prepareView()
        self.topNavigationView.navigationTitle = "Оплата заказа"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
        
        
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        self.scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(10)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        self.contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(scrollView)
            make.left.right.equalTo(view)
        }
        labelForDetails.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(16)
        }
        
        orderDetailView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(labelForDetails.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            
            
        }
        
        imageForCustomerName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(18)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
            
        }
        
        self.customerNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForCustomerName)
            make.left.equalTo(imageForCustomerName.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        imageForCustomerPhone.snp.makeConstraints { (make) in
            make.top.equalTo(imageForCustomerName.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
            
        }
        self.customerPhoneLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForCustomerPhone)
            make.left.equalTo(imageForCustomerPhone.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        
        imageForAddress.snp.makeConstraints { (make) in
            make.top.equalTo(imageForCustomerPhone.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
        }
        
        self.addressLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForAddress)
            make.left.equalTo(imageForAddress.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        imageForOrderTime.snp.makeConstraints { (make) in
            make.top.equalTo(imageForAddress.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        self.orderTimeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForOrderTime)
            make.left.equalTo(imageForOrderTime.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        labelForItems.snp.makeConstraints { (make) in
            make.top.equalTo(orderDetailView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(16)
        }
        
        orderItemsView.snp.makeConstraints { (make) in
            make.top.equalTo(labelForItems.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.centerX.equalToSuperview()
            
            
        }
        self.orderItemsStackView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-20)
        }
        
        
        self.payTypeCashButton.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.left.right.equalToSuperview()
            make.top.equalTo(orderItemsView.snp.bottom).offset(20)
            
        }
        self.payTypeDeliverButton.snp.makeConstraints { (make) in
//            make.height.equalTo(48)
            make.height.equalTo(0)
            make.left.right.equalToSuperview()
            make.top.equalTo(self.payTypeCashButton.snp.bottom)
//            make.top.equalTo(self.payTypeCashButton.snp.bottom).offset(1)
        }
        
        self.payTypeOnlineButton.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.left.right.equalToSuperview()
            make.top.equalTo(self.payTypeDeliverButton.snp.bottom).offset(1)
        }
        self.payTypeApplePayButton.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.left.right.equalToSuperview()
            make.top.equalTo(self.payTypeOnlineButton.snp.bottom).offset(1)
        }
        self.infButton.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview()
            make.top.equalTo(self.payTypeApplePayButton.snp.bottom).offset(10)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
        
    }
    private func setData(){
        self.customerNameLabel.text = DataManager.shared.customerHelper.name
        self.customerPhoneLabel.text = DataManager.shared.customerHelper.phone
        let calendar = Calendar(identifier: .gregorian)
        if calendar.component(Calendar.Component.day, from: time) != calendar.component(Calendar.Component.day, from: timeNow) {
            self.orderTimeLabel.text = "Доставить " + time.toUserFormatString(format: "HH:mm dd.MM.YYYY")}else{
            self.orderTimeLabel.text = "Доставить к " + time.toUserFormatString(format: "HH:mm")
        }
        if let t = terminal {
            self.addressLabel.text = t.address.replacingOccurrences(of: "Россия, Республика Татарстан, Казань, ", with: "")
        } else {
            self.addressLabel.text = "\(address!.street) \(address!.home)"
        }
        for  cartItem in self.cartItems {
            let cartItemView = UIView()
            let cartItemTitle = UILabel()
            let cartItemCount = UILabel()
            let cartItemPrice = UILabel()
            cartItemView.backgroundColor = .white
            cartItemView.addSubview(cartItemTitle)
            cartItemView.addSubview(cartItemCount)
            cartItemView.addSubview(cartItemPrice)
            cartItemView.frame.size  = CGSize(width: self.orderItemsStackView.frame.width, height: 46)
            
            self.orderItemsStackView.addArrangedSubview(cartItemView)
            cartItemView.snp.makeConstraints { (make) in
                make.height.equalTo(46)
            }
            cartItemTitle.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemCount.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemPrice.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemPrice.textAlignment = .right
            cartItemTitle.adjustsFontSizeToFitWidth = true
            cartItemTitle.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(20)
                make.width.equalToSuperview().multipliedBy(0.6)
            }
            cartItemPrice.snp.makeConstraints { (make) in
                make.right.equalToSuperview().offset(-20)
                make.centerY.equalToSuperview()
                make.width.equalTo(40)
                
            }
            cartItemCount.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.right.equalTo(cartItemPrice.snp.left).offset(-20)
            }
            
            cartItemTitle.text = "\(cartItem.0.name) \(cartItem.1?.name ?? "")"
            cartItemPrice.text = (Double(cartItem.2) * (cartItem.0.price + (cartItem.1?.price ?? 0))).cleanValue + RUBLE_LETTER
            cartItemCount.text = "x" + String(cartItem.2)
            self.totalPrice += (Double(cartItem.2) * (cartItem.0.price + (cartItem.1?.price ?? 0)))
        }
        
        let totalView = UIView()
        let labelForTotal = UILabel()
        let totalLabel = UILabel()
        
        totalView.frame.size  = CGSize(width: self.orderItemsStackView.frame.width, height: 46)
        totalLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        labelForTotal.font = FONT_ROBOTO_REGULAR?.withSize(12)
        labelForTotal.textColor = DEFAULT_COLOR_GRAY
        labelForTotal.text  = "Итого:"
        totalLabel.textAlignment = .right
        totalView.addSubview(labelForTotal)
        totalView.addSubview(totalLabel)
        
        self.orderItemsStackView.addArrangedSubview(totalView)
        totalView.snp.makeConstraints { (make) in
            make.height.equalTo(46)
        }
        
        labelForTotal.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(10)
        }
        
        totalLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(10)
        }
        
        totalLabel.text = self.totalPrice.cleanValue + RUBLE_LETTER
        
        let byBonusesView = UIView()
        let forByBonusesLabel = UILabel()
        
        forByBonusesLabel.text = "Оплата бонусами"
        forByBonusesLabel.numberOfLines = 0
        
        byBonusesView.addSubview(forByBonusesLabel)
        byBonusesView.addSubview(self.byBonesesLabel)
        
        self.byBonesesLabel.textColor = DEFAULT_COLOR_GREEN
        self.byBonesesLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        
        forByBonusesLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        forByBonusesLabel.textColor = DEFAULT_COLOR_GRAY
        
        forByBonusesLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.width.equalToSuperview().offset(-40)
            make.top.equalToSuperview()
        }
        self.byBonesesLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview()
        }
        
        self.orderItemsStackView.addArrangedSubview(byBonusesView)
        
        let bonusesView = UIView()
        bonusesView.backgroundColor = .white
        
        let labelForBonusesCount = UILabel()
        labelForBonusesCount.text = "Списать бонусы"
        labelForBonusesCount.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        labelForBonusesCount.textColor = DEFAULT_COLOR_BLACK
        bonusesView.addSubview(labelForBonusesCount)
        bonusesView.addSubview(self.bonusesCountTextField)
        
        bonusesSliderView.addSubview(self.bonusesSlider)
        self.bonusesSlider.minimumValue = 0
        self.bonusesSlider.maximumValue = Float(calcMaxBonuses())
        self.bonusesSlider.value = self.bonusesSlider.maximumValue
        self.byBonesesLabel.text = String(calcMaxBonuses()) + " " +  RUBLE_LETTER
        self.bonusesCountTextField.text  = String(Int(calcMaxBonuses()))
        self.orderItemsStackView.addArrangedSubview(bonusesView)
        self.orderItemsStackView.addArrangedSubview(bonusesSliderView)
        
        self.bonusesCountTextField.textAlignment = .center
        self.bonusesCountTextField.layer.borderWidth = 1
        self.bonusesCountTextField.layer.borderColor = UIColor().hexStringToUIColor(0xEBEBEB).cgColor
        self.bonusesCountTextField.layer.cornerRadius = 4
        
        if bonus == 0 {
            bonusesView.isHidden = true
            bonusesSliderView.isHidden = true
            byBonesesLabel.isHidden = true
            forByBonusesLabel.text = "Делайте заказ и получайте гарантированно 4% от суммы каждого заказа на доставку и 10% от заказа, который хотите забрать самостоятельно с одного из наших кафе."
            byBonusesView.snp.makeConstraints { (make) in
                make.height.equalTo(60)
            }
        }else{
            byBonusesView.snp.makeConstraints { (make) in make.height.equalTo(30) }
        }
        bonusesView.snp.makeConstraints { (make) in
            make.height.equalTo(56)
        }
        
        self.bonusesCountTextField.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
            make.width.equalTo(115)
        }
        
        labelForBonusesCount.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
        }
        
        bonusesSliderView.snp.makeConstraints { (make) in
            make.height.equalTo(46)
        }
        self.bonusesSlider.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
        
        self.bonusesSlider.addTarget(self, action: #selector(self.bonusesSliderValueChanged), for: .valueChanged)
        self.bonusesSlider.tintColor = DEFAULT_COLOR_RED
        
        self.bonusesCountTextField.backgroundColor = .white
        self.bonusesCountTextField.snp.makeConstraints { (make) in make.height.equalTo(40) }
        self.bonusesCountTextField.isEnabled = false
    }
    
    @objc func bonusesSliderValueChanged(_ sender: UISlider){
        if   abs(Int(sender.value - self.previousValue)) > 15 {
            InteractionFeedbackManager.shared.makeFeedback()
            self.previousValue = sender.value
        }
        self.bonusesCountTextField.text =  Double(round(sender.value)).cleanValue
        self.byBonesesLabel.text = Double(round(sender.value)).cleanValue + " " +  RUBLE_LETTER
        
    }
    private func calcMaxBonuses() -> Int{
        var maxBonuses = min(bonus, 500)
        maxBonuses = min(maxBonuses, Int(totalPrice/2))
        return Int(maxBonuses)
    }
    
    @objc func checkOutByCashTapped(){
        addOrder(checkOutButtonTapped(paymentTypeName: .byCash))
    }
    @objc func checkOutByDeliveryCardTapped(){
        addOrder(checkOutButtonTapped(paymentTypeName: .byCardInDelivery))
    }
    
    @objc func payTypeOnlineButtonTapped(){
        let order = checkOutButtonTapped(paymentTypeName: .byCard)
        let spiner = self.showModalSpinner()
        let bonusesCount  = Int(self.bonusesCountTextField.text ?? "0") ?? 0
        let itog = Int64(totalPrice - Double(bonusesCount)) * 100
        _ = DataManager.shared.sberPay(amount: itog)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { url in
                self.hideModalSpinner(indicator: spiner)
                NavigationHelper.shared.showNext(SberPayController(url: url, order: order))
            },onError: { e in
                self.hideModalSpinner(indicator: spiner)
                 AlertUtil.alert({}, mes: e.localizedDescription)
            })
    }
    
    private func addOrder(_ order: IikoOrders){
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.addOrderFinale(order)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { result in
                self.hideModalSpinner(indicator: spiner)
                if result {
                    NavigationHelper.shared.showNext(ThankYouViewController())
                }else{
                    AlertUtil.alert({}, mes: "К сожалению, нам не удалось создать заказ. Позвоните нам для завершения создания заказа \n8 (843) 233-44-33")
                }
            })
    }
    
    func checkOutButtonTapped(paymentTypeName: PaymentTypes) -> IikoOrders{
        var listO: [IikoItem] = []
        var sum: Double = 0;
        cartItems.forEach { (o) in
            var item: IikoItem = IikoItem(id: o.0.id, name: o.0.name, count: o.2, code: o.0.code);
            if let m = o.1{
                var modifiersList: [IikoModifier] = []
                modifiersList.append(IikoModifier(id: m.id, name: m.name, groupId: m.groupId));
                item.modifiers = modifiersList;
            }
            item.sum = Double(o.2) * (o.0.price + (o.1?.price ?? 0.0))
            listO.append(item)
            sum += Double(o.2) * (o.0.price + (o.1?.price ?? 0.0))
        }
        var cusOrder: IikoOrders = IikoOrders(organization: IikoHelper.orgId, customer: iikoCustomer(phone: DataManager.shared.customerHelper.phone, name: DataManager.shared.customerHelper.name));
        var order: IikoOrder = IikoOrder(phone: DataManager.shared.customerHelper.phone, isSelfService: address == nil, comment: comment, orderTypeId: address != nil ? "bb6f5cee-1fb5-45a2-bf6f-aecd0a2e98d8" : "4229b272-a67e-44cd-8f71-88d1e4373296", date: time.toUserFormatString(format: "YYYY-MM-dd HH:mm:ss"), items: listO);
        if (address == nil){
            cusOrder.deliveryTerminalId = terminal?.deliveryTerminalId ?? ""
        }else{
            order.address = iikoAddress(street: address!.street, home: address!.home, entrance: address!.entrance,
                                        apartment: address!.apartment, floor: address!.floor, doorphone: address!.doorphone, comment: address!.comment)
        }
        
        var paymentItems: [IikoPaymentItem] =  []
        let bonusesCount = Int(self.bonusesCountTextField.text ?? "0") ?? 0
        var pItem = IikoPaymentItem(sum: String(sum - Double(bonusesCount)), isProcessedExternally: paymentTypeName == .byCard)
        var pType = IikoPaymentType();
        switch (paymentTypeName) {
        case .byCash:
            pType.name = "Наличные"
            pType.code = "CASH"
            pType.id   = "09322f46-578a-d210-add7-eec222a08871"
            break
        case .byCardInDelivery:
            pType.name = "Карта"
            pType.code = "ZCARD"
            pType.id = "371e1133-9ac2-41f1-aea5-d5791c658733"
            break
        case .byCard:
            pType.name = "МП Онлайн"
            pType.code = "MCARD"
            pType.id = "ed4ca935-3e09-4f89-bbe9-8aad1bea7d62"
            break
        }
        pItem.paymentType = pType
        paymentItems.append(pItem);
        if (bonusesCount > 0) {
            var pItemBonus = IikoPaymentItem(sum: String(bonusesCount), isProcessedExternally: false)
            pItemBonus.additionalData = "{\"searchScope\": \"PHONE\", \"credential\": \"" + DataManager.shared.customerHelper.phone + "\"}";
            var pTypeBonus = IikoPaymentType();
            pTypeBonus.name = "iikoCard5"
            pTypeBonus.code = "INET"
            pTypeBonus.id = "e681c306-a257-4071-ac5f-23e3c573b968"
            pItemBonus.paymentType = pTypeBonus
            paymentItems.append(pItemBonus);
        }
        order.paymentItems = paymentItems
        cusOrder.order = order
        return cusOrder
    }
    
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    @objc func infButtonTapped(){
        NavigationHelper.shared.showNext(InfoController())
    }
    
    @objc func payAsApplePayButtonTapped(){
        if (self.isCheckoutPressed == true){ return }
        self.isCheckoutPressed = true
        let request = PKPaymentRequest()
        request.merchantIdentifier = ApplePaySwagMerchantID
        request.supportedNetworks = SupportedPaymentNetworks
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "RU"
        request.currencyCode = "RUB"
        for o in cartItems {
            let productAmount = NSDecimalNumber(value: Double(o.2) * (o.0.price + (o.1?.price ?? 0.0)))
            request.paymentSummaryItems.append(PKPaymentSummaryItem(label: "\(o.0.name) \( o.1?.name ?? "" )", amount: productAmount))
        }
        let bonusesCount  = Int(self.bonusesCountTextField.text ?? "0") ?? 0
        let itog = NSDecimalNumber(value: (self.totalPrice - Double(bonusesCount)))
        request.paymentSummaryItems.append(PKPaymentSummaryItem(label: "Окинава", amount: itog))
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        applePayController?.delegate = self
        self.present(applePayController!, animated: true, completion: nil)
    }
    
    private var completion: ((PKPaymentAuthorizationStatus) -> Void)?
    func comp() {
        if self.isCheckoutPressed {
            completion?(PKPaymentAuthorizationStatus.failure)
            self.isCheckoutPressed = false
            //AlertUtil.alert({ self.isCheckoutPressed = false }, mes: "Нам не удалось совершить оплату через ApplePay. Вы можете выбрать другой тип оплаты")
        }else{
            completion?(PKPaymentAuthorizationStatus.success)
            let spiner = self.showModalSpinner()
            let order = checkOutButtonTapped(paymentTypeName: .byCard)
            _ = DataManager.shared.addOrderFinale(order)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { result in
                    self.hideModalSpinner(indicator: spiner)
                    if result{
                        NavigationHelper.shared.showNext(ThankYouViewController())
                    }else{
                        Crashlytics.sharedInstance().recordError(IikoError.init("код оплаты \(SmartHelper.orderNumber ?? 0)"), withAdditionalUserInfo: order.toJSON())
                         AlertUtil.alert({}, mes: "К сожалению, нам не удалось создать заказ. Ваш код оплаты \(SmartHelper.orderNumber ?? 0). Позвоните нам и сообщите этот код для завершения создания заказа  /n8 (843) 233-44-33")
                    }
                })
        }
    }
}


extension PaymentViewController: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        self.completion = completion
        _ = DataManager.shared.applePay(token: payment.token.paymentData.base64EncodedString())
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { res in
                self.isCheckoutPressed = false
                self.comp()
            }, onError: { t in
                self.comp()
            })
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: {
        })
    }
}

enum PaymentTypes {
    case byCash
    case byCardInDelivery
    case byCard
}
