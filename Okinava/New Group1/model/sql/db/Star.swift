//
//  Star.swift
//  Okinava
//
//  Created by Timerlan on 06.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import SQLite

struct Star: SQLCommand {
    var id: String
    var time: String
    var text: String
    
    init(
        id: String,
        time: String,
        text: String
        ) {
        self.id = id
        self.time = time
        self.text = text
    }
    
    static let id = Expression<String>("id")
    static let time = Expression<String>("time")
    static let text = Expression<String>("text")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Star.id <- id,
            Star.time <- time,
            Star.text <- text
        ))
    }
    
    func update(_ db: Connection, table: Table) {}
    
    func delete(_ db: Connection, table: Table) {
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Star.id == id)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Star(id: row[id], time: row[time], text: row[text]) as! T
    }
    
    func tableName() -> String{ return "Star"}
    static func tableName() -> String{ return "Star"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Star.tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id)
            t.column(time)
            t.column(text)
        })
        return table
    }
}


