//
//  Favourites.swift
//  Okinava
//
//  Created by Timerlan on 28.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import SQLite

struct Favourites: SQLCommand {
    var productId: String
    init( productId: String ) {
        self.productId = productId
    }
    
    static let productId = Expression<String>("productId")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            Favourites.productId <- productId
        ))
    }
    
    func update(_ db: Connection, table: Table) {}
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(Favourites.productId == productId ).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(Favourites.productId == productId )) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Favourites(productId: row[productId]) as! T
    }
    
    func tableName() -> String{ return "Favourites"}
    static func tableName() -> String{ return "Favourites"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(Favourites.tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(productId, primaryKey: true)
        })
        return table
    }
}
