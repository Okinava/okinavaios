//
//  DBHelper.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import SQLite
import RxSwift

class DBHelper {
    var main: DB = DB()
    static var sales: [Products]?

    func add(_ obj: SQLCommand) {
        let inDB = obj.isWasInDb(self.main.db, table: main.getTable(obj.tableName()))
        if !inDB { obj.insert(self.main.db, table: main.getTable(obj.tableName())) }else{ obj.update(self.main.db , table: main.getTable(obj.tableName())) }
    }
    
    func clearNomenclature() {
        try! main.db.run(main.groups.delete())
        try! main.db.run(main.products.delete())
        try! main.db.run(main.modifiers.delete())
        if let _ = DBHelper.sales {
            add(Products(id: "-1", name: "Акция", code: "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e"))
            DBHelper.sales!.forEach { (p) in add(p ) }
        }
    }
    
    func isHaveNomenclature() -> Bool { return try! main.db.scalar(main.products.count) != 0 }
    
    func getGroups() -> [Groups] {
        var result: [Groups] = []
        result.append(Groups(id: "0000", name: "Избранное", order: -2))
        result.append(Groups(id: "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e", name: "Акция", order: -1))
        for row in try! main.db.prepare(main.groups.filter(Groups.isIncludedInMenu && Groups.parentGroup == "").order(Groups.order)){
            result.append(Groups.fromRow(row))
        }
        return result
    }
    
    func getSubGroups(parentGroup: String) -> [Groups] {
        var result: [Groups] = []
        for row in try! main.db.prepare(main.groups.filter(Groups.isIncludedInMenu && Groups.parentGroup == parentGroup).order(Groups.order)){
            result.append(Groups.fromRow(row))
        }
        return result
    }
    
    func getProducts(parentGroup: String) -> [Products] {
        var result: [Products] = []
        for row in try! main.db.prepare(main.products.filter(Products.isIncludedInMenu && Products.parentGroup == parentGroup).order(Groups.order)){
            result.append(Products.fromRow(row))
        }
        return result
    }
    
    func getFavouritesProducts() -> [Products] {
        var result: [Products] = []
        for row in try! main.db.prepare(main.favourites){
            if let r = try! main.db.pluck(main.products.filter(Products.id == row[Favourites.productId])){
                result.append(Products.fromRow(r))
            }
        }
        return result
    }
    
    func getProducts() -> [Products] {
        var result: [Products] = []
        var pp = getFavouritesProducts().filter{ $0.productCategoryId != "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e"}
        if pp.count > 0 {
            result.append(Products(id: "-1", name: "Избранное", code: "0000"))
            for i in 0...pp.count-1 { pp[i].parentGroup = "0000" }
            result.append(contentsOf: pp)
        }
        if let _ = DBHelper.sales {
            result.append(Products(id: "-1", name: "Акция", code: "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e"))
            DBHelper.sales!.forEach { (p) in result.append(p) }
        }
        getGroups().forEach { (gr) in
            let sgr = getSubGroups(parentGroup: gr.id);
            if sgr.count == 0 {
                let p = getProducts(parentGroup: gr.id)
                if p.count > 0{
                    result.append(Products(id: "-1", name: gr.name, code: gr.id))
                    result.append(contentsOf: p)
                }
            }else{
                sgr.forEach{ (g) in
                    var p = getProducts(parentGroup: g.id)
                    if p.count > 0{
                        result.append(Products(id: "-1", name: gr.name + " " + g.name, code: gr.id))
                        for i in 0...p.count-1 { p[i].groupId = gr.id }
                        result.append(contentsOf: p)
                    }
                }
            }
        }
        
        return result
    }
    
    func addFavourite(product: Products) {
        add(Favourites(productId: product.id))
    }
    
    func removeFavourite(product: Products) {
        Favourites(productId: product.id).delete(main.db, table: main.favourites)
    }
    
    func isFavourite(product: Products) -> Bool {
        return Favourites(productId: product.id).isWasInDb(main.db, table: main.favourites)
    }
    
    func addOrder(product p: String, modificator m: String, count: Int64 = 1) -> Bool{
        for row in try! main.db.prepare(main.orders.filter(Orders.productId == p && Orders.modificatorId == m)){
            var o: Orders = Orders.fromRow(row)
            o.count = o.count + count
            o.update(main.db, table: main.orders)
            NotificationCenter.default.post(name: NotificationUtil.ORDERS_UPDATE, object: o)
            return true
        }
        if let _ = try! main.db.pluck(main.products.filter(Products.id == p)){
            let o = Orders(productId: p, modificatorId: m, count: count)
            add(o)
            NotificationCenter.default.post(name: NotificationUtil.ORDERS_ADD, object: o)
            return true
        }
        return false
    }
    
    func removeOrder(product p: Products, modificator mid: String = "", count: Int64? = nil) {
        for row in try! main.db.prepare(main.orders.filter(Orders.productId == p.id && Orders.modificatorId == mid)){
            var o: Orders = Orders.fromRow(row)
            if count == nil || o.count - (count ?? 0) == 0 {
                o.delete(main.db, table: main.orders)
                NotificationCenter.default.post(name: NotificationUtil.ORDERS_REMOVE, object: o)
                if try! main.db.scalar(main.orders.count) == 0 {
                    NotificationCenter.default.post(name: NotificationUtil.ORDERS_CLEAR, object: nil)
                }
                return
            }
            o.count = o.count - count!
            o.update(main.db, table: main.orders)
            NotificationCenter.default.post(name: NotificationUtil.ORDERS_UPDATE, object: o)
            return
        }
    }
    
    func clearOrder() {
        try! main.db.run(main.orders.delete())
        NotificationCenter.default.post(name: NotificationUtil.ORDERS_CLEAR, object: nil)
    }
    
    func inOrder(product p: Products, modificator_id mid: String = "") -> Int64{
        if let row = try! main.db.pluck(main.orders.filter(Orders.productId == p.id && Orders.modificatorId == mid)){
            let o: Orders = Orders.fromRow(row)
            return o.count
        }
        return 0
    }
    
    func OrderCount() -> Int { return try! main.db.scalar(main.orders.count) }
    
    func getModificators(product p: Products) -> [Products]{
        var result: [Products] = []
        for row in try! main.db.prepare(main.modifiers.filter(Modifiers.parentProduct == p.id)){
            if let r = try! main.db.pluck(main.products.filter(Products.id == row[Modifiers.id])){
                var product: Products = Products.fromRow(r)
                product.productCategoryId = row[Modifiers.modifiersId]
                result.append(product)
            }
        }
        return result
    }
    
    func getOrder() -> [(Products,Products?,Int64)]{
        var result: [(Products,Products?,Int64)] = []
        for row in try! main.db.prepare(main.orders){
            let r2 = try! main.db.pluck(main.products.filter(Products.id == row[Orders.modificatorId]))
            let m: Products? = r2 != nil ? Products.fromRow(r2!) : nil
            if let r = try! main.db.pluck(main.products.filter(Products.id == row[Orders.productId])){
                result.append((Products.fromRow(r),m,row[Orders.count]))
            }
        }
        return result
    }
    
    func addAdress(street: String, home: String, entrance: String, apartment: String, floor: String, doorphone: String, comment: String, phone: String) {
        add(Address(id: 0, phone: phone, street: street, home: home, entrance: entrance, apartment: apartment, floor: floor, doorphone: doorphone, comment: comment))
        NotificationCenter.default.post(name: NotificationUtil.ADDRESS, object: nil)
    }
    
    func updateAdress(street: String, home: String, entrance: String, apartment: String, floor: String, doorphone: String, comment: String, id: Int64) {
        Address(id: id, phone: "", street: street, home: home, entrance: entrance, apartment: apartment, floor: floor, doorphone: doorphone, comment: comment).update(main.db, table: main.address)
        NotificationCenter.default.post(name: NotificationUtil.ADDRESS, object: nil)
    }
    
    func removeAdress(id: Int64) {
        Address(id: id, phone: "", street: "", home: "", entrance: "", apartment: "", floor: "", doorphone: "", comment: "").delete(main.db, table: main.address)
        NotificationCenter.default.post(name: NotificationUtil.ADDRESS, object: nil)
    }
    
    func getAdress(phone: String) -> [Address] {
        var result: [Address] = []
        for row in try! main.db.prepare(main.address.filter(Address.phone == phone)){
            result.append(Address.fromRow(row))
        }
        return result
    }
    
    func isStar(id: String) -> Bool {
        return Star(id: id, time: "", text: "").isWasInDb(self.main.db, table: main.star)
    }
    
    func getStar(id: String) -> String {
        if let row = try! main.db.pluck(main.star.filter(Star.id == id)){
            let o: Star = Star.fromRow(row)
            return o.text
        }
        return ""
    }
}
