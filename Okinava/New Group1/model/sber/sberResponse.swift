//
//  sberResponse.swift
//  Okinava
//
//  Created by Timerlan on 08.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import Gloss

//MARK: - smartPromo
public struct sberResponse: Glossy {
    var orderId: String!
    var formUrl: String!
    var errorCode: String!
    var errorMessage: String!
    
    //MARK: Decodable
    public init?(json: JSON){
        orderId = "orderId" <~~ json
        formUrl = "formUrl" <~~ json
        errorCode = "errorCode" <~~ json
        errorMessage = "errorMessage" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "orderId" ~~> orderId,
            "formUrl" ~~> formUrl,
            "errorCode" ~~> errorCode,
            "errorMessage" ~~> errorMessage,
            ])
    }
    
}


