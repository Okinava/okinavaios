//
//  iikoHistory.swift
//  Okinava
//
//  Created by Timerlan on 04.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Foundation
import Gloss

//MARK: - iikoHistory
public struct IikoHistory: Glossy {
    
    public var customersDeliveryHistory : [IikoCustomersDeliveryHistory]!
    
    //MARK: Decodable
    public init?(json: JSON){
        customersDeliveryHistory = "customersDeliveryHistory" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "customersDeliveryHistory" ~~> customersDeliveryHistory,
            ])
    }
    
}


//MARK: - CustomersDeliveryHistory
public struct IikoCustomersDeliveryHistory: Glossy {
    
    public var customer : iikoCustomer!
    public var deliveryHistory : [IikoDeliveryHistory]!
    
    //MARK: Decodable
    public init?(json: JSON){
        customer = "customer" <~~ json
        deliveryHistory = "deliveryHistory" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "customer" ~~> customer,
            "deliveryHistory" ~~> deliveryHistory,
            ])
    }
    
}


//MARK: - DeliveryHistory
public struct IikoDeliveryHistory: Glossy {
    
    public var address : iikoAddress!
    public var comment : String!
    public var conception : String!
    public var customerId : String!
    public var date : String!
    public var discount : Double!
    public var isSelfService : Bool!
    public var items : [IikoItem]!
    public var number : String!
    public var opinion : AnyObject!
    public var orderId : String!
    public var organizationId : String!
    public var phone : String!
    public var status : String!
    public var sum : Double!
    
    //MARK: Decodable
    public init?(json: JSON){
        address = "address" <~~ json
        comment = "comment" <~~ json
        conception = "conception" <~~ json
        customerId = "customerId" <~~ json
        date = "date" <~~ json
        discount = "discount" <~~ json
        isSelfService = "isSelfService" <~~ json
        items = "items" <~~ json
        number = "number" <~~ json
        opinion = "opinion" <~~ json
        orderId = "orderId" <~~ json
        organizationId = "organizationId" <~~ json
        phone = "phone" <~~ json
        status = "status" <~~ json
        sum = "sum" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "address" ~~> address,
            "comment" ~~> comment,
            "conception" ~~> conception,
            "customerId" ~~> customerId,
            "date" ~~> date,
            "discount" ~~> discount,
            "isSelfService" ~~> isSelfService,
            "items" ~~> items,
            "number" ~~> number,
            "opinion" ~~> opinion,
            "orderId" ~~> orderId,
            "organizationId" ~~> organizationId,
            "phone" ~~> phone,
            "status" ~~> status,
            "sum" ~~> sum,
            ])
    }
    
    func getStatusInRussian() -> String{
        switch (status) {
        case "CANCELLED":
            return "Отменен"
        case "NEW":
            return "Новый"
        case "WAITING":
            return "В ожидании"
        case "ON_WAY":
            return "В пути"
        case "CLOSED":
            return "Закрыт"
        case "DELIVERED":
            return "Доставлен"
        case "UNCONFIRMED":
            return "Не подтвержден"
        default :
            return ""
            
        }
    }
    
}

//MARK: - Item
public struct IikoItem: Glossy {
    public var amount : Int64!
    public var code : String!
    public var comment : AnyObject!
    public var guestId : String!
    public var id : String!
    public var modifiers : [IikoModifier]!
    public var name : String!
    public var sum : Double!
    
    init(id: String, name: String, count: Int64, code: String) {
        self.id = id
        self.name = name
        self.amount = count
        self.code = code
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        amount = "amount" <~~ json
        code = "code" <~~ json
        comment = "comment" <~~ json
        guestId = "guestId" <~~ json
        id = "id" <~~ json
        modifiers = "modifiers" <~~ json
        name = "name" <~~ json
        sum = "sum" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "amount" ~~> amount,
            "code" ~~> code,
            "comment" ~~> comment,
            "guestId" ~~> guestId,
            "id" ~~> id,
            "modifiers" ~~> modifiers,
            "name" ~~> name,
            "sum" ~~> sum,
            ])
    }
    
}
