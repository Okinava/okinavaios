//
//  SberPayView.swift
//  Okinava
//
//  Created by Timerlan on 08.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import WebKit

class SberPayView: BaseView {
    private lazy var topNavigationView = OK_TopNavigationBlock()
    lazy var webView: UIWebView = {
        let view = UIWebView()
        return view
    }()
    
    override func setContent() {
        super.setContent()
        contentView.addSubview(topNavigationView)
        contentView.addSubview(webView)
        self.topNavigationView.prepareView()
        self.topNavigationView.navigationTitle = "Оплата заказа"
        contentView.backgroundColor = UIColor().hexStringToUIColor(0xf4f4f4)
        topNavigationView.backgroundColor = UIColor().hexStringToUIColor(0xf4f4f4)
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.topMargin)
        }
        webView.snp.remakeConstraints { make in
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(10)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
    @objc func goBackButtonTapped(){
        NavigationHelper.shared.back()
    }
}
