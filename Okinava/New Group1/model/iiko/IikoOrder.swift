//
//  IikoOrder.swift
//  Okinava
//
//  Created by Timerlan on 08.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import Gloss

//MARK: - IikoOrders
public struct IikoOrders: Glossy {
    
    var organization: String!
    var customer: iikoCustomer!
    var order: IikoOrder!
    var deliveryTerminalId: String!
    var coupon: String = DataManager.shared.customerHelper.promo
    
    init(organization: String, customer: iikoCustomer) {
        self.organization = organization
        self.customer = customer
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        organization = "organization" <~~ json
        customer = "customer" <~~ json
        order = "order" <~~ json
        deliveryTerminalId = "deliveryTerminalId" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "organization" ~~> organization,
            "customer" ~~> customer,
            "order" ~~> order,
            "deliveryTerminalId" ~~> deliveryTerminalId,
            "coupon" ~~> coupon
            ])
    }
}

//MARK: - IikoOrder
public struct IikoOrder: Glossy {
    
    var date: String!
    var phone: String!
    var isSelfService: String!
    var items: [IikoItem]!
    var address : iikoAddress!
    var paymentItems: [IikoPaymentItem]!
    var comment: String!
    var orderTypeId: String!
    var personsCount: Int = OrderingViewController.counter
    
    init(phone: String, isSelfService: Bool, comment: String, orderTypeId: String, date: String?, items: [IikoItem]) {
        self.phone = phone
        self.isSelfService = String(isSelfService)
        self.comment = comment
        self.orderTypeId = orderTypeId
        self.date = date
        self.items = items
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        date = "date" <~~ json
        phone = "phone" <~~ json
        isSelfService = "isSelfService" <~~ json
        items = "items" <~~ json
        address = "address" <~~ json
        paymentItems = "paymentItems" <~~ json
        comment = "comment" <~~ json
        orderTypeId = "orderTypeId" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "date" ~~> date,
            "phone" ~~> phone,
            "isSelfService" ~~> isSelfService,
            "items" ~~> items,
            "address" ~~> address,
            "paymentItems" ~~> paymentItems,
            "comment" ~~> comment,
            "orderTypeId" ~~> orderTypeId,
            "personsCount" ~~> personsCount,
            ])
    }
}


//MARK: - IikoPaymentItem
public struct IikoPaymentItem: Glossy {
    
    var sum: String!
    var paymentType: IikoPaymentType!
    var isProcessedExternally: Bool!
    var additionalData: String!
    
    init(sum: String, isProcessedExternally: Bool) {
        self.sum = sum
        self.isProcessedExternally = isProcessedExternally
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        sum = "sum" <~~ json
        paymentType = "paymentType" <~~ json
        isProcessedExternally = "isProcessedExternally" <~~ json
        additionalData = "additionalData" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "sum" ~~> sum,
            "paymentType" ~~> paymentType,
            "isProcessedExternally" ~~> isProcessedExternally,
            "additionalData" ~~> additionalData,
            ])
    }
}


//MARK: - IikoPaymentType
public struct IikoPaymentType: Glossy {
    
    var id: String!
    var code: String!
    var name: String!
    
    init() {}
    
    //MARK: Decodable
    public init?(json: JSON){
        id = "id" <~~ json
        code = "code" <~~ json
        name = "name" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "code" ~~> code,
            "name" ~~> name,
            ])
    }
}
