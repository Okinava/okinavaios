//
//  smartCheckSales.swift
//  Okinava
//
//  Created by Timerlan on 08.11.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import Foundation
import Gloss

//MARK: - smartCheckSales
public struct smartCheckSales: Glossy {
    
    public var availablePayments : [JSON]?
    public var loyatyResult : smartLoyatyResult?
    public var validationWarnings : [JSON]?

    
    //MARK: Decodable
    public init?(json: JSON){
        availablePayments = "availablePayments" <~~ json
        loyatyResult = "loyatyResult" <~~ json
        validationWarnings = "validationWarnings" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "availablePayments" ~~> availablePayments,
            "loyatyResult" ~~> loyatyResult,
            "validationWarnings" ~~> validationWarnings,
            ])
    }
    
}

//MARK: - smartLoyatyResult
public struct smartLoyatyResult: Glossy {
    
    public var programResults : [JSON]?
    
    //MARK: Decodable
    public init?(json: JSON){
        programResults = "programResults" <~~ json
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "programResults" ~~> programResults,
            ])
    }
    
}
