//
//  ProductViewController.swift
//  Okinava
//
//  Created by _ on 06.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
import RxSwift

class ProductViewController: UIViewController {
    var product: Products!
    var productTitleLabel = UILabel()
    let productPriceLabel = UILabel()
    let productTotalPriceLabel = UILabel()
    let productDescriptionLabel = UILabel()
    let productImage = UIImageView(image: UIImage())
    let productCountLabel = UILabel()
    let productCountMore = UIButton()
    let productCountLess = UIButton()
    let productWeightLabel = UILabel()
    let addToCartButton  = UIButton()
    var modificatorSegment = UISegmentedControl()
    var currentProductCount = 1
    let headerTypeLabel = UILabel()
    var currentModifier: String = ""
    var modiferProducts: [Products] = []
    var currentModifierPrice: Double = 0.0
    var nutValueStackView = UIStackView()
    let carbohydrateView = OK_NutValueView()
    let fatView = OK_NutValueView()
    let energyView = OK_NutValueView()
    let fiberView = OK_NutValueView()
    let modifiersStackView = UIStackView()
    let segmentScrollView = UIScrollView()
    var favouriteButton = UIButton()
    let headerDesriptionLabel = UILabel()
    let basketButton = OK_BasketButton()
    let mainScrollView = UIScrollView()
    
    
    override func viewDidLoad() {        
        super.viewDidLoad()
        self.prepareView()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func prepareView(){
        self.view.backgroundColor = .white
        self.title = product.name
        
        
        let contentView = UIView()
        let bottomView = UIView()
        let shortInfoView = UIView()
        let favView = UIView()
        
        let headerNutValueLabel = UILabel()
        
        
        self.nutValueStackView.axis = .vertical
        self.nutValueStackView.distribution = .fill
        self.nutValueStackView.alignment = .fill
        
        self.nutValueStackView.isOpaque = false
        
        favView.clipsToBounds = true
        
        self.mainScrollView.alwaysBounceVertical = true
        
        //        shortInfoView.addSubview(self.productTitleLabel)
        //        shortInfoView.addSubview(self.productPriceLabel)
        //        shortInfoView.addSubview(self.productWeightLabel)
        bottomView.backgroundColor = .white
        bottomView.addSubview(self.productCountMore)
        bottomView.addSubview(self.productCountLabel)
        bottomView.addSubview(self.productCountLess)
        bottomView.addSubview(self.productTotalPriceLabel)
        bottomView.addSubview(self.addToCartButton)
        
        bottomView.layer.shadowOffset = CGSize(width: 0, height: -4)
        bottomView.layer.shadowRadius = 7;
        bottomView.layer.shadowOpacity = 0.04;
        
        let backButton = OK_BackButton()
        backButton.isHidden = true
        basketButton.isHidden = true
        
        self.basketButton.isWithShadow = true
        
        self.view.addSubview(mainScrollView)
        
        self.view.addSubview(bottomView)
        mainScrollView.addSubview(contentView)
        shortInfoView.backgroundColor = .white
        shortInfoView.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        shortInfoView.layer.shadowOpacity = 0.08
        shortInfoView.layer.shadowOffset = CGSize(width: 0, height: 8   )
        shortInfoView.layer.shadowRadius = 16
        shortInfoView.layer.cornerRadius = 8
        
        //                self.modifiersStackView.distribution = .equalSpacing
        self.modifiersStackView.axis = .vertical
        //        self.modifiersStackView.alignment = .top
        
        
        headerTypeLabel.textColor = DEFAULT_COLOR_BLACK
        headerTypeLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        headerTypeLabel.text = "Добавить"
        
        headerDesriptionLabel.textColor = DEFAULT_COLOR_BLACK
        headerDesriptionLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        headerDesriptionLabel.text = "Описание"
        
        headerNutValueLabel.textColor = DEFAULT_COLOR_BLACK
        headerNutValueLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        headerNutValueLabel.text = "Пищевая ценность на 100г"
        
        self.productDescriptionLabel.font = FONT_ROBOTO_REGULAR?.withSize(14   )
        self.productDescriptionLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.productDescriptionLabel.numberOfLines = 0
        //        self.productDescriptionLabel.
        
        self.modificatorSegment.tintColor = DEFAULT_COLOR_RED
        self.modificatorSegment.selectedSegmentIndex = 0
        
        
        self.addToCartButton.setTitle("В корзину", for: .normal)
        self.addToCartButton.setTitleColor(DEFAULT_COLOR_RED, for: .normal )
        self.addToCartButton.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        self.addToCartButton.layer.borderWidth = 1
        self.addToCartButton.layer.cornerRadius = 4
        self.addToCartButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
        
        self.productCountLabel.textAlignment = .center
        self.productCountLabel.font = FONT_ROBOTO_MEDIUM?.withSize(21)
        self.productCountLabel.text = "1"
        
        self.productCountMore.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
        self.productCountLess.setImage(#imageLiteral(resourceName: "ic_minus"), for: .normal   )
        
        self.productCountMore.setTitleColor(DEFAULT_COLOR_RED, for: .highlighted)
        
        self.productCountMore.layer.cornerRadius = 4
        self.productCountMore.layer.borderWidth = 1
        self.productCountMore.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        
        self.productCountLess.layer.cornerRadius = 4
        self.productCountLess.layer.borderWidth = 1
        self.productCountLess.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        
        
        self.productTitleLabel.textColor = DEFAULT_COLOR_BLACK
        self.productTitleLabel.font = FONT_ROBOTO_BOLD?.withSize(18)
        
        self.productWeightLabel.textColor = DEFAULT_COLOR_GRAY
        self.productWeightLabel.font = FONT_ROBOTO_REGULAR?.withSize(16)
        
        self.productPriceLabel.textColor = DEFAULT_COLOR_BLACK
        self.productPriceLabel.font = FONT_ROBOTO_BOLD?.withSize(21)
        self.productPriceLabel.textAlignment = .right
        
        self.productTotalPriceLabel.textColor = UIColor().hexStringToUIColor(0x222222)
        self.productTotalPriceLabel.font = FONT_ROBOTO_MEDIUM?.withSize(21)
        
        
        backButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.productCountLess.addTarget(self, action: #selector(productLessTapped), for: .touchUpInside)
        self.productCountMore.addTarget(self, action: #selector(productMoreTapped), for: .touchUpInside)
        self.addToCartButton.addTarget(self, action: #selector(addToCartButtonTapped), for: .touchUpInside)
        
        
        bottomView.snp.makeConstraints { (make) in
            make.height.equalTo(64)
            make.width.equalToSuperview()
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.centerX.equalToSuperview()
        }
        self.productCountMore.snp.makeConstraints { (make) in
            make.width.height.equalTo(32)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            
        }
        self.productCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(self.productCountMore.snp.right).offset(5)
            make.width.greaterThanOrEqualTo(30)
            
        }
        self.productCountLess.snp.makeConstraints { (make) in
            make.width.height.equalTo(32)
            make.left.equalTo(self.productCountLabel.snp.right).offset(5)
            make.centerY.equalToSuperview()
        }
        self.addToCartButton.snp.makeConstraints { (make) in
            make.width.equalTo(96)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-16)
        }
        
        self.productTotalPriceLabel.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        //        self.segmentScrollView.showsHorizontalScrollIndicator = false
        //        self.segmentScrollView.showsVerticalScrollIndicator = false
        self.productImage.contentMode = .scaleAspectFill
        
        contentView.addSubview(self.productImage)
        contentView.addSubview(backButton)
        contentView.addSubview(basketButton)
        contentView.addSubview(shortInfoView)
        contentView.addSubview(headerDesriptionLabel)
        contentView.addSubview(self.productDescriptionLabel)
        contentView.addSubview(headerNutValueLabel)
        contentView.addSubview(self.nutValueStackView)
        contentView.addSubview(headerTypeLabel)
        
        //        contentView.addSubview(self.segmentScrollView)
        contentView.addSubview(self.modifiersStackView)
        //        self.segmentScrollView.addSubview(self.modificatorSegment)
        
        self.nutValueStackView.addArrangedSubview(self.carbohydrateView)
        self.nutValueStackView.addArrangedSubview(self.fatView)
        self.nutValueStackView.addArrangedSubview(self.fiberView)
        self.nutValueStackView.addArrangedSubview(self.energyView)
        
        
        shortInfoView.addSubview(favView)
        shortInfoView.addSubview(self.productTitleLabel)
        shortInfoView.addSubview(self.productWeightLabel)
        shortInfoView.addSubview(self.productPriceLabel)
        
        contentView.addSubview(self.productDescriptionLabel)
        
        
        mainScrollView.snp.makeConstraints { (make) in
            make.bottom.equalTo(bottomView.snp.top)
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        
        mainScrollView.contentInset = UIEdgeInsets(top: -UIApplication.shared.statusBarFrame.height, left: 0, bottom: 0, right: 0)
        
        contentView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
            make.left.right.equalTo(view)
        }
        
        self.productImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
        
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(contentView.snp.width).multipliedBy(0.7)
        }
        
        backButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(32)
            //            if #available(iOS 11, *) {
            //                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            //            } else {
            //                make.top.equalToSuperview().offset(16)
            //            }
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16 + UIApplication.shared.statusBarFrame.height)
        }
        
        basketButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(32)
            make.top.equalToSuperview().offset(16 + UIApplication.shared.statusBarFrame.height)
            make.right.equalToSuperview().offset(-16)
        }
        
        favView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        favView.layer.cornerRadius = 8
        favView.clipsToBounds = true
        
        favView.addSubview( favView.addBorders(edges: [.right], color: DEFAULT_COLOR_GRAY.withAlphaComponent(0.4), thickness: 1)[0])
        
        shortInfoView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.9)
            make.top.equalTo(self.productImage.snp.bottom).offset(-42)
            //            make.height.equalTo(70)
            make.centerX.equalToSuperview()
        }
        
        
        
        
        headerDesriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(shortInfoView.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
            
        }
        self.productDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(headerDesriptionLabel.snp.bottom).offset(10)
            make.width.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE*2)
            make.centerX.equalToSuperview()
        }
        
        headerNutValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.productDescriptionLabel.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        }
        self.nutValueStackView.snp.makeConstraints { (make) in
            make.top.equalTo(headerNutValueLabel.snp.bottom).offset(10)
            make.width.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE*2)
            make.centerX.equalToSuperview()
            
        }
        headerTypeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.nutValueStackView.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        }
        
        //        self.segmentScrollView.snp.makeConstraints { (make) in
        //            make.top.equalTo(headerTypeLabel.snp.bottom).offset(10)
        //            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
        //            make.right.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE)
        //
        //            make.height.equalTo(32)
        //        }
        self.modifiersStackView.snp.makeConstraints { (make) in
            //            make.top.equalTo(self.segmentScrollView.snp.bottom)
            make.top.equalTo(headerTypeLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            //            make.height.equalTo(100)
            make.bottom.equalToSuperview().offset(-20)
            
        }
        
        
        //        self.modificatorSegment.snp.makeConstraints { (make) in
        //            make.top.left.right.bottom.equalToSuperview()
        //        }
        
        
        self.productTitleLabel.adjustsFontSizeToFitWidth = true
        self.productTitleLabel.minimumScaleFactor = 0.5
        
        favView.addSubview(self.favouriteButton)
        
        favView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.height.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalTo(28)
        }
        self.favouriteButton.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.height.equalTo(20)
        }
        self.productTitleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(9)
            make.left.equalTo(favView.snp.right).offset(10)
            make.right.equalTo(self.productPriceLabel.snp.left).offset(5)
            
        }
        self.productWeightLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.productTitleLabel.snp.bottom).offset(3)
            make.left.equalTo(self.productTitleLabel.snp.left)
            make.bottom.equalToSuperview().offset(-9)
        }
        
        self.productPriceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(shortInfoView.snp.right).offset(-16)
            make.top.equalTo(self.productTitleLabel.snp.top)
            make.width.equalTo(80)
        }
        self.fiberView.snp.makeConstraints { (make) in
            make.height.equalTo(22)
            
        }
        self.fatView.snp.makeConstraints { (make) in
            make.height.equalTo(22)
        }
        self.carbohydrateView.snp.makeConstraints { (make) in
            make.height.equalTo(22)
        }
        self.energyView.snp.makeConstraints { (make) in
            make.height.equalTo(22)
        }
        
        //        self.modificatorSegment.addTarget(self, action: #selector(modifierChanged), for: .valueChanged)
        self.fiberView.keyLabel.text = "Белки"
        self.fatView.keyLabel.text = "Жиры"
        self.carbohydrateView.keyLabel.text = "Углеводы"
        self.energyView.keyLabel.text = "кКал"
        
        self.carbohydrateView.backgroundColor = .white
        self.fatView.backgroundColor = .white
        self.fiberView.backgroundColor = .white
        self.energyView.backgroundColor = .white
        
        self.favouriteButton.addTarget(self, action: #selector(self.addToFavButtonPressed), for: .touchUpInside)
    }
    
    
    func setData(){
        productTitleLabel.text = product.name
        self.fiberView.valueLabel.text = String(self.product.fiberAmount)
        self.fatView.valueLabel.text = String(self.product.fatAmount) + "г"
        self.carbohydrateView.valueLabel.text = String(self.product.carbohydrateAmount) + "г"
        self.energyView.valueLabel.text = String(self.product.energyAmount) + "г"
        
        if product.weight >= 1 {
            productWeightLabel.text = String(format: "%.02f", product.weight) + " кг."
        }else{
            productWeightLabel.text = "\(Int(product.weight * 1000)) гр."
        }
        self.productPriceLabel.text = self.product.price.cleanValue + " " + RUBLE_LETTER .uppercased()
        self.productTotalPriceLabel.text = self.product.price.cleanValue + " " + RUBLE_LETTER .uppercased()
        
        productDescriptionLabel.text = product.product_description
        if ((self.product.image.count) > 0){
            productImage.sd_setImage(with: URL(string: self.product!.image ))
        } else {
            productImage.snp.makeConstraints { (make) in make.height.equalTo(0) }
        }
        
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        _ = DataManager.shared.getModificators(product: product)
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ms in
                self.modiferProducts = ms;
                if ms.count == 0 { self.headerTypeLabel.text = "" }
                ms.forEach({ (p) in
                    var name = p.name + " (+" + p.price.cleanValue + RUBLE_LETTER + ")"
                    if name.first == "-" { name.remove(at: name.startIndex) }
                    let modView = OK_ModifierView(title: name, product: p)
                    modView.haveSelectingView = true
                    self.modifiersStackView.addArrangedSubview(modView)
                    modView.snp.makeConstraints { (make) in make.height.equalTo(35) }
                    let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.modifierTapped))
                    modView.addGestureRecognizer(gesture)
                })
            })
            if (DataManager.shared.isFavourite(product: product)) {
                self.favouriteButton.setImage(#imageLiteral(resourceName: "icon_star_fill"), for: .normal)
            } else {
                self.favouriteButton.setImage(#imageLiteral(resourceName: "icon_star_bordered"), for: .normal)
            }
    }
    
    @objc func modifierTapped(_ sender: UITapGestureRecognizer){
        for subview in self.modifiersStackView.arrangedSubviews {
            (subview as? OK_ModifierView)?.isSelecting(visible: false)
        }
        
        (sender.view as? OK_ModifierView)?.isSelecting(visible: true)
        self.currentModifier  = (sender.view as? OK_ModifierView)!.product!.id
        self.currentModifierPrice = (sender.view as? OK_ModifierView)!.product!.price
        self.changeProductCount(increment: 0)
        
        
    }
    
    @available(*, deprecated)
    @objc func modifierChanged(sender: UISegmentedControl){
        self.currentModifier  = self.modiferProducts[sender.selectedSegmentIndex].id
        self.currentModifierPrice = self.modiferProducts[sender.selectedSegmentIndex].price
        self.changeProductCount(increment: 0)
    }
    
    
    @objc func addToCartButtonTapped(){
       
    }
    @objc func productLessTapped(){
        self.changeProductCount(increment: -1)
    }
    
    @objc func productMoreTapped(){
        self.changeProductCount(increment: 1)
    }
    
    @objc func goBackButtonTapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func changeProductCount(increment: Int){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        var i = increment
        if increment < 0 && self.currentProductCount == 1 {
            i = 0
            InteractionFeedbackManager.shared.makeNotification(level: .warning)
        }
        self.currentProductCount = self.currentProductCount + i
        
        self.productTotalPriceLabel.text = ((self.product.price + self.currentModifierPrice) * Double(self.currentProductCount)).cleanValue + " " + RUBLE_LETTER
        self.productCountLabel.text =  String(self.currentProductCount)
        
    }
    
    @objc func addToFavButtonPressed() {
//        if (!DataManager.shared.isProductIdInFav(id: self.product.id)) {
//            DataManager.shared.addProductToFav(product: product)
//            self.favouriteButton.setImage(#imageLiteral(resourceName: "icon_star_fill"), for: .normal)
//        } else {
//            DataManager.shared.removeProductFromFav(id: self.product.id)
//            self.favouriteButton.setImage(#imageLiteral(resourceName: "icon_star_bordered"), for: .normal)
//        }
        InteractionFeedbackManager.shared.makeFeedback()
    }

    
    override func viewDidAppear(_ animated: Bool) {
//        self.updateBasketButton()
//        mainScrollView.delegate = self
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProductViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let percent = (scrollView.contentOffset.y / scrollView.contentSize.height) * 100
        let maxHeight = self.view.frame.width * 0.7
        let newHeight =  max(0, min(maxHeight, (maxHeight * ( 1 - percent/100))))
        
        if newHeight != CGFloat.nan {
             self.productImage.frame = CGRect(x: 0, y: 0, width: self.view.frame.width,  height: newHeight)
//            self.productImage.snp.updateConstraints { (make) in
//                make.height.equalTo(self.view.snp.width).multipliedBy( newHeight)
//            }
        }
        
    }
}
