//
//  ProductsController.swift
//  Okinava
//
//  Created by Timerlan on 02.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import UIKit
import RxSwift

class ProductsController: SwipeRightToPopViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    private var products: [Products]
    private var product: Products
    private var basketButton = OK_BasketButton()
    private lazy var backButton = OK_BackButton()
    private lazy var mainSlider: UICollectionView = {
        var mainCollectionViewLayout =  UICollectionViewFlowLayout.init()
        mainCollectionViewLayout.minimumLineSpacing = 0
        mainCollectionViewLayout.minimumInteritemSpacing = 0
        mainCollectionViewLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: mainCollectionViewLayout)
        view.setCollectionViewLayout(mainCollectionViewLayout, animated: true)
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.backgroundColor = .white
        view.register(ProductCell.self, forCellWithReuseIdentifier: ProductCellID)
        view.delegate = self
        view.dataSource = self
        view.isPagingEnabled = true
        return view
    }()
    
    init(products: [Products], product: Products) {
        self.products = [product]
        self.product = product
        super.init(nibName: nil, bundle: nil)
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor  = .white
        view.addSubview(mainSlider)
        view.addSubview(basketButton)
        view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        updateViewConstraints()
//        if let p = products.enumerated().first(where: {$0.element.id == product.id}){
//            _ = Observable<Int>.timer(0.5, scheduler: ThreadUtil.shared.backScheduler)
//                .observeOn(ThreadUtil.shared.mainScheduler)
//                .subscribe(onNext: {t in
//                    self.mainSlider.scrollToItem(at: IndexPath(row: p.offset, section: 0), at: .right, animated: true)
//                })
//        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        mainSlider.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
            make.top.equalToSuperview().offset(-10)
        }
        backButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.left.equalToSuperview().offset(16)
        }
        basketButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20 + UIApplication.shared.statusBarFrame.height)
            make.width.height.equalTo(32)
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return products.count }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCellID, for: indexPath) as! ProductCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard case let cell as ProductCell = cell else { return }
        cell.setData(product: products[indexPath.row], index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: collectionView.frame.height)
    }
}
