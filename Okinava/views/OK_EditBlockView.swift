//
//  OK_EditBlockView.swift
//  Okinava
//
//  Created by _ on 09.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit
class OK_EditBlockView: UIView {
    
    var titleLabel = UILabel()
    var valueTextField = OK_TextField()
    
    let padding = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12);
    override func draw(_ rect: CGRect) {
       
        self.backgroundColor = .white
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.titleLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.titleLabel.textColor = DEFAULT_COLOR_GRAY
        
        self.valueTextField.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.valueTextField.textColor = .black
        
        self.valueTextField.layer.borderWidth = 1
        self.valueTextField.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.valueTextField)
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16+12)
            make.right.equalToSuperview().offset((16+12) * -1)
            make.top.equalToSuperview()
        }
        
        self.valueTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(2)
            make.height.equalTo(48)
            make.bottom.equalToSuperview()
        }
//        self.valueTextField.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, padding))
//        self.valueTextField.placeholderRect(forBounds: UIEdgeInsetsInsetRect(bounds, padding))
//        self.valueTextField.editingRect(forBounds: UIEdgeInsetsInsetRect(bounds, padding))
//        self.valueTextField.delegate = self.parentViewController as? AddAddressViewController
        
    }
    
    
    public func set(title: String) {
        self.titleLabel.text = title
    }
    public func set(value: String){
        self.valueTextField.text = value
    }
    public func getValue()-> String? {
        return self.valueTextField.text
    }
    
}
