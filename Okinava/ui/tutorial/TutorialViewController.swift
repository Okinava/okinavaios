//
//  TutorialViewController.swift
//  Okinava
//
//  Created by _ on 29.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
private let mainCollectionViewLayout =  UICollectionViewFlowLayout.init()
    private var mainSlider: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    private var nextButton = UIButton()
    private let closeButton = UIButton()
    private let pager = UIPageControl()
    
    override func loadView() {
        navigationController?.navigationBar.isHidden = true
        super.loadView()
    }
    
    
    private var slides = [
        ["title" : "Узнайте самое вкусное  на главной", "description" :"Бонусы и промокод всегда на виду, как и любимые блюда, которые можно добавить в избранное или корзину", "image": #imageLiteral(resourceName: "slide_3")],
        ["title" : "Ищите выгоду в акциях", "description" :"Смотрите актуальные акции в понятном виде и делитесь ими с друзьями", "image": #imageLiteral(resourceName: "slide_1")],
        ["title" : "Найдите нас в контактах", "description" :"Найдите ближайшее заведение от Вас, посмотрите фотографии и свяжитесь при необходимости", "image": #imageLiteral(resourceName: "slide_2")],
        ["title" : "Проверьте и оплатите,  если хотите", "description" :"Проверьте заказ перед готовкой и выберите нужный тип оплаты или спишите бонусы", "image": #imageLiteral(resourceName: "slide_4")],
        ["title" : "Оформите быстрее чем такси",
         "description" :"Укажите дом на карте или выберите из предыдущих мест, а если нужно забрать самому – скажем сколько добираться", "image": #imageLiteral(resourceName: "slide_5")],
        ["title" : "Смотрите главное о себе в профиле", "description" :"Проверяйте статусы всех Ваших заказов и редактируйте информацию о себе", "image": #imageLiteral(resourceName: "slide_6")],
        ["title" : "Скажите, что думаете в чате", "description" :"Оставляйте обратную связь по сделанным заказам и смотрите историю общения", "image": #imageLiteral(resourceName: "slide_7")],
        
    ]
   /* private var slides_normal = [
        ["title" : "Узнайте самое вкусное  на главной", "description" :"Бонусы и промокод всегда на виду, как и любимые блюда, которые можно добавить в избранное или корзину", "image": #imageLiteral(resourceName: "screen_1_normal")],
        ["title" : "Ищите выгоду в акциях", "description" :"Смотрите актуальные акции в понятном виде и делитесь ими с друзьями", "image": #imageLiteral(resourceName: "screen_2_normal")],
        ["title" : "Найдите нас в контактах", "description" :"Найдите ближайшее заведение от Вас, посмотрите фотографии и свяжитесь при необходимости", "image": #imageLiteral(resourceName: "screen_3_normal")],
        ["title" : "Проверьте и оплатите,  если хотите", "description" :"Проверьте заказ перед готовкой и выберите нужный тип оплаты или спишите бонусы", "image": #imageLiteral(resourceName: "screen_4_normal")],
       
        
    ]*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       /* if (CONST_DEVICE_MODEL != "IPhone X") {
            self.slides = slides_normal
        }*/
        self.prepareView()
        // Do any additional setup after loading the view.
    }
    private func prepareView(){
        self.mainCollectionViewLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        self.mainSlider.setCollectionViewLayout(mainCollectionViewLayout, animated: true)
        
        self.mainSlider.showsVerticalScrollIndicator = false
        self.mainSlider.showsHorizontalScrollIndicator = false
        self.view.backgroundColor  = .white
        self.view.addSubview(self.closeButton)
        self.view.addSubview(self.mainSlider)
        self.view.addSubview(self.nextButton)
        self.view.addSubview(self.pager)
        
        self.mainSlider.snp.makeConstraints { (make) in
            make.top.equalTo(self.closeButton.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.nextButton.snp.top).offset(-10)
        }
        
        self.nextButton.backgroundColor = DEFAULT_COLOR_RED
        self.nextButton.setTitle("Далее", for: .normal)
        self.nextButton.setTitleColor(.white, for: .normal)
        self.nextButton.layer.cornerRadius = 4
        
        self.nextButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-16)
            make.width.equalToSuperview().multipliedBy(0.95)
            make.height.equalTo(48)
            make.centerX.equalToSuperview()
        }
        
        self.closeButton.setImage(#imageLiteral(resourceName: "icon_close"), for: .normal)
        self.closeButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(UIApplication.shared.statusBarFrame.height + 16)
            make.left.equalToSuperview().offset(20)
            make.height.width.equalTo(18)
        }
        
        self.pager.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.mainSlider).multipliedBy(1.23)
            make.width.equalToSuperview()
            make.height.equalTo(20)
            make.centerX.equalToSuperview()
        }
        self.pager.numberOfPages = self.slides.count
        self.mainSlider.backgroundColor = .white
        self.pager.pageIndicatorTintColor = DEFAULT_COLOR_GRAY
        self.pager.currentPageIndicatorTintColor = DEFAULT_COLOR_RED
        
        //Slider config
        self.mainSlider.register(TutorialCollectionViewCell.self, forCellWithReuseIdentifier: TutorialCollectionViewCellIdentifer)
        self.mainSlider.delegate = self
        self.mainSlider.dataSource = self
        self.mainSlider.isPagingEnabled = true
        self.mainCollectionViewLayout.minimumLineSpacing = 0
        self.mainCollectionViewLayout.minimumInteritemSpacing = 0
        
        self.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        self.closeButton.addTarget(self, action: #selector(self.closeTutorialButtonTapped), for: .touchUpInside)
        
    }
    
    @objc private func closeTutorialButtonTapped(){  NavigationHelper.shared.showMain() }
    
    @objc private func nextButtonTapped(){
        if (self.pager.currentPage < self.slides.count - 1) {
            self.pager.currentPage += 1
            self.mainSlider.scrollToItem(at: IndexPath(row: self.pager.currentPage, section: 0), at: .centeredHorizontally, animated: true)
            self.nextButton.setTitle("Далее", for: .normal)
            if (self.pager.currentPage == self.slides.count - 1){
                self.nextButton.setTitle("Начать заказывать", for: .normal)
            }
        } else {
            NavigationHelper.shared.showMain()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TutorialViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
 {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.slides.count
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
         let cell =  (self.mainSlider.visibleCells[0] as UICollectionViewCell)
       
        self.pager.currentPage =  self.mainSlider.indexPath(for: cell)!.row
        if (self.pager.currentPage == self.slides.count - 1) {
            self.nextButton.setTitle("Начать заказывать", for: .normal)
        } else {
            self.nextButton.setTitle("Далее", for: .normal)
        }
    }
//    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
//        self.pager.currentPage = indexPath.row
//        print(indexPath.row)
//    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorialCollectionViewCellIdentifer, for: indexPath) as! TutorialCollectionViewCell
        
        if (!cell.isPreparied) {
            cell.prepareView()
        }
        cell.setData(title: self.slides[indexPath.row]["title"] as! String, description: self.slides[indexPath.row]["description"] as! String, image: self.slides[indexPath.row]["image"] as! UIImage)
        
//        cell.frame.size =  CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//        self.pager.currentPage = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: collectionView.frame.height)
    }
    
}
