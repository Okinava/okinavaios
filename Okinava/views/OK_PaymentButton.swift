//
//  OK_PaymentButton.swift
//  Okinava
//
//  Created by _ on 05.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit

class OK_PaymentButton: ZFRippleButton {
    let rightArrow = UIImageView(image: #imageLiteral(resourceName: "payment_arrow"))
    let line = UIView()
    override func draw(_ rect: CGRect) {
        self.setTitleColor(.white, for: .highlighted)
        self.addSubview(rightArrow)
        self.rightArrow.contentMode = .scaleAspectFit
        self.rightArrow.frame = CGRect(x: self.frame.width - 40, y: self.frame.height / 2 - 5 , width: 10, height: 10)
        line.backgroundColor = UIColor().hexStringToUIColor(0xeeeeee)
        line.frame = CGRect(x: 0, y: self.frame.height - 1 , width: self.frame.width, height: 1)
        self.addSubview(line)
//        self.addTarget(self, action: #selector(self.buttonPressed), for: [.touchDown])
//        self.addTarget(self, action: #selector(self.buttonReleased), for: [.touchDragExit, .touchUpInside, .touchUpOutside, .touchCancel])
    }
//    @objc func buttonPressed(){
//        self.rightArrow.frame = CGRect(x: self.frame.width - 20, y: self.rightArrow.frame.minY, width: self.rightArrow.frame.width, height: self.rightArrow.frame.height)
//        self.backgroundColor =  DEFAULT_COLOR_RED
//    }
//    @objc func buttonReleased(){
//        self.rightArrow.frame = CGRect(x: self.frame.width - 40, y: self.rightArrow.frame.minY, width: self.rightArrow.frame.width, height: self.rightArrow.frame.height)
//        self.backgroundColor =  .white
//    }
    
    
}
