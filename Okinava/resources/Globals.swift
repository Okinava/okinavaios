//
//  Globals.swift
//  Okinava
//
//  Created by _ on 02.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit

let IS_DEBUG = false

let UD_DEVICE_ID = "device-id"
let UD_ACCESS_TOKEN: String = "apiAccesToken"
let UD_ORGANIZATION_ID: String = "organizationId"
let UD_CURRENT_CUSTOMER_ID : String = "currentCustomerId"
let UD_CURRENT_CUSTOMER_AVATAR : String = "currentCustomerAvatar"
let UD_LAST_ADDRESS_DOWNLOAD_DATE : String = "lastdownloaddate"
let UD_LAST_RESTARAUNTS_DOWNLOAD_DATE : String = "lastdownloaddaterestaraunt"
let UD_LAST_PROMO_DOWNLOAD_DATE : String = "lastdownloaddatepromo"
let UD_FCM_TOKEN: String = "ud_fcm_token"
let UD_IS_FIRST_LAUNCH = "is_first_lauch"

let HKLOG_MAX_STORE_LOGS = 10
let CONST_APP_VERSION = Bundle.main.releaseVersionNumber!
let CONST_APP_NAME = "Окинава"
let CONST_APP_BUILD  = Bundle.main.buildVersionNumber!
let CONST_DEVICE_OS = UIDevice.current.systemVersion
let CONST_DEVICE_MODEL = UIDevice.current.modelName



let DEFAULT_OFFSET_SIZE = 16


let FONT_ROBOTO_REGULAR = UIFont(name: "Roboto-Regular", size: 14)
let FONT_ROBOTO_MEDIUM = UIFont(name: "Roboto-Medium", size: 14)
let FONT_ROBOTO_LIGHT = UIFont(name: "Roboto-Light", size: 14)
let FONT_ROBOTO_BOLD = UIFont(name: "Roboto-Bold", size: 14)

let DEFAULT_COLOR_BLACK = UIColor().hexStringToUIColor(0x222222)
let DEFAULT_COLOR_GREEN = UIColor().hexStringToUIColor(0x82C001)
let DEFAULT_BACKGROUND = UIColor.white

let DEFAULT_COLOR_RED = UIColor().hexStringToUIColor(0xFF4748) //official
let DEFAULT_COLOR_YELLOW = UIColor().hexStringToUIColor(0xfaD347)

//let DEFAULT_COLOR_RED = UIColor().hexStringToUIColor(hex: "ff5c49") //ibm
let DEFAULT_COLOR_LIGHT_RED = UIColor().hexStringToUIColor(0xFED1D2)
let DEFAULT_COLOR_GRAY = UIColor().hexStringToUIColor(0xC8C8C8)
let DEFAULT_COLOR_DARK_GRAY = UIColor().hexStringToUIColor(0x888888)

let DEFAULT_BAR_BACKGROUND_COLOR = UIColor().hexStringToUIColor(0xece8e2)

let RUBLE_LETTER = "₽"


let MIN_DELIVERY_ORDER_SUM = 600
