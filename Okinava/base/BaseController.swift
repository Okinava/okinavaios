//
//  BaseController.swift
//  Okinava
//
//  Created by Timerlan on 25.09.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import RappleProgressHUD
import AudioToolbox

class BaseController<T: BaseView>: SwipeRightToPopViewController {
    var mainView: T! { return view as? T }
    
    override func loadView() {
        super.loadView()
        view = T()
    }
    
    func vibrate(){ AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) }
}



