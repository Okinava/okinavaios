//
//  smartRestorans.swift
//  Okinava
//
//  Created by Timerlan on 08.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//


import Foundation
import Gloss

//MARK: - smartRestorans
public struct smartRestorans: Glossy {
    
    public var address : String!
    public var coordinates : String!
    public var id : String!
    public var image : String!
    public var name : String!
    public var openingHours : [OpeningHour]!
    
    //MARK: Decodable
    public init?(json: JSON){
        address = "address" <~~ json
        coordinates = "coordinates" <~~ json
        id = "id" <~~ json
        image = "image" <~~ json
        name = "name" <~~ json
        openingHours = "openingHours" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "address" ~~> address,
            "coordinates" ~~> coordinates,
            "id" ~~> id,
            "image" ~~> image,
            "name" ~~> name,
            "openingHours" ~~> openingHours,
            ])
    }
    
}

//
//  OpeningHour.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on October 8, 2018

import Foundation
import Gloss

//MARK: - OpeningHour
public struct OpeningHour: Glossy {
    
    public var allDay : Bool!
    public var closed : Bool!
    public var dayOfWeek : Int!
    public var from : String!
    public var to : String!
    
    //MARK: Decodable
    public init?(json: JSON){
        allDay = "allDay" <~~ json
        closed = "closed" <~~ json
        dayOfWeek = "dayOfWeek" <~~ json
        from = "from" <~~ json
        to = "to" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "allDay" ~~> allDay,
            "closed" ~~> closed,
            "dayOfWeek" ~~> dayOfWeek,
            "from" ~~> from,
            "to" ~~> to,
            ])
    }
    
}

