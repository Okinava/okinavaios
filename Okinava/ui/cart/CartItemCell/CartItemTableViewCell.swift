//
//  CartItemTableViewCell.swift
//  Okinava
//
//  Created by _ on 23.04.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit

class CartItemTableViewCell: UITableViewCell {
    private var cart: (Products,Products?,Int64)!
    let productImageView = UIImageView()
    let productTitleLabel = UILabel()
    let productWeightLabel = UILabel()
    let countMoreButton = ZFRippleButton()
    let countLessButton = ZFRippleButton()
    let productCountLabel = UILabel()
    let productPriceLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = DEFAULT_BACKGROUND
        let contentView = UIView()
        let buttonsView = UIView()
        selectionStyle = .none
        contentView.addSubview(self.productImageView)
        contentView.addSubview(self.productTitleLabel)
        contentView.addSubview(self.productWeightLabel)
        contentView.addSubview(self.productPriceLabel)
        contentView.addSubview(self.productCountLabel)
        contentView.addSubview(buttonsView)
        
        self.addSubview(contentView)
        buttonsView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
            make.right.equalToSuperview().offset(-16)
            make.width.equalTo(32)
        }
        buttonsView.addSubview(self.countMoreButton)
        buttonsView.addSubview(self.countLessButton)
        
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
        contentView.layer.shadowOpacity = 0.04
        contentView.layer.shadowOffset = CGSize(width: 0, height: 8)
        contentView.layer.shadowRadius = 16
        
        self.countLessButton.setImage(#imageLiteral(resourceName: "ic_minus"), for: .normal)
        self.countMoreButton.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
        
        buttonsView.layer.borderWidth = 1
        buttonsView.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        buttonsView.layer.cornerRadius = 4
        
        self.productPriceLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.productCountLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.productTitleLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.productWeightLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        
        self.productPriceLabel.textColor = DEFAULT_COLOR_BLACK
        self.productCountLabel.textColor = DEFAULT_COLOR_BLACK
        self.productTitleLabel.textColor = DEFAULT_COLOR_BLACK
        self.productWeightLabel.textColor = DEFAULT_COLOR_GRAY
        
        self.productImageView.layer.cornerRadius = 4
        
        self.productImageView.contentMode = .scaleAspectFill
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        self.productImageView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.width.equalTo(self.productImageView.snp.height)
        }
        
        self.countMoreButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.5)
            make.top.equalToSuperview()
            make.left.equalToSuperview()
        }
        
        self.countLessButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.5)
            make.top.equalTo(self.countMoreButton.snp.bottom)
            make.left.equalToSuperview()
        }
        
        self.productTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.productImageView.snp.right).offset(16)
            make.top.equalToSuperview().offset(16)
            make.right.equalTo(buttonsView.snp.left).offset(-16)
        }
        self.productPriceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.productImageView.snp.right).offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
        self.productWeightLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.productTitleLabel.snp.bottom)
            make.left.equalTo(self.productImageView.snp.right).offset(16)
        }
        self.productCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(buttonsView.snp.left).offset(-10)
        }
        
        self.countLessButton.addTarget(self, action: #selector(countLessButtonTapped), for: .touchUpInside)
        self.countMoreButton.addTarget(self, action: #selector(countMoreButtonTapped), for: .touchUpInside)
        self.productImageView.clipsToBounds = true
        self.productTitleLabel.clipsToBounds = true
        self.productImageView.clipsToBounds = true
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public func setData(item: (Products,Products?,Int64))
    {
        self.cart = item
        productTitleLabel.text = item.0.name + (item.1 != nil ? " \(item.1!.name)" : "")
        if item.0.productCategoryId == "1e1e6a7c-5b91-2fc2-0146-1d7a94731d2e" {
            productTitleLabel.text = productTitleLabel.text! + " (Акция)"
        }
        if item.0.image.count > 0 { productImageView.sd_setImage(with: URL(string: item.0.image )) }
        if item.0.weight >= 1 {
            productWeightLabel.text = String(format: "%.02f", item.0.weight) + " кг."
        }else{
            productWeightLabel.text = "\(Int(item.0.weight * 1000)) гр."
        }
        productCountLabel.text = "x"+String(item.2)
        productPriceLabel.text = String( ((item.0.price + (item.1 != nil ? item.1!.price : 0)) * Double(item.2)).cleanValue) + "руб"
    }
    @objc private func countLessButtonTapped(){
        InteractionFeedbackManager.shared.makeFeedback()
        _ = DataManager.shared.removeOrder(product: cart.0, modificator: cart.1?.id ?? "", count: 1).subscribe{}
        setData(item: (cart.0,cart.1,cart.2 - 1))
    }
    @objc private func countMoreButtonTapped(){
        InteractionFeedbackManager.shared.makeFeedback()
        _ = DataManager.shared.addOrderAndCheck(product: cart.0, modificator: cart.1?.id ?? "")
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { res in
                if !res.0 { AlertUtil.alert({}, mes: res.1!) }
                else{ self.setData(item: (self.cart.0,self.cart.1,self.cart.2 + 1)) }
            })
    }
}
