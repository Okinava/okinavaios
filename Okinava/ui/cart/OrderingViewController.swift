//
//  OrderingViewController.swift
//  Okinava
//
//  Created by _ on 19.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import RxSwift


class OrderingViewController: SwipeRightToPopViewController {
    private lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.navigationTitle = "Оформление заказа"
        view.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        view.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        view.rightButton.removeFromSuperview()
        view.prepareView()
        return view
    }()
    private lazy var nextButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("Далее", for: .normal)
        view.setTitleColor(.white, for: .normal  )
        view.backgroundColor = DEFAULT_COLOR_RED
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(16)
        view.layer.cornerRadius = 6
        view.layer.cornerRadius = 6
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var isSelfServiceSegmentControl: UISegmentedControl = {
        let view = UISegmentedControl(items: ["Самовывоз", "Доставка"])
        view.tintColor = DEFAULT_COLOR_RED
        view.setTitleTextAttributes([NSAttributedString.Key.font: FONT_ROBOTO_REGULAR?.withSize(16) as Any], for: .normal)
        return view
    }()
    private lazy var customerNameLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    private lazy var customerPhoneLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    private lazy var orderCommentTextField: OK_TextField = {
        let view = OK_TextField()
        view.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.placeholder = "Введите ваш комментарий"
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        return view
    }()
    private lazy var addressesStackView = UIStackView()
    private lazy var orderTimeTextField: OK_TextField = {
        let view = OK_TextField()
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = DEFAULT_COLOR_GRAY.cgColor
        view.font = FONT_ROBOTO_MEDIUM!.withSize(14)
        view.text = "00:00"
        view.textColor = DEFAULT_COLOR_BLACK
        view.textAlignment = .center
        return view
    }()
    private lazy var labelForAddress  : UILabel = {
        let view = UILabel()
        view.text = "Адрес доставки"
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var labelForDetails  : UILabel = {
        let view = UILabel()
        view.text = "Детали заказа"
        view.font = FONT_ROBOTO_BOLD?.withSize(18)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var labelForTime     : UILabel = {
        let view = UILabel()
        view.text = "Время доставки"
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var labelForComments : UILabel = {
        let view = UILabel()
        view.text = "Комментарий"
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var labelForCounter : UILabel = {
        let view = UILabel()
        view.text = "Количество\nприборов"
        view.numberOfLines = 0
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.textColor = DEFAULT_COLOR_BLACK
        return view
    }()
    private lazy var productCountView: UIView = {
        let view = UIView()
        view.layer.borderColor = DEFAULT_COLOR_RED.cgColor
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.addSubview(self.countLessButton)
        view.addSubview(self.countLabel)
        view.addSubview(self.countMoreButton)
        return view
    }()
    private lazy var countMoreButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("+", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.addTarget(self, action: #selector(self.countMoreButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var countLessButton: ZFRippleButton = {
        let view = ZFRippleButton()
        view.setTitle("-", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        view.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        view.addTarget(self, action: #selector(self.countLessButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var countLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        view.text = "1x"
        view.textAlignment = .center
        return view
    }()
    private lazy var viewForDetails       : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowOpacity = 0.08
        view.layer.shadowRadius = 16
        view.layer.shadowOffset = CGSize(width: 0, height: 8)
        view.addSubview(imageForCustomerName)
        view.addSubview(customerNameLabel)
        view.addSubview(imageForCustomerPhone)
        view.addSubview(customerPhoneLabel)
        view.addSubview(dividerBeforeAddress)
        view.addSubview(labelForAddress)
        view.addSubview(addressesStackView)
        view.addSubview(dividerBeforeTime)
        view.addSubview(labelForTime)
        view.addSubview(orderTimeTextField)
        view.addSubview(dividerBeforeComment)
        view.addSubview(labelForComments)
        view.addSubview(orderCommentTextField)
        view.addSubview(dividerBeforeCounter)
        view.addSubview(labelForCounter)
        view.addSubview(productCountView)
        return view
    }()
    private lazy var dividerBeforeAddress : UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        return view
    }()
    private lazy var dividerBeforeTime    : UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        return view
    }()
    private lazy var dividerBeforeComment : UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        return view
    }()
    private lazy var dividerBeforeCounter : UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY.withAlphaComponent(0.6)
        return view
    }()
    private lazy var imageForCustomerName = UIImageView(image: #imageLiteral(resourceName: "icon_user"))
    private lazy var imageForCustomerPhone = UIImageView(image: #imageLiteral(resourceName: "icon_phone"))
 
    static var counter = 1
    @objc func countMoreButtonTapped(){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        if OrderingViewController.counter < 10 {
            OrderingViewController.counter = OrderingViewController.counter + 1
            self.countLabel.text =  String(OrderingViewController.counter) + "x"
        }
    }
    @objc func countLessButtonTapped(){
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        if OrderingViewController.counter > 1 {
            OrderingViewController.counter = OrderingViewController.counter - 1
            self.countLabel.text =  String(OrderingViewController.counter) + "x"
        }
    }
    
    var timePicker = UIDatePicker()
    private var cartItems: [(Products,Products?,Int64)] = []
    var totalSum: Double = 0.0
    let scrollView = UIScrollView()
    let contentView = UIView()
    var isError : Bool = false
    var activeField: UITextField?
    private var time: Int64
    private var bonus: Double
    
    init(time: Int64, bonus: Double) {
        self.time = time
        self.bonus = bonus
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = DEFAULT_BACKGROUND
        orderCommentTextField.delegate = self
        self.orderTimeTextField.delegate = self
        self.prepareView()
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(forName: NotificationUtil.ADDRESS, object: nil, queue: OperationQueue.main){ notification in self.reloadViewFromNib() }
        self.getOrder();
    }
    
    private func getOrder(){
        _ = DataManager.shared.getOrder()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ os in
                self.cartItems = os
                self.calcTotalSum()
                if self.totalSum >= Double(MIN_DELIVERY_ORDER_SUM) {
                    self.isSelfServiceSegmentControl.selectedSegmentIndex = 1
                } else {
                    self.isSelfServiceSegmentControl.selectedSegmentIndex = 0
                }
                self.setData()
            })
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.goBackButtonTapped()
        }
    }
    func prepareView(){
        scrollView.alwaysBounceVertical = true
        view.backgroundColor = DEFAULT_BACKGROUND
        contentView.backgroundColor = DEFAULT_BACKGROUND
        scrollView.backgroundColor = DEFAULT_BACKGROUND
        addressesStackView.axis = .vertical
        isSelfServiceSegmentControl.addTarget(self, action: #selector(self.isSelfValueChanged), for: .valueChanged)
        imageForCustomerPhone.contentMode = .scaleAspectFit
        imageForCustomerName.contentMode = .scaleAspectFit
        view.addSubview(self.topNavigationView)
        view.addSubview(self.isSelfServiceSegmentControl)
        view.addSubview(self.scrollView)
        scrollView.addSubview(self.contentView)
        contentView.addSubview(labelForDetails)
        contentView.addSubview(viewForDetails)
        scrollView.addSubview(nextButton)

        
        topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        isSelfServiceSegmentControl.snp.makeConstraints { (make) in
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(16)
            make.height.equalTo(40)
        }
        nextButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-30)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(view).offset(-16)
            make.height.equalTo(45)
        }
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(self.isSelfServiceSegmentControl.snp.bottom).offset(10)
            make.bottom.equalToSuperview().offset(0)
            make.left.right.width.equalToSuperview()
        }
        contentView.snp.makeConstraints { (make) in
            make.top.left.right.width.equalToSuperview()
            make.bottom.equalToSuperview().offset(-90)
        }
        labelForDetails.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.height.equalTo(30)
        }
        viewForDetails.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(labelForDetails.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.contentView.snp.bottom).offset(0)
            
        }
        imageForCustomerName.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(18)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24) 
        }
        self.customerNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForCustomerName)
            make.left.equalTo(imageForCustomerName.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        imageForCustomerPhone.snp.makeConstraints { (make) in
            make.top.equalTo(imageForCustomerName.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
            
        }
        
        self.customerPhoneLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageForCustomerPhone)
            make.left.equalTo(imageForCustomerPhone.snp.right).offset(10)
            make.right.equalToSuperview().offset(-20)
        }
        dividerBeforeAddress.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(imageForCustomerPhone.snp.bottom).offset(15)
        }
        
        labelForAddress.snp.makeConstraints { (make) in
            make.top.equalTo(dividerBeforeAddress.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        self.addressesStackView.snp.makeConstraints { (make) in
            make.top.equalTo(labelForAddress.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        dividerBeforeTime.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.addressesStackView.snp.bottom).offset(15)
        }
        
        labelForTime.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(dividerBeforeTime.snp.bottom).offset(25)
        }
        self.orderTimeTextField.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalTo(160)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(dividerBeforeTime.snp.bottom).offset(15)
        }
        
        dividerBeforeComment.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.orderTimeTextField.snp.bottom).offset(15)
        }
        
        labelForComments.snp.makeConstraints { (make) in
            make.top.equalTo(dividerBeforeComment.snp.bottom).offset(15)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        self.orderCommentTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(labelForComments.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
        
        dividerBeforeCounter.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.orderCommentTextField.snp.bottom).offset(15)
        }
        
        labelForCounter.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(dividerBeforeCounter.snp.bottom).offset(15)
        }
        productCountView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalTo(160)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(dividerBeforeCounter.snp.bottom).offset(15)
            make.bottom.equalToSuperview().offset(-16)
        }
        countMoreButton.snp.remakeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.3)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
        }
        countLessButton.snp.remakeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.3)
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        countLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(self.countMoreButton.snp.right)
            make.right.equalTo(self.countLessButton.snp.left)
            make.centerY.equalToSuperview()
        }
        
        self.contentView.layoutSubviews()
        
        
        timePicker = UIDatePicker(frame: CGRect(x: 0, y: (self.view.frame.height - 220), width: view.frame.width, height: 220))
        timePicker.backgroundColor = .white
        
        //        datePicker.delegate = self
        //        datePicker.dataSource = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = DEFAULT_COLOR_RED
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Выбрать", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.timeSelectDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, spaceButton,  doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.timePicker.backgroundColor = .white
        self.orderTimeTextField.inputView = timePicker
        self.orderTimeTextField.inputAccessoryView = toolBar
    }
    
    func setData(){
        self.addressesStackView.safelyRemoveArrangedSubviews()
        self.calcTotalSum()
        if self.isSelfServiceSegmentControl.selectedSegmentIndex == 1{
            self.setDataForDelivery()
        } else {
            self.setDataForSelfService()
        }
        self.customerNameLabel.text = DataManager.shared.customerHelper.name
        self.customerPhoneLabel.text = DataManager.shared.customerHelper.phone
    }
    
    private var terminals: [IikoDeliveryTerminal] = []
    private func setDataForSelfService (){
        let spiner = self.showModalSpinner()
        _ = DataManager.shared.getTerminals()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { terminals in
                self.hideModalSpinner(indicator: spiner)
                self.addressesStackView.safelyRemoveArrangedSubviews()
                self.labelForAddress.text = "Самовывоз"
                self.terminals = terminals.deliveryTerminals
                for terminal in terminals.deliveryTerminals {
                    let block = OK_ContactView(image: #imageLiteral(resourceName: "icon_home"), title: terminal.name)
                    block.accessibilityIdentifier = terminal.deliveryTerminalId
                    block.padding = 0
                    block.haveSelectingView = true
                    self.addressesStackView.addArrangedSubview(block)
                    block.snp.makeConstraints { (make) in make.height.equalTo(40) }
                    let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.selfServiceBlockTapped))
                    block.addGestureRecognizer(gesture)
                    self.selfServiceBlockTapped(gesture)
                }
                let date = Date(timeIntervalSince1970: TimeInterval(self.time / 10 / 1000))
                var startDate = date.addingTimeInterval(1500) //calendar.date(byAdding: .minute, value: 80, to: Date())
                while(true){
                    if self.checkDate(date: startDate){
                        startDate = startDate.addingTimeInterval(600)
                    }else{
                        self.timePicker.minimumDate = startDate
                        self.timePicker.date = startDate
                        self.orderTimeTextField.text = startDate.toUserFormatString(format: "HH:mm")
                        return
                    }
                }
            },onError: { t in
                self.hideModalSpinner(indicator: spiner)
                AlertUtil.alert({}, mes: t.localizedDescription)
            })
    }
    
    private var addresses: [Address] = []
    private func setDataForDelivery(){
        _ = DataManager.shared.getAddress()
            .observeOn(ThreadUtil.shared.mainScheduler)
            .subscribe(onNext:{ addreses in
                self.addressesStackView.safelyRemoveArrangedSubviews()
                self.labelForAddress.text = "Адрес доставки"
                self.addresses = addreses
                for address in addreses {
                    let block = OK_ContactView(image: #imageLiteral(resourceName: "icon_home"), title:  address.street + " " + address.home)
                    block.tag = Int(address.id)
                    block.padding = 0
                    block.haveSelectingView = true
                    self.addressesStackView.addArrangedSubview(block)
                    block.snp.makeConstraints { (make) in make.height.equalTo(40) }
                    let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.addressTapped))
                    block.addGestureRecognizer(gesture)
                    self.addressTapped(gesture)
                }
                let addButton = OK_ContactView(image: #imageLiteral(resourceName: "icon_plus_red"), title: "Добавить адрес")
                addButton.isSelecting(visible: false)
                addButton.padding = 0
                addButton.haveSelectingView = false
                self.addressesStackView.addArrangedSubview(addButton)
                addButton.snp.makeConstraints { (make) in  make.height.equalTo(40) }
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.addAddressButtonTapped))
                addButton.addGestureRecognizer(gesture)
                let date = Date(timeIntervalSince1970: TimeInterval(self.time / 10 / 1000))
                var startDate = date.addingTimeInterval(3600) //calendar.date(byAdding: .minute, value: 80, to: Date())
                while(true){
                    if self.checkDate(date: startDate){
                        startDate = startDate.addingTimeInterval(600)
                    }else{
                        self.timePicker.minimumDate = startDate
                        self.orderTimeTextField.text = startDate.toUserFormatString(format: "HH:mm")
                        return
                    }
                }
            })
    }
    @objc func addAddressButtonTapped(){
        self.navigationController?.pushViewController(AddAddressViewController(), animated: true)
    }
    
    private var terminal: IikoDeliveryTerminal?
    @objc func selfServiceBlockTapped (_ sender: UITapGestureRecognizer) {
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        for subview in self.addressesStackView.arrangedSubviews {
            (subview as? OK_ContactView)?.isSelecting(visible: false)
        }
        (sender.view as? OK_ContactView)?.isSelecting(visible: true)
        terminal = terminals.first(where: { $0.deliveryTerminalId == sender.view?.accessibilityIdentifier})
        address = nil
    }
    private var address: Address?
    @objc func addressTapped (_ sender: UITapGestureRecognizer) {
        InteractionFeedbackManager.shared.makeFeedback(level: .light)
        for subview in self.addressesStackView.arrangedSubviews {
            (subview as? OK_ContactView)?.isSelecting(visible: false)
        }
        (sender.view as? OK_ContactView)?.isSelecting(visible: true)
        address = addresses.first(where: { $0.id == Int64(sender.view?.tag ?? 0) })
        terminal = nil
    }
    @objc func nextButtonTapped(){
        if isSelfServiceSegmentControl.selectedSegmentIndex == 1 && address == nil {
            self.showAlert(title: "Еще чуть-чуть", message: "Выберите адрес доставки")
            return
        }
        if isSelfServiceSegmentControl.selectedSegmentIndex == 0 && terminal == nil {
            self.showAlert(title: "Еще чуть-чуть", message: "Выберите пункт самовывоза")
            return
        }
        navigationController?.pushViewController(PaymentViewController(time: timePicker.date, address: address, terminal: terminal, bonus: bonus,comment: orderCommentTextField.text ?? "", timeNow:  Date(timeIntervalSince1970: TimeInterval(self.time / 10 / 1000))), animated: true)
    }
    
    @objc func isSelfValueChanged(sender: UISegmentedControl){
        self.calcTotalSum()
        if self.isSelfServiceSegmentControl.selectedSegmentIndex == 1 && self.totalSum < Double(MIN_DELIVERY_ORDER_SUM) {
            self.showAlert(title: "Доставка невозможна", message: "Минимальная сумма заказа - 600 рублей")
            self.isSelfServiceSegmentControl.selectedSegmentIndex = 0
        }
        self.setData()
    }
    
    private func calcTotalSum (){
        self.totalSum = 0
        for cartItem in self.cartItems {
            self.totalSum += Double(cartItem.2) * (cartItem.0.price + (cartItem.1?.price ?? 0))
        }
    }
    @objc func goBackButtonTapped(){ self.navigationController?.popViewController(animated: true) }
    
    @objc func timeSelectDone(){
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        if checkDate(date: timePicker.date){
            AlertUtil.alert({}, mes: "К сожелению мы не осуществляем доставку в это время, поэтому не можем принять Ваш заказ\n\nВремя доставки:\n" +
                "Вс-Чт: с 11:00 до 01:00\n" +
                "Пт-Сб: с 11:00 до 02:00" +
                "\n\nВремя самовывоза:\n" +
                "Вс-Чт: с 10:25 до 01:00\n" +
                "Пт-Сб: с 10:25 до 02:00")
            return
        }
        if newYear(date: timePicker.date){
            AlertUtil.alert({}, mes: "К сожелению мы не осуществляем доставку в это время, поэтому не можем принять Ваш заказ\n\nМы не работаем с 20:00 31 декабря по 10:00 2 января!")
            return
        }
        let calendar = Calendar(identifier: .gregorian)
        if calendar.component(Calendar.Component.day, from: Date(timeIntervalSince1970: TimeInterval(self.time / 10 / 1000))) != calendar.component(Calendar.Component.day, from: timePicker.date) {
            self.orderTimeTextField.text = self.timePicker.date.toUserFormatString(format: "HH:mm dd.MM.YYYY")}else{
            self.orderTimeTextField.text = self.timePicker.date.toUserFormatString(format: "HH:mm")
        }
        self.orderTimeTextField.endEditing(true)
    }
    private func checkDate(date: Date) -> Bool{
        let calendar = Calendar(identifier: .gregorian)
        let week = calendar.component(Calendar.Component.day, from: date)
        let hour = calendar.component(Calendar.Component.hour, from: date)
        let minute = calendar.component(Calendar.Component.minute, from: date)
        return ((week == 1 || week == 7) &&
            (hour * 60 + minute) > (2 * 60) &&
            (hour * 60 + minute) < (10 * 60 + (isSelfServiceSegmentControl.selectedSegmentIndex == 1 ? 60 : 25) )) ||
            ((week > 1 && week < 7 ) &&
                (hour * 60 + minute) > (1 * 60) &&
                (hour * 60 + minute) < (10 * 60 + (isSelfServiceSegmentControl.selectedSegmentIndex == 1 ? 60 : 25) ))
    }
    private func newYear(date: Date) -> Bool{
        let calendar = Calendar(identifier: .gregorian)
        let day = calendar.component(Calendar.Component.day, from: date)
        let month = calendar.component(Calendar.Component.month, from: date)
        let hour = calendar.component(Calendar.Component.hour, from: date)
        return ((day == 31 && month == 12 && hour >= 20) || ( day == 1 && month == 1))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterFromKeyboardNotifications()
    }
}
extension OrderingViewController: UITextFieldDelegate {
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWasShown(notification: NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(-keyboardSize!.height + 30) }
        if self.isSelfServiceSegmentControl.selectedSegmentIndex == 1 {
            let bottomOffset = CGPoint(x: 0, y: 300)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }else{
            let bottomOffset = CGPoint(x: 0, y: 400)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        self.scrollView.snp.updateConstraints { (make) in make.bottom.equalToSuperview().offset(0) }
    }
}


extension UIViewController {
    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view) // This line causes the view to be reloaded
    }
}
