//
//  Restaurants.swift
//  Okinava
//
//  Created by _ on 02.05.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import MapKit
import Foundation


class RestaurantMapViewAnnotation :   NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
