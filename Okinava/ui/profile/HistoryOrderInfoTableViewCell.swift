//
//  HistoryOrderInfoItem.swift
//  Okinava
//
//  Created by _ on 20.05.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit
import SnapKit


class HistoryOrderInfoTableViewCell : UITableViewCell {
    let dateLabel = UILabel()
    let sumLabel = UILabel()
    let orderStatusLabel = UILabel()
    let orderNumberLabel = UILabel()
    var orderInfo: IikoDeliveryHistory?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        prepareView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func prepareView(){
        let wrapper = UIView()
        self.addSubview(wrapper)
        wrapper.addSubview(self.dateLabel)
        wrapper.addSubview(self.orderNumberLabel)
        wrapper.addSubview(self.sumLabel)
        wrapper.addSubview(self.orderStatusLabel)
        
       
        wrapper.layer.shadowOpacity = 0.08
        wrapper.layer.shadowRadius = 4
        wrapper.layer.shadowOffset = CGSize(width: 0, height: 4 )
        wrapper.backgroundColor = .white
        wrapper.layer.cornerRadius = 8
        
        self.orderNumberLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.orderNumberLabel.textColor = DEFAULT_COLOR_BLACK
        self.orderNumberLabel.adjustsFontSizeToFitWidth = true
        
        self.dateLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.dateLabel.textColor = DEFAULT_COLOR_GRAY
        self.dateLabel.adjustsFontSizeToFitWidth = true
        
        self.sumLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.sumLabel.textColor = DEFAULT_COLOR_BLACK
        self.sumLabel.textAlignment = .right
        self.sumLabel.adjustsFontSizeToFitWidth = true
        self.orderStatusLabel.font = FONT_ROBOTO_REGULAR?.withSize(12)
        self.orderStatusLabel.textColor = DEFAULT_COLOR_GRAY
        self.orderStatusLabel.textAlignment = .right
        self.orderStatusLabel.adjustsFontSizeToFitWidth = true
        wrapper.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(10)
            make.right.bottom.equalToSuperview().offset(-10)
            make.height.equalTo(75)
        }
        self.orderNumberLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16)
            make.width.equalTo(160)
        }
        self.dateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.orderNumberLabel.snp.left)
            make.top.equalTo(self.orderNumberLabel.snp.bottom).offset(4)
            make.width.equalTo(160)
        }
        self.sumLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(16)
            make.width.equalTo(70)
        }
        self.orderStatusLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(self.sumLabel.snp.bottom).offset(4)
            make.width.equalTo(70)
        }
    }
    func setData(orderInfo: IikoDeliveryHistory){
        self.orderInfo = orderInfo
        self.dateLabel.text =  orderInfo.date
        if (orderInfo.sum == 0) { self.sumLabel.text = "0"  + RUBLE_LETTER  }
        else { self.sumLabel.text =  String(orderInfo.sum.cleanValue) + RUBLE_LETTER }
        self.orderStatusLabel.text = orderInfo.getStatusInRussian().lowercased()
        self.orderNumberLabel.text = "Заказ №" + (orderInfo.number ?? "0")
    }
}
