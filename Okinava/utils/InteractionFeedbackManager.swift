//
//  InteractionFeedbackManager.swift
//  Okinava
//
//  Created by _ on 21.06.2018.
//  Copyright © 2018 _. All rights reserved.
//

import Foundation
import UIKit
class InteractionFeedbackManager {
   public static let shared : InteractionFeedbackManager  = InteractionFeedbackManager()
    private init(){
        
    }
    func makeNotification(level: UINotificationFeedbackGenerator.FeedbackType = .success) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(level)
    }
    func makeFeedback(level: UIImpactFeedbackGenerator.FeedbackStyle = .light){
        let generator = UIImpactFeedbackGenerator(style: level)
        generator.impactOccurred()
    }
}
