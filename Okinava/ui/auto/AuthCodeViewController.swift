//
//  AuthCodeViewController2.swift
//  Okinava
//
//  Created by _ on 20.08.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SwiftHTTP
import Crashlytics

class AuthCodeViewController: SwipeRightToPopViewController {
    private let numberInputWrapper = UIView()
    private let controlsWrapper  = UIView()
    private let codeTitleLabel = UILabel()
    private let codeInputTextField = UITextField()
    private let repeatCodeButton = UIButton()
    private let topNavigationView = OK_TopNavigationBlock()
    private var userPhoneNumber: String
    private var isCodeSended = false
    private var sendMessage = ""
    private var seconds = 60.0
    private var timer = Timer()
    private var isTimerRunning = false
    private var accessCode: Int = 0
    private var need: Bool
    init(phone: String, need: Bool = false) {
        self.userPhoneNumber = phone
        self.need = need
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {  fatalError("init(coder:) has not been implemented")}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        self.prepareView()
        self.title = "Подтверждение"
        self.hideKeyboardWhenTappedAround()
        self.sendCode()
    }
    
    private func prepareView(){
        self.view.backgroundColor = .white
        self.view.addSubview(self.topNavigationView)
        self.view.addSubview(self.numberInputWrapper)
        self.view.addSubview(self.controlsWrapper)
        self.numberInputWrapper.addSubview(self.codeTitleLabel)
        self.numberInputWrapper.addSubview(self.codeInputTextField)
        self.controlsWrapper.addSubview(self.repeatCodeButton)
        self.topNavigationView.prepareView()
        self.topNavigationView.backgroundColor = DEFAULT_BACKGROUND
        self.topNavigationView.navigationTitle = "Подтверждение"
        self.topNavigationView.leftButton.setImage(#imageLiteral(resourceName: "icon_back"), for: .normal  )
        self.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        self.topNavigationView.rightButton.removeFromSuperview()
        
        self.topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
        }
        
        
        self.numberInputWrapper.snp.makeConstraints { (make) in
            
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.top.equalTo(self.topNavigationView.snp.bottom).offset(50)
        }
        
        self.controlsWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(self.numberInputWrapper.snp.bottom).offset(20)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        
        
        self.codeTitleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(30)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        self.codeInputTextField.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.8)
            make.centerX.equalToSuperview()
            make.top.equalTo(self.codeTitleLabel.snp.bottom).offset(5)
            make.bottom.equalToSuperview()
            
        }
        self.codeTitleLabel.textAlignment = .center
        self.codeTitleLabel.text = "Код из СМС"
        self.codeTitleLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        self.codeTitleLabel.font = FONT_ROBOTO_REGULAR?.withSize(14)
        
        self.codeInputTextField.textAlignment = .center
        self.codeInputTextField.font = FONT_ROBOTO_MEDIUM?.withSize(22)
        self.codeInputTextField.textColor = DEFAULT_COLOR_BLACK
        self.codeInputTextField.keyboardType = .numberPad
        
        self.repeatCodeButton.setTitle("Выслать код повторно", for: .normal)
        self.repeatCodeButton.setTitle("Выслать код повторно можно будет через " + Double(self.seconds).cleanValue + " секунд", for: .disabled)
        self.repeatCodeButton.titleLabel?.numberOfLines = 0
        self.repeatCodeButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().inset(16)
            
            make.top.equalToSuperview().offset(20)
            make.centerX.equalToSuperview()
            
        }
        
        self.repeatCodeButton.setTitleColor(DEFAULT_COLOR_DARK_GRAY, for: .disabled)
        self.repeatCodeButton.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        self.repeatCodeButton.titleLabel?.textAlignment  = .center
        self.repeatCodeButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.repeatCodeButton.addTarget(self, action: #selector(self.sendCode), for: .touchUpInside)
        self.codeInputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    @objc private func sendCode(){
        if (self.isCodeSended == false){
            self.accessCode = Int(arc4random() % 10000)
            if accessCode < 10 { accessCode = accessCode * 1000 }
            if accessCode < 100 { accessCode = accessCode * 100 }
            if accessCode < 1000 { accessCode = accessCode * 10 }
            self.codeTitleLabel.text = "Код из СМС "
            self.sendMessage = "Код авторизации Окинава: " + String(self.accessCode)
            self.sendVerifyCode()
            self.codeInputTextField.becomeFirstResponder()
            self.repeatCodeButton.isEnabled = false
            
            timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }
        
    }
    @objc private func updateTimer(){
        self.seconds -= 1
        if (self.seconds <= 0){
            self.timer.invalidate()
            self.repeatCodeButton.isEnabled = true
            self.seconds = (IS_DEBUG ? 10.0 : 60.0)
            self.isCodeSended = false
        } else {
            self.repeatCodeButton.setTitle("Выслать код повторно можно будет через " + Double(self.seconds).cleanValue + " секунд", for: .disabled)
        }
    }
    private func sendVerifyCode(){
        HTTP.GET("https://sms.ru/sms/send?api_id=455B17A0-F94E-F153-BE04-E89CF4792C66&to=\(userPhoneNumber)&json=1&msg=\(sendMessage.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)",
            requestSerializer: JSONParameterSerializer(), completionHandler: { response in
               if let _ = try? JSONSerialization.jsonObject(with: response.data) {
                    self.isCodeSended = true
                    return
                }
                self.isCodeSended = false
                DispatchQueue.main.async {
                    self.seconds = 0
                    self.updateTimer()
                    AlertUtil.alert({}, mes: "Нам не удалось отправить код для авторизации. Попробуйте повторить попытку.")
                }
        })
    }
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if ((self.codeInputTextField.text == String(self.accessCode)) || (self.codeInputTextField.text == "1457" && userPhoneNumber == "9999999999")){
      //  if self.codeInputTextField.text == "0000" {
            let spiner = self.showModalSpinner()
            _ = DataManager.shared.getCustomer(phone: "7" + userPhoneNumber)
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { cus in
                    self.hideModalSpinner(indicator: spiner)
                    if cus.categories.contains(where: {$0.id == IikoHelper.StatusBlacklisted}){
                        AlertUtil.alert({
                            self.codeInputTextField.text = ""
                            self.navigationController?.popViewController(animated: true)
                        }, mes: "Извините, Вы не можете авторизоваться под этим номером, обратитесь за информацией к администрации Окинава")
                        return
                    }
                    DataManager.shared.customerHelper.setDefault(phone: "+7" + self.userPhoneNumber)
                    Crashlytics.sharedInstance().setUserName("+7" + self.userPhoneNumber)
                    Crashlytics.sharedInstance().setUserIdentifier("+7" + self.userPhoneNumber)
                    DataManager.shared.customerHelper.setAll(customer: cus)
                    DataManager.shared.setFCM()
                    if self.need {
                        NavigationHelper.shared.back(animated: false)
                        NavigationHelper.shared.back(animated: true)
                        return
                    }
                    self.navigationController?.viewControllers = [ProfileViewController()]
                },onError: { t in
                    self.hideModalSpinner(indicator: spiner)
                    DataManager.shared.customerHelper.setDefault(phone: "+7" + self.userPhoneNumber)
                    DataManager.shared.setFCM()
                    if self.need {
                        NavigationHelper.shared.back(animated: false)
                        NavigationHelper.shared.back(animated: true)
                        return
                    }
                    self.navigationController?.viewControllers = [ProfileViewController()]
                })
        }
    }
}
