//
//  PromoScreenTableViewCell.swift
//  Okinava
//
//  Created by _ on 19.07.2018.
//  Copyright © 2018 _. All rights reserved.
//

import UIKit
import SnapKit

let PromoScreenTableViewCellIdentifer = "PromoScreenTableViewCellIdentifer"
class PromoScreenTableViewCell: UITableViewCell {
    public var promo: smartPromo!
    private let wrapperView = UIView()
    private let promoImageView = UIImageView(image: #imageLiteral(resourceName: "promo_image_1"))
    private let promoTitleLabel  = UILabel ()
    private let promoDescriptionLabel = UILabel()
    private let promoMoreButton = UIButton()
    private let promoShareButton = UIButton()
    
    func prepareView(){

        self.wrapperView.backgroundColor = .white
        self.subviews.forEach({ $0.removeFromSuperview()})
        self.addSubview(self.wrapperView)
        
        self.wrapperView.addSubview(self.promoImageView)
        self.wrapperView.addSubview(self.promoTitleLabel)
        self.wrapperView.addSubview(self.promoDescriptionLabel)
        self.wrapperView.addSubview(self.promoShareButton)
        self.wrapperView.addSubview(self.promoMoreButton)
        
        self.wrapperView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
        }
        self.promoImageView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(self.snp.width).multipliedBy(0.5)

            make.top.equalToSuperview()
        }
        self.promoTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.promoImageView.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
            make.right.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE)
        }
        self.promoDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.promoTitleLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE)
            make.right.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE)
        }
        
        self.promoShareButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(DEFAULT_OFFSET_SIZE * 2 + 10)
            make.bottom.equalToSuperview().offset(-10)
            make.top.equalTo(self.promoDescriptionLabel.snp.bottom).offset(10)
        }
        self.promoMoreButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE)
            make.centerY.equalTo(self.promoShareButton)
        }
        
        self.promoTitleLabel.numberOfLines = 0
        self.promoTitleLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        self.promoTitleLabel.textColor = DEFAULT_COLOR_BLACK
        
        self.promoDescriptionLabel.numberOfLines = 3
        self.promoDescriptionLabel.font = FONT_ROBOTO_REGULAR?.withSize(14)
        self.promoDescriptionLabel.textColor = DEFAULT_COLOR_DARK_GRAY
        
        self.promoMoreButton.setTitle("Читать", for: .normal)
        self.promoMoreButton.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        self.promoMoreButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        
        self.promoShareButton.setTitle("Поделиться", for: .normal)
        self.promoShareButton.setTitleColor(DEFAULT_COLOR_RED, for: .normal)
        self.promoShareButton.titleLabel?.font = FONT_ROBOTO_REGULAR?.withSize(12)
        
        self.promoImageView.contentMode = .scaleAspectFill
        self.promoImageView.clipsToBounds = true
        
        self.wrapperView.layer.shadowOffset = CGSize(width: 0, height: 8)
        self.wrapperView.layer.shadowOpacity = 0.12
        self.wrapperView.layer.shadowRadius = 15
        self.wrapperView.layer.cornerRadius = 8

        self.promoImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.promoImageView.layer.cornerRadius = 8
        
        let shareIcon = UIImageView(image: #imageLiteral(resourceName: "ic_share"))
        shareIcon.contentMode = .scaleAspectFit
        shareIcon.clipsToBounds = true
        self.promoShareButton.addSubview(shareIcon)
        shareIcon.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(-DEFAULT_OFFSET_SIZE - 10)
            make.width.height.equalTo(DEFAULT_OFFSET_SIZE)
        }
        self.promoShareButton.addTarget(self, action: #selector(self.shareButtonTapped), for: .touchUpInside)
        self.promoMoreButton.addTarget(self, action: #selector(self.showMoreButtonTapped), for: .touchUpInside)
    }
    
    func setData(){
        self.promoTitleLabel.text = self.promo.title
        self.promoDescriptionLabel.text = self.promo.descriptionField
        self.promoImageView.sd_setImage(with: URL(string: self.promo!.image))
    }

    
    @objc private func showMoreButtonTapped(){
       (self.parentViewController as? PromosScreenViewController)?.openPromoScreenHandler(promo: self.promo!)
    }
    
    @objc private func shareButtonTapped(){
        let firstActivityItem = self.promoTitleLabel.text!
        let secondActivityItem : NSURL = NSURL(string: "http//okinavakazan.ru")!
        let image : UIImage = self.promoImageView.image!
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.saveToCameraRoll ]
        self.parentViewController?.present(activityViewController, animated: true, completion: nil)
    }
}
