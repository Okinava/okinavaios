//
//  HistoryDetailController.swift
//  Okinava
//
//  Created by Timerlan on 31.10.2018.
//  Copyright © 2018 SmartResto. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class HistoryDetailController: BaseController<HistoryDetailView> {
    private var orderInfo: IikoDeliveryHistory
    init(orderInfo: IikoDeliveryHistory){
        self.orderInfo = orderInfo
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        mainView.set(orderInfo: orderInfo)
        mainView.topNavigationView.leftButton.addTarget(self, action: #selector(goBackButtonTapped), for: .touchUpInside)
        mainView.byButton.addTarget(self, action: #selector(byButtonTapped), for: .touchUpInside)
    }
    
    @objc func goBackButtonTapped(){ navigationController?.popViewController(animated: true) }
    
    @objc func byButtonTapped(){
        _ = DataManager.shared.clearOrder().subscribe(onNext: { b in
            AlertUtil.repeatOrder({
                _ = DataManager.shared.addOrder(history: self.orderInfo)
                    .observeOn(ThreadUtil.shared.mainScheduler)
                    .subscribe(onNext: { b in
                        _ = DataManager.shared.getOrder()
                            .observeOn(ThreadUtil.shared.mainScheduler)
                            .subscribe(onNext: { r in
                                if r.count == 0 {
                                    AlertUtil.alert({}, mes: "Невозможно повторить заказ, в данном заказе нет активных товаров")
                                    return
                                }
                                if !b {
                                    AlertUtil.alert({
                                        NavigationHelper.shared.showNext(CartViewController())
                                    }, mes: "В данном заказе не все позиции товаров активны для продажи")
                                }else{
                                    NavigationHelper.shared.showNext(CartViewController())
                                }
                            })
                    })
            })
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainView.disp?.dispose()
    }
}

class HistoryDetailView: BaseView {
    lazy var topNavigationView: OK_TopNavigationBlock = {
        let view = OK_TopNavigationBlock()
        view.prepareView()
        view.navigationTitle = "История заказа"
        view.leftButton.setImage(UIImage(named: "icon_close"), for: .normal  )
        view.rightButton.removeFromSuperview()
        return view
    }()
    
    private lazy var orderNumberLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(16)
        view.textColor = DEFAULT_COLOR_BLACK
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    
    private lazy var imgTime: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "clock")
        return view
    }()

    private lazy var timeLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(20)
        view.textColor = DEFAULT_COLOR_BLACK
        view.adjustsFontSizeToFitWidth = true
        view.text = "00:00"
        return view
    }()
    
    private lazy var callButton: ZFRippleButton = {
        let view = ZFRippleButton()
        var img = UIImage(named: "phone_white")
        view.contentEdgeInsets = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
        view.setImage(img, for: .normal)
        view.backgroundColor = .white
        view.layer.backgroundColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
        view.layer.cornerRadius = 35
        view.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 8
        view.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        return view
    }()
    lazy var byButton: ZFRippleButton = {
        let view = ZFRippleButton()
        var img = #imageLiteral(resourceName: "icon_basket")
        view.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        view.setImage(img, for: .normal)
        view.backgroundColor = .white
        view.layer.backgroundColor = UIColor().hexStringToUIColor(0xe4e4e4).cgColor
        view.layer.cornerRadius = 35
        view.layer.borderColor = UIColor().hexStringToUIColor(0xe4e4e4).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 8
        return view
    }()
    @objc private func callButtonTapped(){
        guard let number = URL(string: "tel://+78432334433") else { return }
        UIApplication.shared.open(number)
    }
    private lazy var lineT: UIView = {
        let view = UIView()
//        view.backgroundColor = UIColor().hexStringToUIColor(0xe4e4e4)
        return view
    }()
    private lazy var line: UIView = {
        let view = UIView()
//        view.backgroundColor = UIColor().hexStringToUIColor(0xe4e4e4)
        return view
    }()
    private lazy var lineB: UIView = {
        let view = UIView()
//        view.backgroundColor = UIColor().hexStringToUIColor(0xe4e4e4)
        return view
    }()
    private lazy var lineGray: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0xe4e4e4)
        return view
    }()
    private lazy var lineGreen: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().hexStringToUIColor(0x75BC07)
        return view
    }()
    
    private lazy var firButton: ZFRippleButton = {
        let view = ZFRippleButton()
        var img = UIImage(named: "fir_his")
        view.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        view.setImage(img, for: .normal)
        view.backgroundColor = .white
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.cornerRadius = 25
        view.layer.borderColor = UIColor().hexStringToUIColor(0xe4e4e4).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 8
        view.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var firLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        view.text = "Принят"
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    
    private lazy var secButton: ZFRippleButton = {
        let view = ZFRippleButton()
        var img = UIImage(named: "sec_his")
        view.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        view.setImage(img, for: .normal)
        view.backgroundColor = .white
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.cornerRadius = 25
        view.layer.borderColor = UIColor().hexStringToUIColor(0xe4e4e4).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 8
        view.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var secLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        view.text = "Готовиться"
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    
    private lazy var thButton: ZFRippleButton = {
        let view = ZFRippleButton()
        var img = UIImage(named: "th_his")
        view.contentEdgeInsets = UIEdgeInsets(top: 15, left: 18, bottom: 15, right: 18)
        view.setImage(img, for: .normal)
        view.backgroundColor = .white
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.cornerRadius = 25
        view.layer.borderColor = UIColor().hexStringToUIColor(0xe4e4e4).cgColor
        view.layer.borderWidth = 1
        view.layer.shadowColor = UIColor().hexStringToUIColor(0x222222).cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 8
        view.addTarget(self, action: #selector(callButtonTapped), for: .touchUpInside)
        return view
    }()
    private lazy var thLabel: UILabel = {
        let view = UILabel()
        view.font = FONT_ROBOTO_MEDIUM?.withSize(12)
        view.textColor = DEFAULT_COLOR_BLACK
        view.text = "В пути"
        view.adjustsFontSizeToFitWidth = true
        return view
    }()
    private lazy var orderItemsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.shadowOffset = CGSize(width: 0, height: 8 )
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.08
        view.addSubview(self.orderItemsStackView)
        return view
    }()
    private lazy var orderItemsStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.alignment = .fill
        return view
    }()
    private lazy var scrollView = UIScrollView()
    private lazy var mView = UIView()

    override func setContent() {
        super.setContent()
        scrollView.alwaysBounceVertical = true
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1500)
        contentView.addSubview(topNavigationView)
        contentView.addSubview(scrollView)
        scrollView.addSubview(mView)
        mView.addSubview(orderNumberLabel)
        mView.addSubview(imgTime)
        mView.addSubview(timeLabel)
        mView.addSubview(callButton)
        mView.addSubview(byButton)
        mView.addSubview(line)
        mView.addSubview(lineT)
        mView.addSubview(lineB)
        mView.addSubview(lineGray)
        mView.addSubview(lineGreen)
        mView.addSubview(firButton)
        mView.addSubview(secButton)
        mView.addSubview(thButton)
        mView.addSubview(firLabel)
        mView.addSubview(secLabel)
        mView.addSubview(thLabel)
        mView.addSubview(orderItemsView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        topNavigationView.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.width.equalToSuperview()
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.topMargin)
        }
        scrollView.snp.remakeConstraints{ make in
            make.top.equalTo(topNavigationView.snp.bottom)
            make.width.equalToSuperview()
            make.left.right.bottom.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        mView.snp.remakeConstraints{ make in
            make.width.equalToSuperview()
            make.top.left.right.bottom.equalToSuperview()
        }
        lineT.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.height.equalTo(1)
            make.width.equalToSuperview()
        }
        orderNumberLabel.snp.remakeConstraints{ make in
            make.top.equalTo(lineT.snp.bottom).offset(24)
            make.left.equalTo(16)
        }
        imgTime.snp.remakeConstraints{ make in
            make.centerY.equalTo(orderNumberLabel.snp.centerY)
            make.right.equalTo(timeLabel.snp.left).offset(-16)
            make.height.width.equalTo(20)
        }
        timeLabel.snp.remakeConstraints{ make in
            make.centerY.equalTo(orderNumberLabel.snp.centerY)
            make.right.equalToSuperview().offset(-24)
        }
        line.snp.remakeConstraints{ make in
            make.top.equalTo(orderNumberLabel.snp.bottom).offset(16)
            make.height.equalTo(1)
            make.width.equalToSuperview()
        }
        secButton.snp.remakeConstraints{ make in
            make.top.equalTo(line.snp.bottom).offset(16)
            make.height.width.equalTo(50)
            make.centerX.equalToSuperview()
        }
        lineGray.snp.remakeConstraints{ make in
            make.centerY.equalTo(secButton.snp.centerY)
            make.left.equalTo(firButton.snp.centerX)
            make.right.equalTo(thButton.snp.centerX)
            make.height.equalTo(1)
        }
        firButton.snp.remakeConstraints{ make in
            make.right.equalTo(secButton.snp.left).offset(-48)
            make.height.width.equalTo(50)
            make.centerY.equalTo(secButton.snp.centerY)
        }
        thButton.snp.remakeConstraints{ make in
            make.left.equalTo(secButton.snp.right).offset(48)
            make.height.width.equalTo(50)
            make.centerY.equalTo(secButton.snp.centerY)
        }
        firLabel.snp.remakeConstraints{ make in
            make.top.equalTo(firButton.snp.bottom).offset(16)
            make.centerX.equalTo(firButton.snp.centerX)
        }
        secLabel.snp.remakeConstraints{ make in
            make.top.equalTo(secButton.snp.bottom).offset(16)
            make.centerX.equalTo(secButton.snp.centerX)
        }
        thLabel.snp.remakeConstraints{ make in
            make.top.equalTo(thButton.snp.bottom).offset(16)
            make.centerX.equalTo(thButton.snp.centerX)
        }
        lineB.snp.remakeConstraints{ make in
            make.top.equalTo(secLabel.snp.bottom).offset(16)
            make.height.equalTo(1)
            make.width.equalToSuperview()
        }
        orderItemsView.snp.remakeConstraints { (make) in
            make.top.equalTo(lineB.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.centerX.equalToSuperview()
        }
        orderItemsStackView.snp.remakeConstraints { (make) in
            make.width.top.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-0)
        }
        callButton.snp.remakeConstraints{ make in
            make.top.equalTo(orderItemsView.snp.bottom).offset(32)
            make.width.height.equalTo(70)
            make.right.equalTo(contentView.snp.centerX).offset(-32)
        }
        byButton.snp.remakeConstraints{ make in
            make.top.equalTo(orderItemsView.snp.bottom).offset(32)
            make.width.height.equalTo(70)
            make.left.equalTo(contentView.snp.centerX).offset(32)
            make.bottom.equalToSuperview()
        }
    }
    
    func set(orderInfo: IikoDeliveryHistory){
        orderNumberLabel.text = "Заказ №" + (orderInfo.number ?? "0")
        for item in orderInfo.items {
            let cartItemView = UIView()
            let cartItemTitle = UILabel()
            let cartItemCount = UILabel()
            let cartItemPrice = UILabel()
            cartItemView.backgroundColor = .white
            cartItemView.addSubview(cartItemTitle)
            cartItemView.addSubview(cartItemCount)
            cartItemView.addSubview(cartItemPrice)
            cartItemView.frame.size  = CGSize(width: self.orderItemsStackView.frame.width, height: 46)
            
            self.orderItemsStackView.addArrangedSubview(cartItemView)
            cartItemView.snp.makeConstraints { (make) in
                make.height.equalTo(46)
            }
            cartItemTitle.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemCount.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemPrice.font = FONT_ROBOTO_MEDIUM?.withSize(14)
            cartItemPrice.textAlignment = .right
            cartItemTitle.adjustsFontSizeToFitWidth = true
            cartItemTitle.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(16)
                make.width.equalToSuperview().multipliedBy(0.6)
            }
            cartItemPrice.snp.makeConstraints { (make) in
                make.right.equalToSuperview().offset(-16)
                make.centerY.equalToSuperview()
                make.width.equalTo(50)
            }
            cartItemCount.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.right.equalTo(cartItemPrice.snp.left).offset(-20)
            }
            cartItemTitle.text = "\(item.name ?? "")"//" \(cartItem.1?.name ?? "")"
            cartItemPrice.text = String(Int64(item.sum ?? 0) / (item.amount ?? 1)) + " " + RUBLE_LETTER
            cartItemCount.text = "x" + String(item.amount)
        }
        let totalView = UIView()
        let labelForTotal = UILabel()
        let totalLabel = UILabel()
        totalView.frame.size  = CGSize(width: self.orderItemsStackView.frame.width, height: 46)
        totalLabel.font = FONT_ROBOTO_MEDIUM?.withSize(14)
        labelForTotal.font = FONT_ROBOTO_REGULAR?.withSize(12)
        labelForTotal.textColor = DEFAULT_COLOR_GRAY
        labelForTotal.text  = "Итого:"
        totalLabel.textAlignment = .right
        totalView.addSubview(labelForTotal)
        totalView.addSubview(totalLabel)
        
        self.orderItemsStackView.addArrangedSubview(totalView)
        totalView.snp.makeConstraints { (make) in make.height.equalTo(46) }
        labelForTotal.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(10)
        }
        totalLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(10)
        }
        totalLabel.text = String(Int64(orderInfo.sum)) + " " + RUBLE_LETTER
        
        
        switch (orderInfo.status) {
        case "NEW","WAITING":
            firButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
        case "ON_WAY":
            firButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
            secButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
            lineGreen.snp.remakeConstraints{ make in
                make.centerY.equalTo(secButton.snp.centerY)
                make.left.equalTo(firButton.snp.centerX)
                make.right.equalTo(secButton.snp.centerX)
                make.height.equalTo(1)
            }
        case "CLOSED","DELIVERED":
            firButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
            secButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
            thButton.layer.borderColor = UIColor().hexStringToUIColor(0x75BC07).cgColor
            lineGreen.snp.remakeConstraints{ make in
                make.centerY.equalTo(secButton.snp.centerY)
                make.left.equalTo(firButton.snp.centerX)
                make.right.equalTo(thButton.snp.centerX)
                make.height.equalTo(1)
            }
        default : break
        }
        let t = (orderInfo.date.toDate()?.timeIntervalSince1970 ?? 0) * 1000 * 10
        let tt = Date().timeIntervalSince1970 * 1000 * 10
        if tt > t { self.timeLabel.text = "00:00"
        }else{
            let min = Int64((((t - tt) / 10) / 1000) / 60.0)
            self.timeLabel.text = String(Int64(min/60)) + ":" + String(min % 60)
        }
        if orderInfo.status == "NEW" || orderInfo.status == "WAITING"
            || orderInfo.status == "ON_WAY" || orderInfo.status == "UNCONFIRMED" {
            disp = Observable<Int64>.interval(1, scheduler: ThreadUtil.shared.backScheduler)
                .concatMap({ t -> Observable<Int64> in return DataManager.shared.getTime() })
                .observeOn(ThreadUtil.shared.mainScheduler)
                .subscribe(onNext: { time in
                    if Double(time) > t {
                        self.timeLabel.text = "00:00"
                    }else{
                        let min = Int64((((t - Double(time)) / 10) / 1000) / 60)
                        self.timeLabel.text = String(Int64(min/60)) + ":" + String(min % 60)
                    }
                })
        }
    }
    var disp: Disposable?
}

extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
//        dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        
        return date
        
    }
}

